<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkendaraan
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKendaraan extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_kendaraan';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $kendaraan = array(
            'field' => 'kendaraan-input', 'label' => 'Jenis Kendaraan',
            'rules' => 'trim|max_length[100]|required'
        );
        $keterangan = array(
            'field' => 'keterangan-input', 'label' => 'Keterangan',
            'rules' => 'trim|max_length[255]'
        );

        return array($kode, $kendaraan, $keterangan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'kendaraan' => '', 'keterangan' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'kendaraan' => ucwords($record->kendaraan), 'keterangan' => $record->keterangan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'kendaraan asc')) as $record) {
            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $data[] = array(
                'kode' => $record->kode,
                'kendaraan' => ucwords($record->kendaraan),
                'aksi' => $linkBtn
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('kendaraan' => $query), 'sort' => 'kendaraan asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => ucwords($record->kendaraan)));
        }

        return $data;
    }

}
