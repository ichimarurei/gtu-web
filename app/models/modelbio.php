<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of modelbio
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelBio extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_biodata';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal_lahir', $this->formatdate->setDate($params['tanggal_lahir-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $id = array(
            'field' => 'id-input', 'label' => 'ID Pegawai',
            'rules' => 'trim|max_length[21]'
        );
        $ktp = array(
            'field' => 'ktp-input', 'label' => 'No KTP',
            'rules' => 'trim|max_length[50]'
        );
        $npwp = array(
            'field' => 'npwp-input', 'label' => 'No NPWP',
            'rules' => 'trim|max_length[50]'
        );
        $bpjs = array(
            'field' => 'bpjs-input', 'label' => 'No BPJS',
            'rules' => 'trim|max_length[50]'
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Nama Lengkap',
            'rules' => 'trim|max_length[255]|required'
        );
        $tempatLahir = array(
            'field' => 'tempat_lahir-input', 'label' => 'Tempat Lahir',
            'rules' => 'trim|max_length[255]|required'
        );
        $tanggalLahir = array(
            'field' => 'tanggal_lahir-input', 'label' => 'Tanggal Lahir',
            'rules' => 'trim|required'
        );
        $alamat = array(
            'field' => 'alamat-input', 'label' => 'Alamat Lengkap',
            'rules' => 'trim|required'
        );
        $telepon = array(
            'field' => 'telepon-input', 'label' => 'No Telepon/HP',
            'rules' => 'trim|max_length[100]|required'
        );
        $rekening = array(
            'field' => 'rekening-input', 'label' => 'Rekening',
            'rules' => 'trim|max_length[100]|required'
        );
        $email = array(
            'field' => 'email-input', 'label' => 'Email',
            'rules' => 'trim|max_length[255]|valid_email'
        );

        return array($kode, $id, $ktp, $npwp, $bpjs, $nama, $tempatLahir, $tanggalLahir, $alamat, $telepon, $email, $rekening);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'id' => '', 'ktp' => '', 'npwp' => '', 'bpjs' => '', 'nama' => '', 'kelamin' => 'cowok',
            'tempat_lahir' => '', 'tanggal_lahir' => '', 'agama' => 'Islam', 'pendidikan' => 'SMP',
            'rekening' => '', 'alamat' => '', 'telepon' => '', 'email' => '',
            'nikah' => 'belum', 'anak' => 0, 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != NULL) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'id' => strtoupper($record->id),
                'ktp' => strtoupper($record->ktp),
                'npwp' => strtoupper($record->npwp),
                'bpjs' => strtoupper($record->bpjs),
                'nama' => ucwords($record->nama),
                'kelamin' => $record->kelamin,
                'tempat_lahir' => ucwords($record->tempat_lahir),
                'tanggal_lahir' => $this->formatdate->getDate($record->tanggal_lahir, TRUE),
                'agama' => $record->agama,
                'pendidikan' => $record->pendidikan,
                'rekening' => $record->rekening,
                'alamat' => $record->alamat,
                'telepon' => $record->telepon,
                'email' => $record->email,
                'nikah' => $record->nikah,
                'anak' => $record->anak,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = array();
        // view all
        $status = 'a';
        $proyek = 'a';

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);

            if (strpos($queries[0], 's') !== FALSE) {
                $stat = explode('_', $queries[0]);
                $status = $stat[1];
            }

            if (strpos($queries[1], 'p') !== FALSE) {
                $stat = explode('_', $queries[1]);
                $proyek = $stat[1];
            }
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
            $rProyek = NULL;
            $rJabatan = NULL;
            $isShow = TRUE;
            $kontrak = '-';
            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="userBtn btn btn-info btn-flat">Akun</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="contractBtn btn btn-success btn-flat">Kontrak</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $rAkun = $this->getRecord(array('table' => 'data_akun', 'where' => array('biodata' => $record->kode, 'terpakai' => 1)));
            $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

            if ($rKontrak != NULL) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rKontrak->proyek, 'terpakai' => 1)));
                $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));

                if ($rKontrak->status === 'PKWT') {
                    $kontrak = $rKontrak->status . '-' . $rKontrak->ke;
                } else {
                    $kontrak = strtoupper($rKontrak->status);
                }

                if (($rKontrak->status === 'PKWT') && ($rKontrak->habis < date('Y-m-d'))) {
                    $kontrak = ($rKontrak->status === 'PKWT') ? 'HABIS MASA KONTRAK' : strtoupper($rKontrak->status);
                }
            }

            if ($status === '1') {
                $isShow = FALSE;

                if ($rKontrak != NULL) {
                    if ($rKontrak->habis >= date('Y-m-d')) { // kontrak masih berlaku
                        if ($rKontrak->status === 'PKWT') {
                            $isShow = TRUE;
                            $kontrak = $rKontrak->status . '-' . $rKontrak->ke;
                        }
                    }
                }
            }

            if ($status === '0') { // kontrak habis
                $isShow = FALSE;

                if ($rKontrak != NULL) {
                    $isShow = (($rKontrak->status !== 'PKWT') || ($rKontrak->habis < date('Y-m-d')));
                    $kontrak = ($rKontrak->status === 'PKWT') ? 'HABIS MASA KONTRAK' : strtoupper($rKontrak->status);
                }
            }

            if ($proyek !== 'a') {
                if ($rProyek != NULL && $isShow) {
                    $isShow = ($rProyek->kode === $proyek);
                } else {
                    $isShow = FALSE;
                }
            }

            if ($isShow) {
                $noTelp = explode('_', $record->telepon);
                $telpText = $noTelp[0];

                if ($noTelp[1] !== '-') {
                    $telpText .= ' &amp; <b>' . $noTelp[1] . '</b>';
                }

                $data[] = array(
                    'kode' => $record->kode,
                    'id' => strtoupper($record->id),
                    'nama' => ucwords($record->nama),
                    'telepon' => $telpText,
                    'akun' => (($rAkun != NULL) ? $rAkun->id : '-'),
                    'proyek' => (($rProyek != NULL) ? ucwords($rProyek->proyek) : '-'),
                    'jabatan' => (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-'),
                    'kontrak' => $kontrak,
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $queries = array($query, NULL);

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('nama' => $queries[1]), 'sort' => 'nama asc')) as $record) {
            $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

            if ($rKontrak != NULL) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rKontrak->proyek, 'terpakai' => 1)));

                if ($rProyek != NULL) {
                    if ($rProyek->kode === $queries[0]) {
                        array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nama) . ' [' . strtoupper($record->id) . ']'));
                    }
                }
            }
        }

        return $data;
    }

}
