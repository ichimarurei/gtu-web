<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkasar
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKasar extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_keu_besar';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $perihal = array(
            'field' => 'perihal-input', 'label' => 'Perihal',
            'rules' => 'trim|max_length[255]|required'
        );
        $debit = array(
            'field' => 'debit-input', 'label' => 'Nominal Pemasukan',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $kredit = array(
            'field' => 'kredit-input', 'label' => 'Nominal Pengeluaran',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $perihal, $debit, $kredit);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'perihal' => '', 'debit' => '', 'kredit' => '',
            'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1
        );
        $where = array('kode' => $kode);

        if (strpos($kode, '___') !== FALSE) {
            $queries = explode('___', $kode);

            if ($queries[0] === 'perihal') {
                $where = array('perihal' => strtoupper($queries[1]));
            }
        }

        $record = $this->getRecord(array('table' => $this->table, 'where' => $where));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'waktu' => $record->waktu, 'perihal' => strtoupper($record->perihal),
                'debit' => self::_toRp($record->debit), 'kredit' => self::_toRp($record->kredit),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $margin = 0;

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(waktu)' => date('Y')), 'sort' => 'perihal asc')) as $record) {
            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $debit = intval($record->debit);
            $kredit = intval($record->kredit);
            $linkTxt = '-';

            if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos'))) {
                $linkTxt = $linkBtn;
            }

            $margin += ($debit - $kredit);
            $data[] = array(
                'kode' => $record->kode,
                'perihal' => strtoupper($record->perihal),
                'debit' => self::_toRp($debit), 'kredit' => self::_toRp($kredit),
                'debitInt' => $debit, 'kreditInt' => $kredit,
                'balance' => self::_toRp($margin), 'balanceInt' => $margin,
                'aksi' => $linkTxt
            );
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
