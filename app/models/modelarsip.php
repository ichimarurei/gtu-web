<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelarsip
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelArsip extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_arsip';
    }

    public function doAction($params) {
        $this->setValues($params);
        $isSave = $this->doSave();

        if ($isSave) {
            $otherTable = 'data_presensi_' . (($params['jenis-input'] === 'presensi') ? 'info' : 'lembur');
            $rData = $this->getRecord(array('table' => $otherTable, 'where' => array('kode' => $params['data-input'], 'terpakai' => 1)));

            if ($rData != NULL) {
                $this->action(array(
                    'table' => $otherTable, 'type' => $this->UPDATE,
                    'data' => array('terpakai' => 0), 'at' => array('kode' => $rData->kode)
                ));
            }
        }

        return $isSave;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $rekap = array(
            'field' => 'rekap-input', 'label' => 'Data Rekap',
            'rules' => 'trim|required'
        );
        $data = array(
            'field' => 'data-input', 'label' => 'Data Presensi/Lembur',
            'rules' => 'trim|required'
        );

        return array($kode, $rekap, $data);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'rekap' => '', 'data' => '', 'jenis' => 'presensi', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'rekap' => $record->rekap, 'data' => $record->data, 'jenis' => $record->jenis,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = explode('___', $query);

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'rekap' => $queries[0]), 'sort' => 'entitas asc')) as $record) {
            $otherTable = 'data_presensi_' . (($record->jenis === 'presensi') ? 'info' : 'lembur');
            $rData = $this->getRecord(array('table' => $otherTable, 'where' => array('kode' => $record->data)));
            $rRekap = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('terpakai' => 1, 'kode' => $record->rekap)));

            if ($rData != NULL && $rRekap != NULL) {
                $rBiodata = NULL;
                $rRemarks = NULL;
                $statusAbsen = '';
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rRekap->proyek)));

                if ($otherTable === 'data_presensi_lembur') {
                    $rPresensi = $this->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $rData->presensi)));

                    if ($rPresensi != NULL) {
                        $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rPresensi->biodata)));
                        $rRemarks = $this->getRecord(array('table' => 'data_presensi_catatan', 'where' => array('presensi' => $rPresensi->kode)));
                    }
                } else {
                    $statusAbsen = strtoupper($rData->status);
                    $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rData->biodata)));
                    $rRemarks = $this->getRecord(array('table' => 'data_presensi_catatan', 'where' => array('presensi' => $rData->kode)));
                }

                if ($rBiodata != NULL && $rProyek != NULL) {
                    $data[] = array(
                        'kode' => $record->kode,
                        'proyek' => ucwords($rProyek->proyek),
                        'id' => strtoupper($rBiodata->id),
                        'biodata' => ucwords($rBiodata->nama),
                        'waktu' => $this->formatdate->getDateTime($rData->waktu),
                        'status' => $statusAbsen,
                        'remarks' => (($rRemarks == NULL) ? '' : $rRemarks->keterangan),
                        'aksi' => ''
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
