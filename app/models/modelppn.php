<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelppn
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelPPN extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_keu_ppn';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $ppn = array(
            'field' => 'ppn-input', 'label' => 'Nominal PPN',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $ppn);
    }

    public function getData($kode) {
        $queries = explode('___', $kode);
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => $queries[0], 'ppn' => '', 'bulan' => $queries[1], 'tahun' => $queries[2], 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('proyek' => $queries[0], 'bulan' => $queries[1], 'tahun' => $queries[2])));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'ppn' => self::_toRp($record->ppn),
                'bulan' => $record->bulan, 'tahun' => $record->tahun,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        return array();
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
