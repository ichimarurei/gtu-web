<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkacil
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKacil extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_keu_kacil';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal', $this->formatdate->setDate($params['tanggal-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $tanggal = array(
            'field' => 'tanggal-input', 'label' => 'Tanggal',
            'rules' => 'trim|required'
        );
        $perihal = array(
            'field' => 'perihal-input', 'label' => 'Perihal',
            'rules' => 'trim|required'
        );
        $nominal = array(
            'field' => 'nominal-input', 'label' => 'Nominal',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $biodata, $tanggal, $perihal, $nominal);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '', 'tanggal' => '', 'perihal' => '', 'nominal' => '', 'jenis' => 'kredit',
            'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'jenis' => $record->jenis, 'biodata' => $record->biodata,
                'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                'nominal' => self::_toRp($record->nominal),
                'waktu' => $record->waktu, 'perihal' => $record->perihal,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $balance = 0;
        $queries = explode('___', $query);

        if ($queries[0] === 'lihat') {
            foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(tanggal)' => date('Y'), 'proyek' => $queries[1]), 'sort' => 'tanggal asc')) as $record) {
                $linkBtn = '<a href="' . $record->kode . '" data-jenis="' . $record->jenis . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $debit = 0;
                $kredit = 0;
                $debitTxt = '';
                $kreditTxt = '';

                if ($record->jenis === 'debit') {
                    $debit = intval($record->nominal);
                } else if ($record->jenis === 'kredit') {
                    $kredit = intval($record->nominal);
                }

                if ($debit > 0) {
                    $debitTxt = self::_toRp($debit);
                }

                if ($kredit > 0) {
                    $kreditTxt = self::_toRp($kredit);
                }

                $balance += ($debit - $kredit);
                $linkTxt = (($record->biodata === $this->session->userdata('_bio')) ? $linkBtn : '-');

                if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos'))) {
                    $linkTxt = $linkBtn;
                }

                $data[] = array(
                    'kode' => $record->kode,
                    'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                    'perihal' => $record->perihal,
                    'debit' => $debitTxt, 'kredit' => $kreditTxt,
                    'tagihInt' => intval($debit), 'bayarInt' => intval($kredit),
                    'balance' => self::_toRp($balance),
                    'aksi' => $linkTxt
                );
            }
        } else if ($queries[0] === 'rekap') {
            for ($at = 1; $at <= 12; $at++) {
                $bln = $at;
                $debit = 0;
                $kredit = 0;

                if ($at < 10) {
                    $bln = '0' . $at;
                }

                if ($queries[1] !== 'all') {
                    foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(tanggal)' => date('Y'), 'MONTH(tanggal)' => $bln, 'proyek' => $queries[1]), 'sort' => 'tanggal asc')) as $record) {
                        if ($record->jenis === 'debit') {
                            $debit += intval($record->nominal);
                        } else if ($record->jenis === 'kredit') {
                            $kredit += intval($record->nominal);
                        }
                    }

                    $data[] = array(
                        'kode' => '0',
                        'periode' => strtoupper($this->formatdate->getMonth($at)) . ' ' . date('Y'),
                        'perihal' => 'Hasil rekap selama bulan ' . $this->formatdate->getMonth($at),
                        'debit' => self::_toRp($debit), 'kredit' => self::_toRp($kredit),
                        'tagihInt' => intval($debit), 'bayarInt' => intval($kredit),
                        'aksi' => ''
                    );
                } else {
                    foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(tanggal)' => date('Y'), 'MONTH(tanggal)' => $bln), 'sort' => 'tanggal asc')) as $record) {
                        $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

                        if ($rProyek != NULL) {
                            if ($record->jenis === 'debit') {
                                $debit += intval($record->nominal);
                            } else if ($record->jenis === 'kredit') {
                                $kredit += intval($record->nominal);
                            }
                        }
                    }

                    $data[] = array(
                        'kode' => '0',
                        'periode' => strtoupper($this->formatdate->getMonth($at)) . ' ' . date('Y'),
                        'perihal' => 'Hasil rekap semua Project di bulan ' . $this->formatdate->getMonth($at),
                        'debit' => self::_toRp($debit), 'kredit' => self::_toRp($kredit),
                        'tagihInt' => intval($debit), 'bayarInt' => intval($kredit),
                        'aksi' => ''
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
