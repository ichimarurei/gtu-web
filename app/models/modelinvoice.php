<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelinvoice
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelInvoice extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_invoice';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $presensi = array(
            'field' => 'presensi-input', 'label' => 'Data Presensi',
            'rules' => 'trim|required'
        );
        $lembur = array(
            'field' => 'lembur-input', 'label' => 'Data Lembur',
            'rules' => 'trim|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'No Invoice',
            'rules' => 'trim|max_length[100]|required'
        );
        $npwp = array(
            'field' => 'npwp-input', 'label' => 'NPWP',
            'rules' => 'trim|max_length[100]|required'
        );

        return array($kode, $nomor, $npwp, $presensi, $lembur, $proyek);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'presensi' => '', 'lembur' => '-',
            'perihal' => '', 'nomor' => '', 'npwp' => '', 'status' => 'proses',
            'bayar_driver' => '', 'fee_driver' => '', 'bayar_helper' => '', 'fee_helper' => '',
            'bayar_korlap' => '', 'fee_korlap' => '', 'ppn' => '', 'waktu' => date('Y-m-d H:i:s'),
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nomor' => strtoupper($record->nomor), 'npwp' => strtoupper($record->npwp),
                'proyek' => $record->proyek, 'ppn' => $record->ppn, 'presensi' => $record->presensi, 'lembur' => $record->lembur,
                'bayar_driver' => $record->bayar_driver, 'fee_driver' => $record->fee_driver,
                'bayar_helper' => $record->bayar_helper, 'fee_helper' => $record->fee_helper,
                'bayar_korlap' => $record->bayar_korlap, 'fee_korlap' => $record->fee_korlap,
                'waktu' => $record->waktu, 'perihal' => $record->perihal, 'status' => $record->status,
                'terpakai' => $record->terpakai
            );

            if ($record->entitas > 0) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

                if ($rProyek != NULL) {
                    $period = explode(' ', $this->formatdate->getDate($record->waktu));
                    $data['tanggal'] = $this->formatdate->getDate($record->waktu);
                    $data['periode'] = strtoupper($period[1]) . ' ' . $period[2];
                    $data['pt'] = ucwords($rProyek->proyek);
                    $data['alamat'] = ucwords($rProyek->alamat);
                    $data['rp_bayar_driver'] = self::_toRp($record->bayar_driver);
                    $data['rp_fee_driver'] = self::_toRp($record->fee_driver);
                    $data['rp_bayar_helper'] = self::_toRp($record->bayar_helper);
                    $data['rp_fee_helper'] = self::_toRp($record->fee_helper);
                    $data['rp_bayar_korlap'] = self::_toRp($record->bayar_korlap);
                    $data['rp_fee_korlap'] = self::_toRp($record->fee_korlap);
                    $data['rp_ppn'] = self::_toRp($record->ppn);
                    $subInv = intval($record->bayar_driver) + intval($record->bayar_helper) + intval($record->bayar_korlap);
                    $subFee = intval($record->fee_driver) + intval($record->fee_helper) + intval($record->fee_korlap);
                    $subTotal = $subInv + $subFee;
                    $totalInv = $subTotal + intval($record->ppn);
                    $data['rp_inv_all'] = self::_toRp($subInv);
                    $data['rp_fee_all'] = self::_toRp($subFee);
                    $data['rp_subtotal'] = self::_toRp($subTotal);
                    $data['rp_total'] = self::_toRp($totalInv);
                    $data['rp_terbilang'] = $this->terbilang->format($totalInv);
                    $rLokasi = $this->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                    $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                    $rKendaraan = $this->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));

                    if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                        $data['kerjaan'] = ucwords($rLokasi->lokasi) . ' ' . ucwords($rKendaraan->kendaraan) . ' ' . ucwords($rTipe->tipe);
                    }
                }
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $status = array('proses' => 'DITAGIHKAN', 'bayar' => 'SUDAH DIBAYAR');

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'nomor asc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

            if ($rProyek != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'nomor' => strtoupper($record->nomor),
                    'waktu' => $this->formatdate->getDate($record->waktu),
                    'status' => $status[$record->status],
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $queries = array($query, NULL);
        $where = array('terpakai' => 1);

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);
        }

        if ($queries[0] !== 'all') {
            $where['proyek'] = $queries[0];
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'find' => array('nomor' => $queries[1]), 'sort' => 'nomor asc')) as $record) {
            $rPresensi = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $record->presensi, 'jenis' => 'presensi', 'status' => 'setuju')));
            $rLembur = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $record->lembur, 'jenis' => 'lembur', 'status' => 'setuju')));

            if ($rPresensi != NULL) {
                $kodeLembur = '0';
                $text = 'Rekap Absensi #' . $this->formatdate->getDate($rPresensi->dari) . ' - ' . $this->formatdate->getDate($rPresensi->hingga);

                if ($rLembur != NULL) {
                    $kodeLembur = $rLembur->kode;
                    $text .= ' | Rekap Lembur #' . $this->formatdate->getDate($rLembur->dari) . ' - ' . $this->formatdate->getDate($rLembur->hingga);
                }

                array_push($data, array('id' => $rPresensi->kode . '___' . $kodeLembur . '___' . $record->kode, 'text' => $text));
            }
        }

        return $data;
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.') . ',-';
    }

}
