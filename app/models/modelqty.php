<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelqty
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelQTY extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_qty';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Waktu Catat',
            'rules' => 'trim|required'
        );
        $inap = array(
            'field' => 'inap-input', 'label' => 'Muat Inap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $skr = array(
            'field' => 'skr-input', 'label' => 'SKR',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $ritase = array(
            'field' => 'ritase-input', 'label' => 'Ritase 2',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $insentif = array(
            'field' => 'insentif-input', 'label' => 'Insentif Pengiriman',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $biodata, $waktu, $inap, $insentif, $ritase, $skr);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '',
            'inap' => '', 'skr' => '', 'ritase' => '', 'insentif' => '',
            'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1,
            'stime' => $this->formatdate->getDate(date('Y-m-d H:i:s'))
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'waktu' => $record->waktu, 'biodata' => $record->biodata,
                'inap' => $record->inap, 'skr' => $record->skr,
                'ritase' => $record->ritase, 'insentif' => $record->insentif,
                'terpakai' => $record->terpakai,
                'stime' => $this->formatdate->getDate($record->waktu)
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'waktu desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'waktu' => $this->formatdate->getDate($record->waktu),
                    'inap' => $record->inap, 'skr' => $record->skr,
                    'ritase' => $record->ritase, 'insentif' => $record->insentif,
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
