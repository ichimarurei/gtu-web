<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modelkontrak
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKontrak extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_kontrak';
    }

    public function doAction($params) {
        $this->setValues($params);
        // overwrite
        $this->setValue('berlaku', $this->formatdate->setDate($params['berlaku-input']));
        $this->setValue('habis', $this->formatdate->setDate($params['habis-input']));
        $isSave = $this->doSave();

        if ($isSave) {
            $urutKe = count($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'terdata' => date('Y-m-d')), 'sort' => 'terdata asc', 'group' => 'biodata')));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $params['biodata-input'], 'id' => '')));
            $noUrut = '';

            for ($at = 4; $at > strlen($urutKe); $at--) {
                $noUrut .= '0';
            }

            if ($rBiodata != NULL) {
                $this->action(array(
                    'table' => 'data_biodata', 'type' => $this->UPDATE,
                    'data' => array('id' => date('Ymd') . $noUrut . $urutKe), 'at' => array('kode' => $rBiodata->kode)
                ));
            }
        }

        return $isSave;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $jabatan = array(
            'field' => 'jabatan-input', 'label' => 'Data Jabatan',
            'rules' => 'trim|required'
        );
        $ke = array(
            'field' => 'ke-input', 'label' => 'Kontrak Ke-',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $berlaku = array(
            'field' => 'berlaku-input', 'label' => 'Tanggal Berlaku',
            'rules' => 'trim|required'
        );
        $habis = array(
            'field' => 'habis-input', 'label' => 'Tanggal Berakhir',
            'rules' => 'trim|required'
        );
        $masuk = array(
            'field' => 'masuk-input', 'label' => 'Jam Masuk',
            'rules' => 'trim|max_length[5]|required'
        );
        $selesai = array(
            'field' => 'selesai-input', 'label' => 'Jam Pulang',
            'rules' => 'trim|max_length[5]|required'
        );
        $gaji = array(
            'field' => 'gaji-input', 'label' => 'Gaji Pegawai',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $fee = array(
            'field' => 'fee-input', 'label' => 'Management Fee',
            'rules' => 'trim|max_length[11]|numeric|required'
        );

        return array($kode, $proyek, $biodata, $jabatan, $ke, $berlaku, $habis, $masuk, $selesai, $gaji, $fee);
    }

    public function getData($kode) {
        $params = explode('___', $kode);
        $urutKe = count($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'biodata' => $params[0], 'status' => 'PKWT'), 'sort' => 'berlaku desc')));
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '', 'jabatan' => '',
            'status' => 'PKWT', 'ke' => ++$urutKe, 'berlaku' => '', 'habis' => '', 'masuk' => '', 'selesai' => '',
            'terdata' => date('Y-m-d'), 'gaji' => 0, 'fee' => 0,
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $params[1])));
        $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $params[0])));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'biodata' => $record->biodata, 'jabatan' => $record->jabatan,
                'status' => $record->status, 'ke' => $record->ke,
                'berlaku' => $this->formatdate->getDate($record->berlaku, TRUE),
                'habis' => $this->formatdate->getDate($record->habis, TRUE),
                'masuk' => $record->masuk, 'selesai' => $record->selesai,
                'gaji' => self::_toRp($record->gaji), 'fee' => self::_fixDec($record->fee),
                'terdata' => $record->terdata,
                'terpakai' => $record->terpakai
            );
        }

        if ($rBiodata != NULL) {
            $data['id'] = strtoupper($rBiodata->id);
            $data['nama'] = ucwords($rBiodata->nama);
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'biodata' => $query), 'sort' => 'berlaku desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));
            $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $record->jabatan)));

            if ($rProyek != NULL && $rBiodata != NULL && $rJabatan != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'jabatan' => ucwords($rJabatan->jabatan),
                    'status' => strtoupper($record->status) . (($record->status === 'PKWT') ? '-' . $record->ke : ''),
                    'berlaku' => $this->formatdate->getDate($record->berlaku) . (($record->status === 'PKWT') ? ' - ' . $this->formatdate->getDate($record->habis) : ''),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

    private function _fixDec($value) {
        return str_replace('.00', '', $value);
    }

}
