<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelproyek
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelProyek extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_info';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Nama Proyek',
            'rules' => 'trim|max_length[100]|required'
        );
        $tipe = array(
            'field' => 'tipe-input', 'label' => 'Data Jenis Proyek',
            'rules' => 'trim|required'
        );
        $lokasi = array(
            'field' => 'lokasi-input', 'label' => 'Data Lokasi',
            'rules' => 'trim|required'
        );
        $kendaraan = array(
            'field' => 'kendaraan-input', 'label' => 'Data Kendaraan',
            'rules' => 'trim|required'
        );
        $inap = array(
            'field' => 'invoice_inap-input', 'label' => 'Muat Inap',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $skr = array(
            'field' => 'invoice_skr-input', 'label' => 'SKR',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $bpjs = array(
            'field' => 'invoice_bpjs-input', 'label' => 'BPJS',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $pengiriman = array(
            'field' => 'invoice_pengiriman-input', 'label' => 'Insentif Pengiriman',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $ritase = array(
            'field' => 'invoice_ritase-input', 'label' => 'Ritase 2',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $pph = array(
            'field' => 'invoice_pph-input', 'label' => 'PPH23',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $ppn = array(
            'field' => 'invoice_ppn-input', 'label' => 'PPN',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $umk = array(
            'field' => 'invoice_umk-input', 'label' => 'UMK',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $thr = array(
            'field' => 'invoice_thr-input', 'label' => 'THR',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $supervisi = array(
            'field' => 'invoice_supervisi-input', 'label' => 'Supervisi',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $seragam = array(
            'field' => 'invoice_seragam-input', 'label' => 'Seragam',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $feeKorlap = array(
            'field' => 'invoice_fee_korlap-input', 'label' => 'Management Fee Korlap',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $feeDriver = array(
            'field' => 'invoice_fee_driver-input', 'label' => 'Management Fee Driver',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $feeHelper = array(
            'field' => 'invoice_fee_helper-input', 'label' => 'Management Fee Helper',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $gKorlap = array(
            'field' => 'gaji_korlap-input', 'label' => 'Gaji Korlap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $mKorlap = array(
            'field' => 'makan_korlap-input', 'label' => 'Uang Makan Korlap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gDriver = array(
            'field' => 'gaji_driver-input', 'label' => 'Gaji Driver',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $mDriver = array(
            'field' => 'makan_driver-input', 'label' => 'Uang Makan Driver',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gHelper = array(
            'field' => 'gaji_helper-input', 'label' => 'Gaji Helper',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $mHelper = array(
            'field' => 'makan_helper-input', 'label' => 'Uang Makan Helper',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gAdmin = array(
            'field' => 'gaji_admin-input', 'label' => 'Gaji Admin',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $mAdmin = array(
            'field' => 'makan_admin-input', 'label' => 'Uang Makan Admin',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gStaf = array(
            'field' => 'gaji_staf-input', 'label' => 'Gaji Staf',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $mStaf = array(
            'field' => 'makan_staf-input', 'label' => 'Uang Makan Staf',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $lDriver = array(
            'field' => 'lembur_driver-input', 'label' => 'Uang Lembur Driver',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $lHelper = array(
            'field' => 'lembur_helper-input', 'label' => 'Uang Lembur Helper',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $lKorlap = array(
            'field' => 'lembur_korlap-input', 'label' => 'Uang Lembur Korlap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $glDriver = array(
            'field' => 'gaji_lembur_driver-input', 'label' => 'Uang Lembur Driver',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $glHelper = array(
            'field' => 'gaji_lembur_helper-input', 'label' => 'Uang Lembur Helper',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $glKorlap = array(
            'field' => 'gaji_lembur_korlap-input', 'label' => 'Uang Lembur Korlap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gInap = array(
            'field' => 'gaji_inap-input', 'label' => 'Shipment Muat Inap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gSKR = array(
            'field' => 'gaji_skr-input', 'label' => 'Shipment SKR',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gRitase = array(
            'field' => 'gaji_ritase-input', 'label' => 'Shipment Ritase 2',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $gInsentif = array(
            'field' => 'gaji_insentif-input', 'label' => 'Shipment Insentif Pengiriman',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array(
            $kode, $proyek, $tipe, $lokasi, $kendaraan, $inap, $skr, $bpjs, $pengiriman, $ritase, $pph, $ppn, $umk, $thr, $supervisi,
            $seragam, $feeKorlap, $feeDriver, $feeHelper, $gKorlap, $mKorlap, $gDriver, $mDriver, $gHelper, $mHelper, $gAdmin, $mAdmin,
            $gStaf, $mStaf, $lDriver, $lHelper, $lKorlap, $glDriver, $glHelper, $glKorlap, $gInap, $gInsentif, $gRitase, $gSKR
        );
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'alamat' => '', 'tipe' => '', 'lokasi' => '', 'kendaraan' => '',
            'invoice_inap' => '', 'invoice_skr' => '', 'invoice_bpjs' => '', 'invoice_pengiriman' => '',
            'invoice_ritase' => '', 'invoice_pph' => '', 'invoice_ppn' => '', 'invoice_umk' => '',
            'invoice_thr' => '', 'invoice_supervisi' => '', 'invoice_seragam' => '',
            'invoice_fee_korlap' => '', 'invoice_fee_driver' => '', 'invoice_fee_helper' => '',
            'gaji_korlap' => '', 'makan_korlap' => '', 'gaji_driver' => '', 'makan_driver' => '',
            'gaji_helper' => '', 'makan_helper' => '', 'gaji_admin' => '', 'makan_admin' => '',
            'gaji_inap' => '', 'gaji_skr' => '', 'gaji_ritase' => '', 'gaji_insentif' => '',
            'gaji_staf' => '', 'makan_staf' => '',
            'lembur_driver' => '', 'lembur_helper' => '', 'lembur_korlap' => '',
            'gaji_lembur_driver' => '', 'gaji_lembur_helper' => '', 'gaji_lembur_korlap' => '',
            'terpakai' => 1,
            'tipeText' => '', 'lokasiText' => '', 'kendaraanText' => ''
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $rLokasi = $this->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $record->lokasi)));
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));
            $rKendaraan = $this->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $record->kendaraan)));
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => ucwords($record->proyek),
                'alamat' => $record->alamat,
                'tipe' => $record->tipe, 'lokasi' => $record->lokasi, 'kendaraan' => $record->kendaraan,
                'invoice_inap' => self::_fixDec($record->invoice_inap), 'invoice_skr' => self::_fixDec($record->invoice_skr),
                'invoice_bpjs' => self::_fixDec($record->invoice_bpjs), 'invoice_ritase' => self::_fixDec($record->invoice_ritase),
                'invoice_pengiriman' => self::_fixDec($record->invoice_pengiriman),
                'invoice_fee_korlap' => self::_fixDec($record->invoice_fee_korlap),
                'invoice_fee_driver' => self::_fixDec($record->invoice_fee_driver),
                'invoice_fee_helper' => self::_fixDec($record->invoice_fee_helper),
                'invoice_pph' => self::_fixDec($record->invoice_pph), 'invoice_ppn' => self::_fixDec($record->invoice_ppn),
                'invoice_umk' => self::_toRp($record->invoice_umk), 'invoice_thr' => self::_toRp($record->invoice_thr),
                'invoice_supervisi' => self::_toRp($record->invoice_supervisi), 'invoice_seragam' => self::_toRp($record->invoice_seragam),
                'gaji_korlap' => self::_toRp($record->gaji_korlap), 'makan_korlap' => self::_toRp($record->makan_korlap),
                'gaji_driver' => self::_toRp($record->gaji_driver), 'makan_driver' => self::_toRp($record->makan_driver),
                'gaji_helper' => self::_toRp($record->gaji_helper), 'makan_helper' => self::_toRp($record->makan_helper),
                'gaji_admin' => self::_toRp($record->gaji_admin), 'makan_admin' => self::_toRp($record->makan_admin),
                'gaji_staf' => self::_toRp($record->gaji_staf), 'makan_staf' => self::_toRp($record->makan_staf),
                'lembur_driver' => self::_toRp($record->lembur_driver), 'gaji_lembur_driver' => self::_toRp($record->gaji_lembur_driver),
                'lembur_helper' => self::_toRp($record->lembur_helper), 'gaji_lembur_helper' => self::_toRp($record->gaji_lembur_helper),
                'lembur_korlap' => self::_toRp($record->lembur_korlap), 'gaji_lembur_korlap' => self::_toRp($record->gaji_lembur_korlap),
                'gaji_inap' => self::_toRp($record->gaji_inap), 'gaji_skr' => self::_toRp($record->gaji_skr),
                'gaji_ritase' => self::_toRp($record->gaji_ritase), 'gaji_insentif' => self::_toRp($record->gaji_insentif),
                'terpakai' => $record->terpakai,
                'tipeText' => '', 'lokasiText' => '', 'kendaraanText' => ''
            );

            if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                $data['tipeText'] = ucwords($rTipe->tipe);
                $data['lokasiText'] = ucwords($rLokasi->lokasi);
                $data['kendaraanText'] = ucwords($rKendaraan->kendaraan);
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'proyek asc')) as $record) {
            $rLokasi = $this->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $record->lokasi)));
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));
            $rKendaraan = $this->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $record->kendaraan)));

            if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="exportBtn btn btn-info btn-flat">Export</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($record->proyek),
                    'tipe' => ucwords($rTipe->tipe), 'lokasi' => ucwords($rLokasi->lokasi), 'kendaraan' => ucwords($rKendaraan->kendaraan),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('proyek' => $query), 'sort' => 'proyek asc')) as $record) {
            $rLokasi = $this->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $record->lokasi)));
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));
            $rKendaraan = $this->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $record->kendaraan)));

            if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                array_push($data, array(
                    'id' => $record->kode,
                    'text' => ucwords($record->proyek) . ' | ' . ucwords($rTipe->tipe) . ' | ' . ucwords($rLokasi->lokasi) . ' | ' . ucwords($rKendaraan->kendaraan)
                ));
            }
        }

        return $data;
    }

    private function _fixDec($value) {
        return str_replace('.00', '', $value);
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
