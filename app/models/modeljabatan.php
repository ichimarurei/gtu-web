<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modeljabatan
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelJabatan extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_jabatan';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $jabatan = array(
            'field' => 'jabatan-input', 'label' => 'Jabatan',
            'rules' => 'trim|max_length[100]|required'
        );
        $komisi = array(
            'field' => 'komisi-input', 'label' => 'Skill',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $keterangan = array(
            'field' => 'keterangan-input', 'label' => 'Keterangan',
            'rules' => 'trim|max_length[255]'
        );

        return array($kode, $jabatan, $keterangan, $komisi);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'jabatan' => '', 'keterangan' => '', 'komisi' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'jabatan' => ucwords($record->jabatan), 'keterangan' => $record->keterangan,
                'komisi' => 'Rp. ' . number_format($record->komisi, 0, ',', '.'),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'jabatan asc')) as $record) {
            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $data[] = array(
                'kode' => $record->kode,
                'jabatan' => ucwords($record->jabatan),
                'komisi' => 'Rp. ' . number_format($record->komisi, 0, ',', '.'),
                'aksi' => $linkBtn
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('jabatan' => $query), 'sort' => 'jabatan asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => ucwords($record->jabatan)));
        }

        return $data;
    }

}
