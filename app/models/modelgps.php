<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelgps
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelGPS extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_gps';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Tanggal Catat',
            'rules' => 'trim|required'
        );
        $long = array(
            'field' => 'long-input', 'label' => 'Longitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $lat = array(
            'field' => 'lat-input', 'label' => 'Latitude',
            'rules' => 'trim|max_length[50]|required'
        );

        return array($kode, $proyek, $biodata, $waktu, $long, $lat);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '',
            'long' => '', 'lat' => '',
            'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'waktu' => $record->waktu, 'biodata' => $record->biodata,
                'long' => $record->long, 'lat' => $record->lat,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'waktu desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->long . '___' . $record->lat . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'waktu' => $this->formatdate->getDateTime($record->waktu),
                    'longlat' => '<b>longitude:</b> ' . $record->long . ', <b>latitude:</b> ' . $record->lat,
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
