<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelgaji
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelGaji extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_gaji';
    }

    public function doAction($params) {
        $this->setValues($params);
        $isSave = $this->doSave();

        if ($isSave) {
            if ($params['action-input'] == $this->CREATE) {
                foreach ($this->getList(array('table' => 'data_proyek_kasbon', 'where' => array('terpakai' => 1, 'proyek' => $params['proyek-input'], 'status' => 'terpotong', 'pegawai' => $params['biodata-input']), 'sort' => 'tanggal desc')) as $record) {
                    $rTermin = $this->getRecord(array('table' => 'data_proyek_cicilan', 'where' => array('kasbon' => $record->kode, 'terpakai' => 1)));

                    if ($rTermin != NULL) {
                        if ($rTermin->termin < $record->cicil) {
                            $this->action(array(
                                'table' => 'data_proyek_cicilan', 'type' => $this->UPDATE,
                                'data' => array('termin' => ($rTermin->termin + 1)), 'at' => array('kode' => $rTermin->kode)
                            ));
                        }
                    }
                }

                foreach ($this->getList(array('table' => 'data_proyek_kasbon', 'where' => array('terpakai' => 1, 'proyek' => $params['proyek-input'], 'status' => 'terima', 'pegawai' => $params['biodata-input']), 'sort' => 'tanggal desc')) as $record) {
                    $this->action(array(
                        'table' => 'data_proyek_kasbon', 'type' => $this->UPDATE,
                        'data' => array('status' => 'terpotong'), 'at' => array('kode' => $record->kode)
                    ));

                    if ($this->getRecord(array('table' => 'data_proyek_cicilan', 'where' => array('kasbon' => $record->kode, 'terpakai' => 1))) == NULL) {
                        $this->action(array(
                            'table' => 'data_proyek_cicilan', 'type' => $this->CREATE,
                            'data' => array(
                                'entitas' => 0, 'kode' => random_string('unique'),
                                'termin' => 1, 'kasbon' => $record->kode, 'terpakai' => 1
                            )
                        ));
                    }
                }

                foreach ($this->getList(array('table' => 'data_proyek_klaim', 'where' => array('terpakai' => 1, 'proyek' => $params['proyek-input'], 'status' => 'terima', 'pegawai' => $params['biodata-input']), 'sort' => 'tanggal desc')) as $record) {
                    $this->action(array(
                        'table' => 'data_proyek_klaim', 'type' => $this->UPDATE,
                        'data' => array('status' => 'terbayar'), 'at' => array('kode' => $record->kode)
                    ));
                }
            }
        }

        return $isSave;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $invoice = array(
            'field' => 'invoice-input', 'label' => 'Data Invoice',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Pegawai',
            'rules' => 'trim|required'
        );
        $pHadir = array(
            'field' => 'pot_hadir-input', 'label' => 'Potongan Hadir',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $pLain = array(
            'field' => 'pot_lain_isi-input', 'label' => 'Potongan Lainnya',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $pHadir, $pLain, $invoice, $biodata, $proyek);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'invoice' => '', 'biodata' => '', 'status' => 'proses',
            'gaji' => '', 'makan' => '', 'lembur' => '',
            'inap' => '', 'insentif' => '', 'ritase' => '',
            'skr' => '', 'klaim' => '', 'pot_kasbon' => '',
            'waktu' => date('Y-m-d H:i:s'), 'pot_hadir' => '', 'pot_lain_info' => '', 'pot_lain_isi' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'invoice' => $record->invoice, 'biodata' => $record->biodata,
                'waktu' => $record->waktu, 'status' => $record->status, 'pot_lain_info' => $record->pot_lain_info,
                'gaji' => self::_toRp($record->gaji),
                'makan' => self::_toRp($record->makan),
                'lembur' => self::_toRp($record->lembur),
                'inap' => self::_toRp($record->inap),
                'insentif' => self::_toRp($record->insentif),
                'ritase' => self::_toRp($record->ritase),
                'skr' => self::_toRp($record->skr),
                'klaim' => self::_toRp($record->klaim),
                'pot_kasbon' => self::_toRp($record->pot_kasbon),
                'pot_lain_isi' => self::_toRp($record->pot_lain_isi),
                'pot_hadir' => self::_toRp($record->pot_hadir),
                'terpakai' => $record->terpakai
            );

            if ($record->entitas > 0) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek, 'terpakai' => 1)));
                $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));
                $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->biodata, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                $period = explode(' ', $this->formatdate->getDate($record->waktu));
                $plus = $record->gaji + $record->makan + $record->lembur + $record->inap + $record->ritase + $record->skr + $record->insentif + $record->klaim;
                $minus = $record->pot_hadir + $record->pot_kasbon + $record->pot_lain_isi;
                $total = $plus - $minus;
                $sebagai = '-';
                $data['periode'] = strtoupper($period[1]) . ' ' . $period[2];
                $data['plus'] = self::_toRp($plus);
                $data['minus'] = self::_toRp($minus);
                $data['total'] = self::_toRp($total) . ',-';
                $data['terbilang'] = $this->terbilang->format($total);

                if ($rProyek != NULL && $rBiodata != NULL && $rKontrak != NULL) {
                    // kontrak masih berlaku
                    if ($rKontrak->habis >= date('Y-m-d')) {
                        if ($rKontrak->status === 'PKWT') {
                            $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));

                            if ($rJabatan != NULL) {
                                $sebagai = ucwords($rJabatan->jabatan);
                            }
                        }
                    }

                    $data['nama'] = strtoupper($rBiodata->nama);
                    $data['nik'] = strtoupper($rBiodata->id);
                    $data['jabatan'] = strtoupper($sebagai);
                }
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = explode('___', $query);
        $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $queries[0], 'terpakai' => 1)));

        if ($rProyek != NULL) {
            $lPresensi = array();
            $lLembur = array();
            $status = array('proses' => 'PROSES', 'bayar' => 'SUDAH DIBAYAR', 'na' => 'BELUM DIPROSES');
            $hariKerja = 0;
            $isUseDay = FALSE;
            $rPresensi = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $queries[1], 'jenis' => 'presensi', 'status' => 'setuju', 'terpakai' => 1)));
            $rLembur = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $queries[2], 'jenis' => 'lembur', 'status' => 'setuju', 'terpakai' => 1)));

            if ($rPresensi != NULL) {
                $objDari = new DateTime($rPresensi->dari);
                $objHingga = new DateTime($rPresensi->hingga);
                $diff = $objHingga->diff($objDari);
                $hariKerja = (($diff != NULL) ? (intval($diff->d) + 1) : 0);

                if ($hariKerja > 26) {
                    $hariKerja = 26;
                }

                $isUseDay = ($hariKerja < 23);

                foreach ($this->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rPresensi->kode, 'jenis' => 'presensi', 'terpakai' => 1))) as $record) {
                    $rData = $this->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $record->data)));

                    if ($rData != NULL) {
                        $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rData->biodata, 'terpakai' => 1)));

                        if ($rBiodata != NULL) {
                            $waktu = explode(' ', $rData->waktu);
                            $lPresensi[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
                        }
                    }
                }
            }

            if ($rLembur != NULL) {
                foreach ($this->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rLembur->kode, 'jenis' => 'lembur', 'terpakai' => 1))) as $record) {
                    $rData = $this->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('kode' => $record->data)));

                    if ($rData != NULL) {
                        $dAbsen = $this->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $rData->presensi)));

                        if ($dAbsen != NULL) {
                            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $dAbsen->biodata, 'terpakai' => 1)));

                            if ($rBiodata != NULL) {
                                $waktu = explode(' ', $rData->waktu);
                                $lLembur[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
                            }
                        }
                    }
                }
            }

            foreach ($this->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                $isShow = FALSE;
                $sebagai = '';
                $rJabatan = NULL;

                if ($rKontrak != NULL && $rProyek != NULL) {
                    // kontrak masih berlaku
                    if ($rKontrak->habis >= date('Y-m-d')) {
                        $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                        $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                        $isShow = TRUE;
                    }
                }

                if ($isShow) {
                    $gajian = 0;
                    $lemburan = 0;
                    $makanan = 0;
                    $intInap = 0;
                    $intSKR = 0;
                    $intRit2 = 0;
                    $intUpah = 0;
                    $payInap = 0;
                    $paySKR = 0;
                    $payRit2 = 0;
                    $payUpah = 0;
                    $klaim = 0;
                    $kasbon = 0;
                    $total = 0;
                    $rGaji = $this->getRecord(array('table' => $this->table, 'where' => array('proyek' => $rProyek->kode, 'invoice' => $queries[3], 'biodata' => $record->kode, 'terpakai' => 1)));

                    if ($rGaji == NULL) {
                        if ($sebagai === 'Helper') {
                            $gajian = $rProyek->gaji_helper;
                            $makanan = $rProyek->makan_helper;
                        } else if ($sebagai === 'Driver') {
                            $gajian = $rProyek->gaji_driver;
                            $makanan = $rProyek->makan_driver;
                        } else if ($sebagai === 'Korlap') {
                            $gajian = $rProyek->gaji_korlap;
                            $makanan = $rProyek->makan_korlap;
                        } else if ($sebagai === 'Staff') {
                            $gajian = $rProyek->gaji_staf;
                            $makanan = $rProyek->makan_staf;
                        } else if ($sebagai === 'Admin') {
                            $gajian = $rProyek->gaji_admin;
                            $makanan = $rProyek->makan_admin;
                        }

                        if ($rKontrak != NULL) {
                            if ($rKontrak->gaji > 0) {
                                $gajian = $rKontrak->gaji;
                            }
                        }

                        $gajiHarian = round($gajian / 26);
                        $makanHarian = round($makanan / 26);

                        if ($isUseDay) { // reset
                            $gajian = 0;
                            $makanan = 0;
                        }

                        if (isset($lPresensi[$record->kode])) {
                            foreach ($lPresensi[$record->kode] as $kPresensi => $vPresensi) {
                                $isLembur = FALSE;
                                $rQty = $this->getRecord(array('table' => 'data_proyek_qty', 'where' => array('proyek' => $rProyek->kode, 'biodata' => $record->kode, 'DATE(waktu)' => $kPresensi, 'terpakai' => 1)));

                                if (in_array($vPresensi[0]->status, array('hadir', 'sakit'))) {
                                    if ($isUseDay) {
                                        $gajian += $gajiHarian;

                                        if ($vPresensi[0]->status === 'hadir') {
                                            $makanan += $makanHarian;
                                        }
                                    }
                                } else {
                                    if (!$isUseDay) {
                                        $gajian -= $gajiHarian;
                                        $makanan -= $makanHarian;
                                    }
                                }

                                if ($rQty != NULL) {
                                    $intInap += $rQty->inap;
                                    $intSKR += $rQty->skr;
                                    $intRit2 += $rQty->ritase;
                                    $intUpah += $rQty->insentif;
                                }

                                if (isset($lLembur[$record->kode][$kPresensi])) {
                                    $isLembur = TRUE;
                                }

                                if ($isLembur) {
                                    if ($sebagai === 'Helper') {
                                        $lemburan += $rProyek->gaji_lembur_helper;
                                    } else if ($sebagai === 'Driver') {
                                        $lemburan += $rProyek->gaji_lembur_driver;
                                    } else if ($sebagai === 'Korlap') {
                                        $lemburan += $rProyek->gaji_lembur_korlap;
                                    }
                                }
                            }
                        }

                        foreach ($this->getList(array('table' => 'data_proyek_kasbon', 'where' => array('terpakai' => 1, 'proyek' => $rProyek->kode, 'status' => 'terima', 'pegawai' => $record->kode), 'sort' => 'tanggal desc')) as $rKasbon) {
                            if ($this->getRecord(array('table' => 'data_proyek_cicilan', 'where' => array('kasbon' => $rKasbon->kode, 'terpakai' => 1))) == NULL) {
                                $kasbon += round($rKasbon->nominal / $rKasbon->cicil);
                            }
                        }

                        foreach ($this->getList(array('table' => 'data_proyek_kasbon', 'where' => array('terpakai' => 1, 'proyek' => $rProyek->kode, 'status' => 'terpotong', 'pegawai' => $record->kode), 'sort' => 'tanggal desc')) as $rKasbon) {
                            $rTermin = $this->getRecord(array('table' => 'data_proyek_cicilan', 'where' => array('kasbon' => $rKasbon->kode, 'terpakai' => 1)));

                            if ($rTermin != NULL) {
                                if ($rTermin->termin < $rKasbon->cicil) {
                                    $kasbon += round($rKasbon->nominal / $rKasbon->cicil);
                                }
                            }
                        }

                        foreach ($this->getList(array('table' => 'data_proyek_klaim', 'where' => array('terpakai' => 1, 'proyek' => $rProyek->kode, 'status' => 'terima', 'pegawai' => $record->kode), 'sort' => 'tanggal desc')) as $rKlaim) {
                            $klaim += $rKlaim->nominal;
                        }

                        $payRit2 = $rProyek->gaji_ritase * $intRit2;
                        $payInap = $rProyek->gaji_inap * $intInap;
                        $paySKR = $rProyek->gaji_skr * $intSKR;
                        $payUpah = $rProyek->gaji_insentif * $intUpah;
                        $total = ($gajian + $makanan + $lemburan + $payInap + $payRit2 + $paySKR + $payUpah + $klaim) - $kasbon;
                    } else {
                        $total = ($rGaji->gaji + $rGaji->makan + $rGaji->lembur + $rGaji->inap + $rGaji->ritase + $rGaji->skr + $rGaji->insentif + $rGaji->klaim) - ($rGaji->pot_hadir + $rGaji->pot_kasbon + $rGaji->pot_lain_isi);
                    }

                    $linkBtn = '<a href="' . (($rGaji == NULL) ? '0' : $rGaji->kode) . '" '
                            . 'data-gaji="' . (($rGaji == NULL) ? self::_toRp($gajian) : '0') . '" data-makan="' . (($rGaji == NULL) ? self::_toRp($makanan) : '0') . '" '
                            . 'data-lemburan="' . (($rGaji == NULL) ? self::_toRp($lemburan) : '0') . '" data-inap="' . (($rGaji == NULL) ? self::_toRp($payInap) : '0') . '" '
                            . 'data-insentif="' . (($rGaji == NULL) ? self::_toRp($payUpah) : '0') . '" data-skr="' . (($rGaji == NULL) ? self::_toRp($paySKR) : '0') . '" '
                            . 'data-ritase="' . (($rGaji == NULL) ? self::_toRp($payRit2) : '0') . '" data-klaim="' . (($rGaji == NULL) ? self::_toRp($klaim) : '0') . '" '
                            . 'data-kasbon="' . (($rGaji == NULL) ? self::_toRp($kasbon) : '0') . '" '
                            . 'data-nik="' . strtoupper($record->id) . '" data-nama="' . ucwords($record->nama) . '" data-who="' . $record->kode . '" '
                            . 'class="actionBtn btn btn-primary btn-flat">Proses</a>';
                    $linkBtn .= ' <a href="' . (($rGaji == NULL) ? '0' : $rGaji->kode) . '" class="printBtn btn btn-success btn-flat">Cetak</a>';
                    $data[] = array(
                        'kode' => (($rGaji == NULL) ? '0' : $rGaji->kode),
                        'id' => strtoupper($record->id),
                        'biodata' => ucwords($record->nama),
                        'tanggal' => (($rGaji != NULL) ? $this->formatdate->getDate($rGaji->waktu) : ''),
                        'status' => $status[(($rGaji != NULL) ? $rGaji->status : 'na')],
                        'nominal' => self::_toRp($total),
                        'jabatan' => $sebagai,
                        'aksi' => $linkBtn
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
