<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelrekap
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelRekap extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_rekap';
    }

    public function doAction($params) {
        $this->setValues($params);
        $isSave = $this->doSave();

        if ($isSave) {
            if (intval($params['terpakai-input']) > 0) {
                if ($params['jenis-input'] === 'presensi') {
                    $where = array(
                        'terpakai' => 1, 'DATE(waktu) >=' => $params['dari-input'], 'DATE(waktu) <=' => $params['hingga-input'],
                        'proyek' => $params['proyek-input']
                    );

                    foreach ($this->getList(array('table' => 'data_presensi_info', 'where' => $where, 'sort' => 'waktu desc')) as $record) {
                        if ($params['status-input'] === 'ajuan') {
                            $this->action(array(
                                'table' => 'data_presensi_arsip', 'type' => $this->CREATE,
                                'data' => array(
                                    'entitas' => 0, 'kode' => random_string('unique'),
                                    'rekap' => $params['kode-input'], 'data' => $record->kode, 'jenis' => $params['jenis-input'],
                                    'terpakai' => 1
                                )
                            ));
                        } else if ($params['status-input'] === 'setuju') {
                            $this->action(array(
                                'table' => 'data_presensi_info', 'type' => $this->UPDATE,
                                'data' => array('terpakai' => 0), 'at' => array('kode' => $record->kode)
                            ));
                        }
                    }
                } else { // lembur
                    foreach ($this->getList(array('table' => 'data_presensi_lembur', 'where' => array('terpakai' => 1, 'YEAR(waktu)' => date('Y')), 'sort' => 'waktu desc')) as $record) {
                        $where = array(
                            'terpakai' => 1, 'DATE(waktu) >=' => $params['dari-input'], 'DATE(waktu) <=' => $params['hingga-input'],
                            'proyek' => $params['proyek-input'], 'kode' => $record->presensi
                        );

                        $rPresensi = $this->getRecord(array('table' => 'data_presensi_info', 'where' => $where));

                        if ($rPresensi != NULL) {
                            if ($params['status-input'] === 'ajuan') {
                                $this->action(array(
                                    'table' => 'data_presensi_arsip', 'type' => $this->CREATE,
                                    'data' => array(
                                        'entitas' => 0, 'kode' => random_string('unique'),
                                        'rekap' => $params['kode-input'], 'data' => $record->kode, 'jenis' => $params['jenis-input'],
                                        'terpakai' => 1
                                    )
                                ));
                            } else if ($params['status-input'] === 'setuju') {
                                $this->action(array(
                                    'table' => 'data_presensi_lembur', 'type' => $this->UPDATE,
                                    'data' => array('terpakai' => 0), 'at' => array('kode' => $record->kode)
                                ));
                            }
                        }
                    }
                }
            }
        }

        return $isSave;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $dari = array(
            'field' => 'dari-input', 'label' => 'Tanggal Awal',
            'rules' => 'trim|required'
        );
        $hingga = array(
            'field' => 'hingga-input', 'label' => 'Tanggal Hingga',
            'rules' => 'trim|required'
        );

        return array($kode, $proyek, $dari, $hingga);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'dari' => '', 'hingga' => '', 'waktu' => date('Y-m-d H:i:s'),
            'proyek' => '', 'status' => 'ajuan', 'jenis' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'dari' => $record->dari, 'hingga' => $record->hingga,
                'proyek' => $record->proyek, 'status' => $record->status, 'waktu' => $record->waktu,
                'jenis' => $record->jenis, 'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $status = array('ajuan' => 'PROSES', 'setuju' => 'DITERIMA', 'tolak' => 'DITOLAK');
        $queries = explode('___', $query);
        $where = array('terpakai' => 1, 'proyek' => $queries[0], 'jenis' => $queries[1]);

        if (isset($queries[2])) {
            $where['status'] = 'setuju';
            unset($where['jenis']);
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'waktu desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

            if ($rProyek != NULL) {
                $pickBtn = '<a href="' . $record->kode . '" data-jenis="' . $record->jenis;
                $pickBtn .= '" data-info="' . ucwords($rProyek->proyek) . ' #<b>';
                $pickBtn .= $this->formatdate->getDate($record->dari) . ' - ' . $this->formatdate->getDate($record->hingga);
                $pickBtn .= '</b>" class="actionBtn btn btn-primary btn-flat">Pilih</a>';
                $linkBtn = '<a href="' . $record->kode . '/' . $record->status . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'jenis' => (($record->jenis === 'presensi') ? 'ABSENSI' : strtoupper($record->jenis)),
                    'proyek' => ucwords($rProyek->proyek),
                    'tanggal' => $this->formatdate->getDate($record->dari) . ' - ' . $this->formatdate->getDate($record->hingga),
                    'waktu' => $this->formatdate->getDate($record->waktu),
                    'status' => $status[$record->status], 'tahap' => $record->status,
                    'aksi' => $linkBtn, 'proses' => $pickBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
