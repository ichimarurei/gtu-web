<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelremarks
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelRemarks extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_catatan';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $pengubah = array(
            'field' => 'pengubah-input', 'label' => 'Data Admin',
            'rules' => 'trim|required'
        );
        $presensi = array(
            'field' => 'presensi-input', 'label' => 'Data Presensi/Lembur',
            'rules' => 'trim|required'
        );
        $keterangan = array(
            'field' => 'keterangan-input', 'label' => 'Keterangan',
            'rules' => 'trim|required'
        );

        return array($kode, $pengubah, $presensi, $keterangan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'pengubah' => '', 'presensi' => $kode, 'keterangan' => '', 'untuk' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('presensi' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'pengubah' => $record->pengubah, 'presensi' => $record->presensi,
                'keterangan' => $record->keterangan, 'untuk' => $record->untuk,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        return array();
    }

    public function getPilih($query) {
        return array();
    }

}
