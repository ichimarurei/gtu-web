<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelspsl
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelSPSL extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_spsl';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal', $this->formatdate->setDate($params['tanggal-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'Nomor Dokumen',
            'rules' => 'trim|max_length[100]|required'
        );
        $tanggal = array(
            'field' => 'tanggal-input', 'label' => 'Tanggal Terbit Dokumen',
            'rules' => 'trim|required'
        );
        $keterangan = array(
            'field' => 'keterangan-input', 'label' => 'Keterangan',
            'rules' => 'trim|required'
        );
        $ke = array(
            'field' => 'ke-input', 'label' => 'Dokumen Ke',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $nomor, $proyek, $biodata, $tanggal, $keterangan, $ke);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '', 'tanggal' => '', 'ke' => '', 'jenis' => '',
            'nomor' => '', 'keterangan' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'jenis' => $record->jenis, 'biodata' => $record->biodata,
                'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                'nomor' => strtoupper($record->nomor), 'ke' => $record->ke, 'keterangan' => $record->keterangan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $where = array('terpakai' => 1, 'YEAR(tanggal)' => date('Y'));

        if ($query != NULL) {
            $queries = explode('___', $query);
            $where['jenis'] = $queries[0];

            if ($queries[1] !== 'all') {
                $where['proyek'] = $queries[1];
            }
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'tanggal desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'tanggal' => $this->formatdate->getDate($record->tanggal),
                    'nomor' => strtoupper($record->nomor), 'ke' => strtoupper($record->jenis) . '-' . $record->ke,
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $queries = explode('___', $query);

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(tanggal)' => date('Y'), 'jenis' => $queries[0]), 'find' => array('nomor' => $queries[1]), 'sort' => 'nomor asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nomor)));
        }

        return $data;
    }

}
