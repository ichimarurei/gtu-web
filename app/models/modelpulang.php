<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelpulang
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelPulang extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_pulang';
    }

    public function doAction($params) {
        $this->setValues($params);
        $isSaved = FALSE;

        if ($params['action-input'] == $this->CREATE) {
            if ($this->getRecord(array('table' => $this->table, 'where' => array('DATE(waktu)' => date('Y-m-d'), 'presensi' => $params['presensi-input'], 'terpakai' => 1))) == NULL) {
                $isSaved = $this->doSave();
            }
        } else {
            $isSaved = $this->doSave();
        }

        return $isSaved;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $presensi = array(
            'field' => 'presensi-input', 'label' => 'Data Presensi',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Waktu',
            'rules' => 'trim|required'
        );

        return array($kode, $presensi, $waktu);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'presensi' => $kode, 'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('presensi' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'presensi' => $record->presensi,
                'waktu' => $record->waktu,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'YEAR(waktu)' => date('Y')), 'sort' => 'waktu desc')) as $record) {
            $where = array('terpakai' => 1, 'kode' => $record->presensi);

            if ($query != NULL) {
                if (strpos($query, '___') !== FALSE) {
                    $queries = explode('___', $query);

                    if ($queries[0] !== 'all') {
                        $where['proyek'] = $queries[0];
                    }

                    if ($queries[1] !== 'all') {
                        $where['biodata'] = $queries[1];

                        if ($queries[2] !== 'x') {
                            $where['YEAR(waktu)'] = $queries[2];
                        }

                        if ($queries[3] !== 'x') {
                            $where['MONTH(waktu)'] = $queries[3];
                        }
                    } else { // periode
                        unset($where['YEAR(waktu)']);
                        unset($where['MONTH(waktu)']);

                        if ($queries[2] !== 'x') {
                            $where['DATE(waktu) >='] = $queries[2];
                        }

                        if ($queries[3] !== 'x') {
                            $where['DATE(waktu) <='] = $queries[3];
                        }
                    }
                }
            }

            $rPresensi = $this->getRecord(array('table' => 'data_presensi_info', 'where' => $where));

            if ($rPresensi != NULL) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rPresensi->proyek)));
                $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rPresensi->biodata)));

                if ($rProyek != NULL && $rBiodata != NULL) {
                    $linkBtn = '<a href="' . $rPresensi->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                    $data[] = array(
                        'kode' => $record->kode,
                        'proyek' => ucwords($rProyek->proyek),
                        'id' => strtoupper($rBiodata->id),
                        'biodata' => ucwords($rBiodata->nama),
                        'waktu' => $this->formatdate->getDateTime($record->waktu),
                        'aksi' => $linkBtn
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
