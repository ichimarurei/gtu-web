<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkasbon
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKasbon extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_kasbon';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal', $this->formatdate->setDate($params['tanggal-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $pegawai = array(
            'field' => 'pegawai-input', 'label' => 'Data Pegawai',
            'rules' => 'trim|required'
        );
        $pemohon = array(
            'field' => 'pemohon-input', 'label' => 'Data Pemohon',
            'rules' => 'trim|required'
        );
        $tanggal = array(
            'field' => 'tanggal-input', 'label' => 'Tanggal Permohonan',
            'rules' => 'trim|required'
        );
        $perihal = array(
            'field' => 'perihal-input', 'label' => 'Perihal Permohonan',
            'rules' => 'trim|required'
        );
        $nominal = array(
            'field' => 'nominal-input', 'label' => 'Nominal Permohonan',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $pegawai, $pemohon, $tanggal, $perihal, $nominal);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'pegawai' => '', 'pemohon' => '', 'status' => 'ajuan',
            'tanggal' => '', 'perihal' => '', 'nominal' => '', 'cicil' => 1, 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'pegawai' => $record->pegawai, 'pemohon' => $record->pemohon,
                'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                'status' => $record->status, 'perihal' => $record->perihal, 'cicil' => $record->cicil,
                'nominal' => self::_toRp($record->nominal),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $status = array('ajuan' => 'PENGAJUAN KASBON', 'tolak' => 'TOLAK', 'terima' => 'TERIMA', 'terpotong' => 'SUDAH DIPOTONG DARI GAJI');

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'tanggal desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pegawai)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'tanggal' => $this->formatdate->getDate($record->tanggal),
                    'status' => $status[$record->status], 'perihal' => $record->perihal,
                    'nominal' => self::_toRp($record->nominal),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
