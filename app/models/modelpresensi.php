<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelpresensi
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelPresensi extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_info';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('waktu', $this->formatdate->setDateTime($params['waktu-input'])); // overwrite
        $isSaved = FALSE;

        if ($params['action-input'] == $this->CREATE) {
            if ($this->getRecord(array('table' => $this->table, 'where' => array('DATE(waktu)' => date('Y-m-d'), 'biodata' => $params['biodata-input'], 'terpakai' => 1))) == NULL) {
                $isSaved = $this->doSave();
            }
        } else {
            $isSaved = $this->doSave();
        }

        return $isSaved;
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Tanggal Absensi',
            'rules' => 'trim|required'
        );

        return array($kode, $proyek, $biodata, $waktu);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'biodata' => '',
            'status' => 'hadir', 'waktu' => $this->formatdate->getDateTime(date('Y-m-d H:i:s'), TRUE),
            'keterangan' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'status' => $record->status, 'biodata' => $record->biodata,
                'waktu' => $this->formatdate->getDateTime($record->waktu, TRUE),
                'keterangan' => $record->keterangan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $where = array('terpakai' => 1, 'YEAR(waktu)' => date('Y'), 'MONTH(waktu)' => date('m'));

        if ($query != NULL) {
            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);

                if ($queries[0] !== 'all') {
                    $where['proyek'] = $queries[0];
                }

                if ($queries[1] !== 'all') {
                    $where['biodata'] = $queries[1];

                    if ($queries[2] !== 'x') {
                        $where['YEAR(waktu)'] = $queries[2];
                    }

                    if ($queries[3] !== 'x') {
                        $where['MONTH(waktu)'] = $queries[3];
                    }
                } else { // periode
                    unset($where['YEAR(waktu)']);
                    unset($where['MONTH(waktu)']);

                    if ($queries[2] !== 'x') {
                        $where['DATE(waktu) >='] = $queries[2];
                    }

                    if ($queries[3] !== 'x') {
                        $where['DATE(waktu) <='] = $queries[3];
                    }
                }
            }
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'waktu desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));
            $rRemarks = $this->getRecord(array('table' => 'data_presensi_catatan', 'where' => array('presensi' => $record->kode)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
                $waktu = explode(' ', $record->waktu);

                if ($waktu[0] === date('Y-m-d') && $record->status === 'hadir') {
                    if ($this->getRecord(array('table' => 'data_presensi_pulang', 'where' => array('DATE(waktu)' => $waktu[0], 'presensi' => $record->kode, 'terpakai' => 1))) == NULL) {
                        $linkBtn .= ' <a href="' . $record->kode . '" class="outBtn btn btn-success btn-flat">Pulang</a>';
                    }

                    if ($this->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('DATE(waktu)' => $waktu[0], 'presensi' => $record->kode, 'terpakai' => 1))) == NULL) {
                        $linkBtn .= ' <a href="' . $record->kode . '" class="otherBtn btn btn-info btn-flat">Lembur</a>';
                    }
                }

                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'waktu' => $this->formatdate->getDateTime($record->waktu),
                    'status' => strtoupper($record->status),
                    'remarks' => (($rRemarks == NULL) ? '' : $rRemarks->keterangan),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
