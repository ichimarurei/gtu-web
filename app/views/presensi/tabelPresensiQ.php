<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Tabel</li><li>Absensi Kehadiran Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
                <span class="ribbon-button-alignment pull-right">
                    <span id="excel-span" class="btn btn-ribbon hidden-xs" data-title="Export"><i class="fa fa-file-excel-o"></i> Export Absensi</span>
                </span>
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Absensi Kehadiran Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-5">
                                        <select style="width:100%" class="select2 form-control" id="bulan-filter">
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select style="width:100%" class="select2 form-control" id="tahun-filter">
                                            <?php for ($thn = 2000; $thn <= intval(date('Y')); $thn++) { ?>
                                                <option><?php echo $thn; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Absensi Kehadiran Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Proyek</th>
                                                    <th>Waktu</th>
                                                    <th>Status</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = 'all___<?php echo $this->session->userdata('_bio'); ?>___x___x';

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/presensi'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "id"},
                    {"data": "biodata"},
                    {"data": "proyek"},
                    {"data": "waktu"},
                    {"data": "status"}, {"data": "remarks"}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#excel-span').click(function () {
                    var proyekan = $('#proyek-filter').val();

                    if (proyekan == null) {
                        swal({
                            title: 'Peringatan',
                            html: 'Harap isikan Filter Proyek',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    } else {
                        excel("<?php echo site_url('excel/presensi'); ?>", {
                            "proyek": proyekan,
                            "bulan": $('#bulan-filter').val(),
                            "tahun": $('#tahun-filter').val(),
                            "periode": 'x',
                            "biodata": '<?php echo $this->session->userdata('_bio'); ?>'
                        });
                    }
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
                $('#excel-span').hide();

                $('#bulan-filter').val('<?php echo date('m'); ?>').trigger('change');
                $('#tahun-filter').val('<?php echo date('Y'); ?>').trigger('change');

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        urlParam = $('#proyek-filter').val() + '___<?php echo $this->session->userdata('_bio'); ?>___' + $('#tahun-filter').val() + '___' + $('#bulan-filter').val();
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/presensi'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                    $('#excel-span').show();
                });
            });

            function aksi(table) { // do nothing
            }

            function excel(url, param) {
                $.blockUI({message: '<h1>Memproses...</h1>'});
                $.ajax({
                    url: url, data: 'proyek=' + param.proyek + '&bulan=' + param.bulan + '&tahun=' + param.tahun + '&biodata=' + param.biodata + '&periode=' + param.periode,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $.unblockUI();
                        swal({
                            title: 'Hasil Export Presensi',
                            html: '<p><a href="<?php echo base_url('etc/excel'); ?>/' + response.return.file + '" class="btn btn-success btn-flat btn-block unduhan-link">Unduh File (' + response.return.file + ')</a></p>',
                            type: 'success',
                            showConfirmButton: false
                        });
                    }
                });
            }
        </script>
    </body>
</html>