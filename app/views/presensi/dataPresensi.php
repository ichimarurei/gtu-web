<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Absensi Kehadiran</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Absensi Kehadiran
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Absensi Kehadiran</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="presensi">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="status-input" name="status-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pegawai</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="biodata-input" name="biodata-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Waktu</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Waktu Presensi" type="text" id="waktu-input" name="waktu-input" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Keterangan</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control text-tetap" placeholder="Keterangan" rows="4" id="keterangan-input" name="keterangan-input"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Lampiran</label>
                                                    <div class="col-md-10">
                                                        <button class="btn btn-default" id="unggah-button" type="button">Unggah</button>
                                                        <a href="" class="btn btn-link" id="unduh-link">Unduh</a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="" id="batal-link">Batal</a>
                                                        <button class="btn btn-primary simpan-button" type="button" data-status="hadir">Hadir</button>
                                                        <button class="btn btn-info simpan-button" type="button" data-status="izin">Izin</button>
                                                        <button class="btn btn-warning simpan-button" type="button" data-status="sakit">Sakit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="unggah-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Unggah Berkas</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="*">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs();
                initUploader();

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#biodata-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    $('#biodata-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Pegawai'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#batal-link').attr('href', '<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>');

                $('.simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    $('#status-input').val($(this).data('status'));
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>", $('#model-form'));
                });
                $('#unggah-button').click(function () {
                    $('#unggah-modal').modal();
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=presensi&kode=0',
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#keterangan-input').val(response['data'].keterangan);
                    }
                });
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 1, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/presensi', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '') {
                        $('#unduh-link').attr('href', '<?php echo base_url('etc/presensi'); ?>/' + $('#kode-input').val() + '/' + filename);
                        $('#unduh-link').show();
                        $('#unggah-button').hide();
                    }

                    $('#filedocument').fileinput('clear');
                    $('#unggah-modal').modal('hide');
                });
            }
        </script>
    </body>
</html>