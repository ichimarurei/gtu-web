<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$urlParam = $this->uri->segment(5);
$lPresensi = array();
$rProyek = NULL;
$hariKerja = 0;

if ($urlParam !== FALSE) {
    $params = explode('___', $urlParam);

    if ($params[1] !== 'x' && $params[2] !== 'x') {
        $dateAwal = $params[1];

        while (strtotime($dateAwal) <= strtotime($params[2])) {
            if (date('N', strtotime($dateAwal)) < 7) {
                $hariKerja++;
            }

            $dateAwal = date("Y-m-d", strtotime("+1 day", strtotime($dateAwal)));
        }

        if (strlen($params[0]) === 32) {
            $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $params[0], 'terpakai' => 1)));

            foreach ($this->model->getList(array('table' => 'data_presensi_info', 'where' => array('proyek' => $params[0], 'DATE(waktu) >=' => $params[1], 'DATE(waktu) <=' => $params[2], 'terpakai' => 1), 'sort' => 'waktu desc')) as $record) {
                $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

                if ($rBiodata != NULL) {
                    $waktu = explode(' ', $record->waktu);
                    $lPresensi[$rBiodata->kode][$waktu[0]] = array($record, $rBiodata);
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Rekap Cut/Off</li><li>Absensi Kehadiran Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Rekap Cut/Off
                            <span>>
                                Absensi Kehadiran Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Dari Tanggal" type="text" id="dari-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Hingga Tanggal" type="text" id="hingga-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                    <div class="col-md-10" id="rekap-div">
                                        <br>
                                        <button class="btn btn-success btn-block" type="button" id="rekap-button">Rekap</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Absensi Kehadiran Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Jabatan</th>
                                                    <th>SKR</th>
                                                    <th>Ritase 2</th>
                                                    <th>Muat Inap</th>
                                                    <th>Insentif Pengiriman</th>
                                                    <th>Hari Kerja</th>
                                                    <th>Absen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                                                    $rKontrak = NULL;
                                                    $rJabatan = NULL;
                                                    $isShow = FALSE;
                                                    $sebagai = '';

                                                    if ($rProyek != NULL) {
                                                        $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                                                    }

                                                    if ($rKontrak != NULL) {
                                                        // kontrak masih berlaku
                                                        if ($rKontrak->habis >= date('Y-m-d')) {
                                                            $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                                                            $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                                                            $isShow = in_array($sebagai, array('Helper', 'Driver', 'Korlap'));
                                                        }
                                                    }

                                                    if ($isShow) {
                                                        $intAbsen = 0;
                                                        $intBolos = 0;
                                                        $intInap = 0;
                                                        $intSKR = 0;
                                                        $intRit2 = 0;
                                                        $intUpah = 0;

                                                        if (isset($lPresensi[$record->kode])) {
                                                            $intAbsen = count($lPresensi[$record->kode]);

                                                            foreach ($lPresensi[$record->kode] as $kPresensi => $vPresensi) {
                                                                $rQty = $this->model->getRecord(array('table' => 'data_proyek_qty', 'where' => array('proyek' => $rProyek->kode, 'biodata' => $record->kode, 'DATE(waktu)' => $kPresensi, 'terpakai' => 1)));

                                                                if ($rQty != NULL) {
                                                                    $intInap += $rQty->inap;
                                                                    $intSKR += $rQty->skr;
                                                                    $intRit2 += $rQty->ritase;
                                                                    $intUpah += $rQty->insentif;
                                                                }
                                                            }
                                                        }

                                                        $intBolos = ($hariKerja - $intAbsen);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo strtoupper($record->id); ?></td>
                                                            <td><?php echo ucwords($record->nama); ?></td>
                                                            <td><?php echo $sebagai; ?></td>
                                                            <td><?php echo $intSKR; ?></td>
                                                            <td><?php echo $intRit2; ?></td>
                                                            <td><?php echo $intInap; ?></td>
                                                            <td><?php echo $intUpah; ?></td>
                                                            <td><?php echo $intAbsen; ?></td>
                                                            <td><?php echo $intBolos; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="rekap">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" name="jenis-input" value="presensi">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="status-input" name="status-input" value="">
                                <input type="hidden" id="dari-input" name="dari-input" value="">
                                <input type="hidden" id="hingga-input" name="hingga-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = '<?php echo ($urlParam === FALSE) ? 'na' : $urlParam; ?>';

            $(document).ready(function () {
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#dari-filter').datepicker().on('changeDate', function (e) {
                    $('#hingga-filter').datepicker('setDate', e.date);
                    $('#hingga-filter').datepicker('setStartDate', e.date);
                });

                $('#filter-button').click(function () {
                    var dari = $('#dari-filter').val();
                    var hingga = $('#hingga-filter').val();

                    // reload tabel by ajax call
                    if (dari === '') {
                        dari = 'x';
                    }

                    if (hingga === '') {
                        hingga = 'x';
                    }

                    if ($('#proyek-filter').val() !== null && (dari !== 'x' && hingga !== 'x')) {
                        $(location).attr('href', "<?php echo site_url('modul/tampil/presensi/dataPRekapRangkum'); ?>/" + $('#proyek-filter').val() + '___' + dari + '___' + hingga);
                    }
                });
                $('#rekap-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    fillInputs();
                });

                if (urlParam === 'na') {
                    $('#rekap-div').hide();
                } else {
                    var paramURL = urlParam.split('___');
                    var pilihProyek = $('#proyek-filter');
                    $.ajax({
                        url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + paramURL[0],
                        dataType: 'json', type: 'POST', cache: false
                    }).then(function (data) {
                        // create the option and append to Select2
                        pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText + ' | ' + data['data'].lokasiText + ' | ' + data['data'].kendaraanText, data['data'].kode, true, true)).trigger('change');
                        // manually trigger the `select2:select` event
                        pilihProyek.trigger({
                            type: 'select2:select',
                            params: {
                                data: data['data']
                            }
                        });
                    });

                    if (paramURL[1] !== 'x') {
                        $('#dari-filter').val(paramURL[1]);
                    }

                    if (paramURL[2] !== 'x') {
                        $('#hingga-filter').val(paramURL[2]);
                    }

                    $('#rekap-div').show();
                }
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=rekap&kode=0',
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#dari-input').val($('#dari-filter').val());
                        $('#hingga-input').val($('#hingga-filter').val());
                        $('#proyek-input').val($('#proyek-filter').val());
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelPRekap'); ?>", $('#model-form'));
                    }
                });
            }
        </script>
    </body>
</html>