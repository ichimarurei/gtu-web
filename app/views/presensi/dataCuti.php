<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Ajuan Cuti</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Ajuan Cuti
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Ajuan Cuti</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="cuti">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="atasan-input" name="atasan-input" value="">
                                            <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Cuti</label>
                                                    <div class="col-md-3">
                                                        <input class="form-control" placeholder="Tanggal Cuti" type="text" id="waktu-input" name="waktu-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                    <label class="col-md-1 control-label">Selama</label>
                                                    <div class="col-md-2">
                                                        <select style="width:100%" class="select2 form-control" id="selama-input" name="selama-input">
                                                            <option value="1">1 Hari</option>
                                                            <option value="2">2 Hari</option>
                                                            <option value="3">3 Hari</option>
                                                            <option value="4">4 Hari</option>
                                                            <option value="5">5 Hari</option>
                                                            <option value="6">6 Hari</option>
                                                            <option value="7">7 Hari</option>
                                                            <option value="8">8 Hari</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Keterangan</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control text-tetap" placeholder="Keterangan" rows="4" id="keterangan-input" name="keterangan-input"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="status-div">
                                                    <label class="col-md-2 control-label">Status</label>
                                                    <div class="col-md-3">
                                                        <select style="width:100%" class="select2 form-control" id="status-input" name="status-input">
                                                            <option value="ajuan">PENGAJUAN</option>
                                                            <option value="setuju">SETUJU</option>
                                                            <option value="tolak">TOLAK</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="" id="batal-link">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var prosesBy = '<?php echo ($this->uri->segment(6) === FALSE) ? '-' : $this->uri->segment(6); ?>';

            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                var esok = new Date();
                var suffixURL = 'Q';
                esok.setDate(esok.getDate() + 1);
                $('#waktu-input').datepicker('setStartDate', esok);

                if (prosesBy === '-') {
                    $('#status-div').hide();
                } else {
                    $('#status-div').show();
                    suffixURL = '';
                }

                $('#batal-link').attr('href', '<?php echo site_url('modul/tampil/presensi/tabelCuti'); ?>' + suffixURL);

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelCuti'); ?>" + suffixURL, $('#model-form'));
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=cuti&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#atasan-input').val(response['data'].atasan);
                        $('#biodata-input').val(response['data'].biodata);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#keterangan-input').val(response['data'].keterangan);
                        $('#selama-input').val(response['data'].selama).trigger('change');
                        $('#status-input').val(response['data'].status).trigger('change');

                        if (response['data'].status !== 'ajuan') {
                            $('#simpan-button').hide();
                        } else {
                            $('#simpan-button').show();
                        }

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText + ' | ' + data['data'].lokasiText + ' | ' + data['data'].kendaraanText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            if ($('#status-input').val() !== 'ajuan') {
                                $('#atasan-input').val(prosesBy);
                            }

                            itungKuota(response['data'].biodata, response['data'].selama);
                        }
                    }
                });
            }

            function itungKuota(param, terpakai) {
                $.ajax({
                    url: "<?php echo site_url('data/tabel/cuti/all___'); ?>" + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var total = parseInt(terpakai);

                        $.each(response['data'], function (index, value) {
                            var selama = value.selama.replace(' Hari', '');
                            var lama = parseInt(selama);

                            if (value.status.toLowerCase() === 'setuju') {
                                total += lama;
                            }
                        });

                        if (total > 8) { // hanya bisa izin cuti selama 8 hari dalam setahun
                            $('#simpan-button').hide();
                        }
                    }
                });
            }
        </script>
    </body>
</html>