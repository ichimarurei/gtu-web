<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Tabel</li><li>Absensi Kehadiran Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
                <span class="ribbon-button-alignment pull-right">
                    <span id="excel-span" class="btn btn-ribbon hidden-xs" data-title="Export"><i class="fa fa-file-excel-o"></i> Export Absensi</span>
                </span>
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Absensi Kehadiran Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Dari Tanggal" type="text" id="dari-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Hingga Tanggal" type="text" id="hingga-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Absensi Kehadiran Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Proyek</th>
                                                    <th>Waktu</th>
                                                    <th>Status</th>
                                                    <th>Remarks</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="presensi">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                <input type="hidden" id="status-input" name="status-input" value="">
                                <input type="hidden" id="keterangan-input" name="keterangan-input" value="">
                            </form>
                            <form id="model-form-lembur">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="lembur">
                                <input type="hidden" name="action-input" id="action-input-lembur" value="">
                                <input type="hidden" name="key-input" id="key-input-lembur" value="">
                                <input type="hidden" name="kode-input" id="kode-input-lembur" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" id="presensi-input-lembur" name="presensi-input" value="">
                                <input type="hidden" id="waktu-input-lembur" name="waktu-input" value="">
                            </form>
                            <form id="model-form-pulang">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="pulang">
                                <input type="hidden" name="action-input" id="action-input-pulang" value="">
                                <input type="hidden" name="key-input" id="key-input-pulang" value="">
                                <input type="hidden" name="kode-input" id="kode-input-pulang" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" id="presensi-input-pulang" name="presensi-input" value="">
                                <input type="hidden" id="waktu-input-pulang" name="waktu-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = 'all___all___x___x';

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/presensi'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "id"},
                    {"data": "biodata"},
                    {"data": "proyek"},
                    {"data": "waktu"},
                    {"data": "status"}, {"data": "remarks"},
                    {"data": "aksi", 'width': '33%'}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#dari-filter').datepicker().on('changeDate', function (e) {
                    $('#hingga-filter').datepicker('setDate', e.date);
                    $('#hingga-filter').datepicker('setStartDate', e.date);
                });
                $('#excel-span').click(function () {
                    var dari = $('#dari-filter').val();
                    var hingga = $('#hingga-filter').val();
                    var proyekan = $('#proyek-filter').val();

                    if (dari === '' || hingga === '' || proyekan == null) {
                        swal({
                            title: 'Peringatan',
                            html: 'Harap isikan Filter Proyek dan Periode',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    } else {
                        excel("<?php echo site_url('excel/presensi'); ?>", {
                            "proyek": proyekan,
                            "bulan": 'x',
                            "tahun": 'x',
                            "periode": dari + '___' + hingga,
                            "biodata": 'all'
                        });
                    }
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
                $('#excel-span').hide();

                $('#filter-button').click(function () {
                    var dari = $('#dari-filter').val();
                    var hingga = $('#hingga-filter').val();

                    // reload tabel by ajax call
                    if (dari === '') {
                        dari = 'x';
                    }

                    if (hingga === '') {
                        hingga = 'x';
                    }

                    if ($('#proyek-filter').val() !== null) {
                        urlParam = $('#proyek-filter').val() + '___all___' + dari + '___' + hingga;
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/presensi'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                    $('#excel-span').show();
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/presensi/dataRemarkP'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .outBtn").on("click", function () {
                    fillPulang($(this).attr('href'));

                    return false;
                });
                $(table + " .otherBtn").on("click", function () {
                    fillLembur($(this).attr('href'));

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData);
                    });

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=presensi&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#biodata-input').val(response['data'].biodata);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#keterangan-input').val(response['data'].keterangan);
                        $('#status-input').val(response['data'].status);
                        doSave('Dihapus', "<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>", $('#model-form'));
                    }
                });
            }

            function fillPulang(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=pulang&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input-pulang').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input-pulang').val(response['data'].key);
                        $('#kode-input-pulang').val(response['data'].kode);
                        $('#presensi-input-pulang').val(response['data'].presensi);
                        $('#waktu-input-pulang').val(response['data'].waktu);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>", $('#model-form-pulang'));
                    }
                });
            }

            function fillLembur(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=lembur&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input-lembur').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input-lembur').val(response['data'].key);
                        $('#kode-input-lembur').val(response['data'].kode);
                        $('#presensi-input-lembur').val(response['data'].presensi);
                        $('#waktu-input-lembur').val(response['data'].waktu);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>", $('#model-form-lembur'));
                    }
                });
            }

            function excel(url, param) {
                $.blockUI({message: '<h1>Memproses...</h1>'});
                $.ajax({
                    url: url, data: 'proyek=' + param.proyek + '&bulan=' + param.bulan + '&tahun=' + param.tahun + '&biodata=' + param.biodata + '&periode=' + param.periode,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $.unblockUI();
                        swal({
                            title: 'Hasil Export Presensi',
                            html: '<p><a href="<?php echo base_url('etc/excel'); ?>/' + response.return.file + '" class="btn btn-success btn-flat btn-block unduhan-link">Unduh File (' + response.return.file + ')</a></p>',
                            type: 'success',
                            showConfirmButton: false
                        });
                    }
                });
            }
        </script>
    </body>
</html>