<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Rekap Cut/Off</li><li>Lembur Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Rekap Cut/Off
                            <span>>
                                Lembur Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Dari Tanggal" type="text" id="dari-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Hingga Tanggal" type="text" id="hingga-filter" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                    <div class="col-md-10" id="rekap-div">
                                        <br>
                                        <button class="btn btn-success btn-block" type="button" id="rekap-button">Rekap</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Lembur Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Proyek</th>
                                                    <th>Tanggal</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="rekap">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" name="jenis-input" value="lembur">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="status-input" name="status-input" value="">
                                <input type="hidden" id="dari-input" name="dari-input" value="">
                                <input type="hidden" id="hingga-input" name="hingga-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = '0___all___x___x';

            $(document).ready(function () {
                $('#rekap-div').hide();
                initTable("<?php echo site_url('data/tabel/lembur'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "id"},
                    {"data": "biodata"},
                    {"data": "proyek"},
                    {"data": "waktu"}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#dari-filter').datepicker().on('changeDate', function (e) {
                    $('#hingga-filter').datepicker('setDate', e.date);
                    $('#hingga-filter').datepicker('setStartDate', e.date);
                });

                $('#filter-button').click(function () {
                    var dari = $('#dari-filter').val();
                    var hingga = $('#hingga-filter').val();

                    // reload tabel by ajax call
                    if (dari === '') {
                        dari = 'x';
                    }

                    if (hingga === '') {
                        hingga = 'x';
                    }

                    if ($('#proyek-filter').val() !== null) {
                        urlParam = $('#proyek-filter').val() + '___all___' + dari + '___' + hingga;
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/lembur'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                    $('#rekap-div').show();
                });
                $('#rekap-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    fillInputs();
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=rekap&kode=0',
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#dari-input').val($('#dari-filter').val());
                        $('#hingga-input').val($('#hingga-filter').val());
                        $('#proyek-input').val($('#proyek-filter').val());
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/presensi/tabelLRekap'); ?>", $('#model-form'));
                    }
                });
            }

            function aksi(table) {
            }
        </script>
    </body>
</html>