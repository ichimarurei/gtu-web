<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us" id="lock-page">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <!-- page related CSS -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/lockscreen.min.css'); ?>">
    </head>

    <body>
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <form class="lockscreen animated flipInY">
                <div class="logo">
                    <h1 class="semi-bold"><img src="<?php echo base_url('res/SmartAdmin/img/logo-o.png'); ?>" alt="" /> HRIS | GTU</h1>
                </div>
                <div>
                    <img src="<?php echo base_url('res/gtu.jpg'); ?>" alt="" width="120" height="120" />
                    <div>
                        <h1><i class="fa fa-wrench fa-3x text-muted air air-top-right hidden-mobile"></i>System Under Maintenance<small><i class="fa fa-lock text-muted"></i> &nbsp;Locked</small></h1>
                        <p class="text-muted"><a href="mailto:iqbal@indesc.com" target="_blank">Kontak Kami</a></p>
                    </div>
                </div>
                <p class="font-xs margin-top-5">PT. GARDA TRIMITRA UTAMA <span class="hidden-xs"> - HRIS</span> © 2019</p>
            </form>
        </div>

        <!--================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
    </body>
</html>
