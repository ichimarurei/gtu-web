<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile-pic > img {
                width: 100%;
                max-width: 500px;
                background-color: white;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Beranda</li><li>Profil</li><li><b class="nama-text">Nama Anda</b></li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-user"></i>
                            Profil
                            <span>>
                                <b class="nama-text">Nama Anda</b>
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- row -->
                <div class="row">
                    <!-- a blank row to get started -->
                    <div class="col-sm-12">
                        <div class="well well-light well-sm no-margin no-padding">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="myCarousel" class="carousel fade profile-carousel">
                                        <div class="air air-top-left padding-10">
                                            <h4 class="txt-color-white font-md"><?php echo $this->session->userdata('_id'); ?></h4>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <!-- Slide 1 -->
                                            <div class="item active">
                                                <img src="<?php echo base_url('res/SmartAdmin/img/demo/s1.jpg'); ?>" alt="Gambar" style="width: 100%;">
                                            </div>
                                            <!-- Slide 2 -->
                                            <div class="item">
                                                <img src="<?php echo base_url('res/SmartAdmin/img/demo/s2.jpg'); ?>" alt="Gambar" style="width: 100%;">
                                            </div>
                                            <!-- Slide 3 -->
                                            <div class="item">
                                                <img src="<?php echo base_url('res/SmartAdmin/img/demo/s3.jpg'); ?>" alt="Gambar" style="width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-3 profile-pic">
                                            <img src="<?php echo base_url('etc/akun/avatar.png'); ?>" alt="Avatar" id="foto-img">
                                        </div>
                                        <div class="col-sm-9">
                                            <h1 class="nama-text">Nama Anda</h1>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-phone"></i> <span class="txt-color-darken telp-utama-text">000</span> - <span class="txt-color-darken telp-alternatif-text">000</span>
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="text-muted"><i class="fa fa-envelope"></i> <span class="txt-color-darken email-text">Email</span></p>
                                                </li>
                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-tag"></i> <span class="txt-color-darken ktp-text">KTP</span>
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-heart"></i> <span class="txt-color-darken tempat-text">TTL</span>, <span class="txt-color-darken tanggal-text">TTL</span>
                                                    </p>
                                                </li>
                                            </ul>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <form class="form-horizontal" id="model-form">
                                                        <!-- Model Mandatories -->
                                                        <input type="hidden" name="model-input" value="akun">
                                                        <input type="hidden" name="action-input" id="action-input" value="">
                                                        <input type="hidden" name="key-input" id="key-input" value="">
                                                        <input type="hidden" name="kode-input" id="kode-input" value="">
                                                        <!-- Model Data -->
                                                        <input type="hidden" name="terpakai-input" value="1">
                                                        <input type="hidden" name="proyek-input" value="-">
                                                        <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                                        <input type="hidden" id="otoritas-input" name="otoritas-input" value="">
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">ID Pengguna</label>
                                                                <div class="col-md-4">
                                                                    <input class="form-control" placeholder="ID Pengguna" type="text" id="id-input" name="id-input" readonly>
                                                                </div>
                                                                <label class="col-md-1 control-label">Sandi</label>
                                                                <div class="col-md-4">
                                                                    <input class="form-control" placeholder="Sandi" type="password" id="pin-input" name="pin-input">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-11 text-right">
                                                                    <button class="btn btn-primary" type="button" id="simpan-button">
                                                                        <i class="fa fa-save"></i>
                                                                        Simpan
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo $this->session->userdata('_bio'); ?>');

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    doSave('Disimpan', "<?php echo site_url(); ?>", $('#model-form'));
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        if (response['data'].key > 0) {
                            var noTelp = response['data'].telepon.split('_');
                            $('.nama-text').text(response['data'].nama);
                            $('.tempat-text').text(response['data'].tempat_lahir);
                            $('.tanggal-text').text(response['data'].tanggal_lahir);
                            $('.ktp-text').text(response['data'].ktp);
                            $('.email-text').text(response['data'].email);
                            $('.telp-utama-text').text(noTelp[0]);

                            if (noTelp[1] !== '-') {
                                $('.telp-alternatif-text').text(noTelp[1]);
                            }

                            $.when(lihatGaleri('etc/akun/' + response['data'].kode)).done(function (galleries) {
                                $.each(galleries.galeri, function (index, value) {
                                    if (value === 'foto.jpg') {
                                        $('#foto-img').attr('src', "<?php echo base_url('etc/akun'); ?>/" + response['data'].kode + '/' + value);
                                    }
                                });
                            });
                        }
                    }
                });

                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=akun&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);

                        if (response['data'].key > 0) {
                            $('#key-input').val(response['data'].key);
                            $('#kode-input').val(response['data'].kode);
                            $('#id-input').val(response['data'].id);
                            $('#pin-input').val(response['data'].pin);
                            $('#biodata-input').val(response['data'].biodata);
                            $('#otoritas-input').val(response['data'].otoritas);
                        }
                    }
                });
            }
        </script>
    </body>
</html>