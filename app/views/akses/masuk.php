<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>

    <body class="animated fadeInDown">
        <header id="header">
            <div id="logo-group">
                <span id="logo"> <img src="<?php echo base_url('res/SmartAdmin/img/logo.png'); ?>" alt="SmartAdmin"> </span>
                <span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">PT. GARDA TRIMITRA UTAMA</span> </span>
            </div>
        </header>

        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                        <h1 class="txt-color-red login-header-big">PT. GARDA TRIMITRA UTAMA</h1>
                        <div class="hero">
                            <img src="<?php echo base_url('res/bg.jpg'); ?>" class="display-image img-responsive img-rounded" alt="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <div class="well no-padding">
                            <form id="login-form" class="smart-form client-form">
                                <header>Otentikasi</header>
                                <fieldset>
                                    <section>
                                        <label class="label">ID</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" id="id-input" name="id-input" placeholder="ID" autofocus>
                                            <b class="tooltip tooltip-top-right"> <i class="fa fa-user txt-color-teal"></i> Isikan ID Anda</b>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Sandi</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="pin-input" name="pin-input" placeholder="Sandi">
                                            <b class="tooltip tooltip-top-right"> <i class="fa fa-lock txt-color-teal"></i> Isikan Sandi Anda</b>
                                        </label>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="button" class="btn btn-primary" id="submit-btn">Masuk</button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).keypress(function (e) {
                if (e.which === 13) { // enter key press event
                    if ($('#id-input').val().trim() === '') {
                        $('#id-input').focus();
                    } else if ($('#pin-input').val().trim() === '') {
                        $('#pin-input').focus();
                    } else {
                        goSend();
                    }
                }
            });

            $(document).ready(function () {
                $('#submit-btn').on('click', function () {
                    goSend();

                    return false;
                });
            });

            function goSend() {
                $.blockUI({message: '<h1>Proses Otentikasi...</h1>'});
                $.ajax({
                    url: "<?php echo site_url('akses/untuk/masuk'); ?>",
                    data: $("#login-form").serialize(),
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    success: function (respon) {
                        $.unblockUI();

                        if (respon['data'].code === 1) {
                            swal({
                                title: 'Berhasil',
                                text: respon['data'].message,
                                timer: 3000,
                                type: 'success',
                                showConfirmButton: false,
                                onClose: function () {
                                    $(location).attr('href', "<?php echo site_url(); ?>");
                                }
                            });
                        } else {
                            swal({
                                title: 'Gagal',
                                html: '<p>' + respon['data'].message + '</p>',
                                timer: (respon['data'].code === 2) ? 3000 : 2000,
                                type: (respon['data'].code === 2) ? 'warning' : 'error',
                                showConfirmButton: false
                            });
                        }
                    }
                });
            }
        </script>
    </body>
</html>