<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Keuangan</li><li>Rekap</li><li>Kas Kecil</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Rekap
                            <span>>
                                Kas Kecil
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <button class="btn btn-success btn-block" type="button" id="all-button" style="margin-left: 5px;">Semua Proyek</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Kas Kecil</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Bulan</th>
                                                    <th>Perihal</th>
                                                    <th>Debit</th>
                                                    <th>Kredit</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <div class="well well-light">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p><b>Debit: </b> <span id="debit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p><b>Kredit: </b> <span id="kredit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <button class="btn btn-primary" type="button" id="baru-button">
                                            <i class="fa fa-money"></i>
                                            Perbarui Kas Besar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <form class="form-horizontal" id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="kasar">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" name="perihal-input" value="KAS KECIL">
                                <input type="hidden" id="debit-input" name="debit-input" value="">
                                <input type="hidden" id="kredit-input" name="kredit-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var rDebit = 0;
            var rKredit = 0;

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/kacil/rekap___0'); ?>", '#dt_basic', [
                    {"data": "periode"},
                    {"data": "perihal"},
                    {"data": "debit"},
                    {"data": "kredit"}
                ]);

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        var tabelnya = $('#dt_basic').DataTable();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/kacil'); ?>/rekap___" + $('#proyek-filter').val()).load();
                        tabelnya.columns.adjust().draw();
                        reloading($('#proyek-filter').val());
                    }
                });
                $('#all-button').click(function () {
                    // reload tabel by ajax call
                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/kacil/rekap___all'); ?>").load();
                    tabelnya.columns.adjust().draw();
                    reloading('all');
                    $('#proyek-filter').val(null).trigger('change');
                });

                $('#baru-button').hide();
                $('#baru-button').click(function () {
                    swal({
                        title: 'Konfirmasi Perbarui Kas Besar?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Perbarui!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs('KAS KECIL');
                    });
                });
            });

            function aksi(table) {
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kasar&kode=perihal___' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#debit-input').val(rDebit);
                        $('#kredit-input').val(rKredit);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/keu/kasBesar'); ?>", $('#model-form'));
                    }
                });
            }

            function reloading(param) {
                $.ajax({
                    url: "<?php echo site_url('data/tabel/kacil'); ?>/rekap___" + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var debit = 0;
                        var kredit = 0;

                        $.each(response.data, function (index, value) {
                            debit += parseInt(value.tagihInt);
                            kredit += parseInt(value.bayarInt);
                        });

                        rDebit = debit;
                        rKredit = kredit;
                        $('#baru-button').show();
                        $('#debit-span').text(formatRp(debit.toString()));
                        $('#kredit-span').text(formatRp(kredit.toString()));
                    }
                });
            }

            function nominalFormats(value) {
                var hasil = 0;

                if (value !== '') {
                    var nominalText = value.replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    hasil = parseInt(nominalText);
                }

                return hasil;
            }
        </script>
    </body>
</html>