<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Keuangan</li><li>Rekap</li><li>Ritase II</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Rekap
                            <span>>
                                Ritase II
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <select style="width:100%" class="select2 form-control" id="bulan-filter">
                                            <option value="0">Pilih Bulan</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <input class="form-control" placeholder="Tahun" type="text" id="tahun-filter" readonly>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Ritase II</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Proyek</th>
                                                    <th>Ditagihkan</th>
                                                    <th>Dibayarkan</th>
                                                    <th>Margin</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <div class="well well-light">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p><b>Ditagihkan: </b> <span id="debit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p><b>Dibayarkan: </b> <span id="kredit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <button class="btn btn-primary" type="button" id="baru-button">
                                            <i class="fa fa-money"></i>
                                            Tambahkan ke Kas Besar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <form class="form-horizontal" id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="kasar">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="1">
                                <input type="hidden" name="perihal-input" value="RITASE II">
                                <input type="hidden" id="debit-input" name="debit-input" value="">
                                <input type="hidden" id="kredit-input" name="kredit-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var rDebit = 0;
            var rKredit = 0;

            $(document).ready(function () {
                initTable("<?php echo site_url('kas/gaji/0/cumaritase'); ?>", '#dt_basic', [
                    {"data": "proyek"},
                    {"data": "tagih"},
                    {"data": "bayar"},
                    {"data": "margin"}
                ]);

                $('#bulan-filter').val('0').trigger('change');
                $('#tahun-filter').val('<?php echo date('Y'); ?>');

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    var bulan = $('#bulan-filter').val();
                    var tahun = $('#tahun-filter').val();
                    var limit = parseInt('<?php echo date('n'); ?>');

                    if (parseInt(bulan) > 0) {
                        if (parseInt(bulan) > limit) {
                            swal({
                                title: 'Peringatan',
                                html: 'Belum dapat memproses data untuk bulan yang dipilih!',
                                timer: 3000,
                                type: 'warning',
                                showConfirmButton: false
                            });
                        } else {
                            var tabelnya = $('#dt_basic').DataTable();
                            tabelnya.ajax.url("<?php echo site_url('kas/gaji'); ?>/" + bulan + '___' + tahun + '/cumaritase').load();
                            tabelnya.columns.adjust().draw();
                            reloading(bulan + '___' + tahun + '/cumaritase');
                        }
                    }
                });

                $('#baru-button').hide();
                $('#baru-button').click(function () {
                    swal({
                        title: 'Konfirmasi Menambahkan Nominal ke Kas Besar?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Tambahkan!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs('RITASE II');
                    });
                });
            });

            function aksi(table) {
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kasar&kode=perihal___' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        rDebit += nominalFormats(response['data'].debit);
                        rKredit += nominalFormats(response['data'].kredit);
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#debit-input').val(rDebit);
                        $('#kredit-input').val(rKredit);
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/keu/kasBesar'); ?>", $('#model-form'));
                    }
                });
            }

            function reloading(param) {
                $.ajax({
                    url: "<?php echo site_url('kas/gaji'); ?>/" + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var debit = 0;
                        var kredit = 0;

                        $.each(response.data, function (index, value) {
                            debit += parseInt(value.tagihInt);
                            kredit += parseInt(value.bayarInt);
                        });

                        rDebit = debit;
                        rKredit = kredit;
                        $('#baru-button').show();
                        $('#debit-span').text(formatRp(debit.toString()));
                        $('#kredit-span').text(formatRp(kredit.toString()));
                    }
                });
            }

            function nominalFormats(value) {
                var hasil = 0;

                if (value !== '') {
                    var nominalText = value.replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    hasil = parseInt(nominalText);
                }

                return hasil;
            }
        </script>
    </body>
</html>