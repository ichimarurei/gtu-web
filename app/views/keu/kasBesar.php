<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Keuangan</li><li>Tabel</li><li>Kas Besar</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Kas Besar
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Kas Besar</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Perihal</th>
                                                    <th>Debit</th>
                                                    <th>Kredit</th>
                                                    <th>Margin</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <div class="well well-light">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p><b>Debit: </b> <span id="debit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p><b>Kredit: </b> <span id="kredit-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p><b>Laba: </b> <span id="laba-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-primary" type="button" id="baru-button">
                                            <i class="fa fa-money"></i>
                                            Buat Baru
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="nominal-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Proses Perubahan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal" id="model-form">
                                                <!-- Model Mandatories -->
                                                <input type="hidden" name="model-input" value="kasar">
                                                <input type="hidden" name="action-input" id="action-input" value="">
                                                <input type="hidden" name="key-input" id="key-input" value="">
                                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                                <!-- Model Data -->
                                                <input type="hidden" id="terpakai-input" name="terpakai-input" value="">
                                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Perihal</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Perihal" rows="4" id="perihal-input" name="perihal-input"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Ditagihkan</label>
                                                        <div class="col-md-6">
                                                            <input class="form-control nominal-text" placeholder="Ditagihkan (Rp)" type="text" id="debit-input" name="debit-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Dibayarkan</label>
                                                        <div class="col-md-6">
                                                            <input class="form-control nominal-text" placeholder="Dibayarkan (Rp)" type="text" id="kredit-input" name="kredit-input">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" type="button" id="simpan-button">Proses</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/kasar'); ?>", '#dt_basic', [
                    {"data": "perihal"},
                    {"data": "debit"},
                    {"data": "kredit"},
                    {"data": "balance"},
                    {"data": "aksi", 'width': '15%'}
                ]);
                reloading();

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    nominalFormats();
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/keu/kasBesar'); ?>", $('#model-form'));
                });
                $('#baru-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    $('#terpakai-input').val('1');
                    fillInputs('0', true);
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    $('#terpakai-input').val('1');
                    fillInputs($(this).attr('href'), true);

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        $('#terpakai-input').val('0');
                        fillInputs(idData, false);
                    });

                    return false;
                });
            }

            function fillInputs(param, isInsert) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kasar&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#perihal-input').val(response['data'].perihal);
                        $('#debit-input').val(response['data'].debit);
                        $('#kredit-input').val(response['data'].kredit);

                        if (isInsert) {
                            $.unblockUI();
                            $('#nominal-modal').modal();
                        } else {
                            nominalFormats();
                            doSave('Dihapus', "<?php echo site_url('modul/tampil/keu/kasBesar'); ?>", $('#model-form'));
                        }
                    }
                });
            }

            function reloading() {
                $.ajax({
                    url: "<?php echo site_url('data/tabel/kasar'); ?>",
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var debit = 0;
                        var kredit = 0;
                        var margin = 0;

                        $.each(response.data, function (index, value) {
                            debit += value.debitInt;
                            kredit += value.kreditInt;
                        });

                        margin = debit - kredit;
                        $('#debit-span').text(formatRp(debit.toString()));
                        $('#kredit-span').text(formatRp(kredit.toString()));
                        $('#laba-span').text(formatRp(margin.toString()));
                    }
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }
        </script>
    </body>
</html>