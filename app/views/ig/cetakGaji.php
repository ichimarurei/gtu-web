<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-block" type="button" id="cetak-button">
                    <i class="fa fa-file-pdf-o"></i>
                    Cetak
                </button>
            </div>
        </div>
        <div id="html-2-pdfwrapper" style="color: black; background: white; padding: 11px 30px;">
            <div class="row">
                <div class="col-sm-7" style="border: 1px solid black;">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1>
                                <b>PT. GARDA TRIMITRA UTAMA</b><br>
                                <span style="font-size: 14px;">Jl. Raya Centex No. 2A Ciracas Jakarta Timur<br>Telepon / Fax (021) 877 80925</span>
                            </h1>
                        </div>
                        <div class="col-sm-3 text-center pull-right" style="border: 1px solid black; margin: 10px; padding: 10px;">
                            <h3 style="margin: 0 auto;">
                                SLIP GAJI<br>
                                <span style="font-size: 20px;"><b class="periode-text">.....</b></span>
                            </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row" style="padding: 10px;">
                        <div class="col-sm-12" style="border: 1px solid black;">
                            <div class="row" style="padding: 10px;">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-3">NAMA</div>
                                        <div class="col-sm-1"> : </div>
                                        <div class="col-sm-8 nama-text">.....</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">NIK</div>
                                        <div class="col-sm-1"> : </div>
                                        <div class="col-sm-8 nik-text">.....</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-3">PROJECT</div>
                                        <div class="col-sm-1"> : </div>
                                        <div class="col-sm-8 proyek-text">.....</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">JABATAN</div>
                                        <div class="col-sm-1"> : </div>
                                        <div class="col-sm-8 jabatan-text">.....</div>
                                    </div>
                                </div>
                            </div>
                            <p style="margin: 0 auto;">=================================================================================================</p>
                            <div class="row" style="padding: 10px;">
                                <div class="col-sm-6" style="border: 1px solid black;">
                                    <div class="row">
                                        <div class="col-sm-12">PENGHASILAN</div>
                                        <div class="col-sm-12" style="border-top: 1px solid black;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    a. Gaji Pokok<br>
                                                    b. Uang Makan<br>
                                                    c. Lembur<br>
                                                    d. Muat Inap<br>
                                                    e. Ritase II<br>
                                                    f. SKR<br>
                                                    g. Insentif Kiriman<br>
                                                    h. Klaim Dll.
                                                    <hr style="border: 1px solid white;">
                                                    Total Penambahan<br style="margin: 5px;">
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <span class="gaji-text">.....</span><br>
                                                    <span class="makan-text">.....</span><br>
                                                    <span class="lembur-text">.....</span><br>
                                                    <span class="inap-text">.....</span><br>
                                                    <span class="ritase-text">.....</span><br>
                                                    <span class="skr-text">.....</span><br>
                                                    <span class="insentif-text">.....</span><br>
                                                    <span class="klaim-text">.....</span>
                                                    <hr style="border: 1px solid black;">
                                                    <span class="plus-text">.....</span><br style="margin: 5px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    <div class="row">
                                        <div class="col-sm-12">PENGURANGAN</div>
                                        <div class="col-sm-12" style="border-top: 1px solid black;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    a. Pot. Kehadiran<br>
                                                    b. Pot. Cicilan Kasbon<br>
                                                    c. Pot. Lain-lain
                                                    <hr style="border: 1px solid white;">
                                                    <br><br><br><br><br>
                                                    Total Pengurangan<br style="margin: 5px;">
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <span class="pot_hadir-text">.....</span><br>
                                                    <span class="pot_kasbon-text">.....</span><br>
                                                    <span class="pot_lain_isi-text">.....</span>
                                                    <hr style="border: 1px solid black;">
                                                    <br><br><br><br><br>
                                                    <span class="minus-text">.....</span><br style="margin: 5px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="padding: 10px 25px;">
                                    <b>TOTAL GAJI</b> <b class="total-text" style="margin-left: 10px;">.....</b><br>
                                    <i class="terbilang-text text-capitalize">.....</i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right" style="padding: 10px 25px;">
                                    <b class="nama-text" style="margin-right: 50px;">.....</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script src="<?php echo base_url('res/jsPDF/dist/jspdf.min.js'); ?>"></script>
        <script src="<?php echo base_url('res/html2canvas/html2canvas.min.js'); ?>"></script>
        <script>
            var urlParam = '<?php echo $this->uri->segment(5); ?>';

            $(document).ready(function () {
                fillInputs();

                $('#cetak-button').click(function () {
                    html2canvas(document.querySelector('#html-2-pdfwrapper')).then(function (canvas) {
                        var imgData = canvas.toDataURL('image/png');
                        var doc = new jsPDF();
                        doc.addImage(imgData, 'PNG', 0, 0);
                        doc.save('gaji#' + urlParam + '.pdf');
                    });
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=gaji&kode=' + urlParam,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('.periode-text').text(response['data'].periode);
                        $('.nama-text').text(response['data'].nama);
                        $('.nik-text').text(response['data'].nik);
                        $('.proyek-text').text(response['data'].proyek);
                        $('.jabatan-text').text(response['data'].jabatan);
                        $('.gaji-text').text(response['data'].gaji.replace('Rp. ', ''));
                        $('.makan-text').text(response['data'].makan.replace('Rp. ', ''));
                        $('.lembur-text').text(response['data'].lembur.replace('Rp. ', ''));
                        $('.inap-text').text(response['data'].inap.replace('Rp. ', ''));
                        $('.insentif-text').text(response['data'].insentif.replace('Rp. ', ''));
                        $('.ritase-text').text(response['data'].ritase.replace('Rp. ', ''));
                        $('.skr-text').text(response['data'].skr.replace('Rp. ', ''));
                        $('.klaim-text').text(response['data'].klaim.replace('Rp. ', ''));
                        $('.pot_kasbon-text').text(response['data'].pot_kasbon.replace('Rp. ', ''));
                        $('.pot_hadir-text').text(response['data'].pot_hadir.replace('Rp. ', ''));
                        $('.pot_lain_isi-text').text(response['data'].pot_lain_isi.replace('Rp. ', ''));
                        $('.plus-text').text(response['data'].plus);
                        $('.minus-text').text(response['data'].minus);
                        $('.total-text').text(response['data'].total);
                        $('.terbilang-text').text('#' + response['data'].terbilang + ' rupiah#');
                    }
                });
            }
        </script>
    </body>
</html>
