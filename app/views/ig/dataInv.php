<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .atas-span {
                font-size: 16px;
            }
            hr {
                border-top: 2px solid black;
            }
            small {
                font-size: 70%;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Pendataan</li><li>Invoice</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Invoice
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Invoice</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-8">
                                                <h1>
                                                    <b>INVOICE</b><br>
                                                    <span class="atas-span periode-text"></span>
                                                    <div class="pull-right"><span class="atas-span nomor-text"></span></div>
                                                </h1>
                                                <hr class="clearfix">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-4">
                                                <p class="pt-text">.....</p>
                                                <p class="alamat-text">.....</p>
                                                <p class="npwp-text">.....</p>
                                            </div>
                                            <div class="col-sm-4 text-right">
                                                <div class="row">
                                                    <div class="col-sm-3">Tanggal</div>
                                                    <div class="col-sm-1"> : </div>
                                                    <div class="col-sm-8 tanggal-text">.....</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">Project</div>
                                                    <div class="col-sm-1"> : </div>
                                                    <div class="col-sm-8 kerjaan-text">.....</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12"><br></div>
                                            <div class="col-sm-offset-2 col-sm-8 text-capitalize rincian-text"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12"><br></div>
                                            <div class="col-sm-offset-2 col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-12">1. Pengemudi</div>
                                                    <div class="col-sm-offset-1 col-sm-5">a. Pengadaan Pengemudi</div>
                                                    <div class="col-sm-6 text-right driver-inv-text">.....</div>
                                                    <div class="col-sm-offset-1 col-sm-5">b. Management Fee</div>
                                                    <div class="col-sm-6 text-right driver-fee-text">.....</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">2. Kernet</div>
                                                    <div class="col-sm-offset-1 col-sm-5">a. Pengadaan Kernet</div>
                                                    <div class="col-sm-6 text-right helper-inv-text">.....</div>
                                                    <div class="col-sm-offset-1 col-sm-5">b. Management Fee</div>
                                                    <div class="col-sm-6 text-right helper-fee-text">.....</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">3. Korlap</div>
                                                    <div class="col-sm-offset-1 col-sm-5">a. Pengadaan Korlap</div>
                                                    <div class="col-sm-6 text-right korlap-inv-text">.....</div>
                                                    <div class="col-sm-offset-1 col-sm-5">b. Management Fee</div>
                                                    <div class="col-sm-6 text-right korlap-fee-text">.....</div>
                                                    <div class="col-sm-offset-9 col-sm-3"><hr></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">TOTAL Pengadaan</div>
                                                    <div class="col-sm-6 text-right all-inv-text">.....</div>
                                                    <div class="col-sm-6">TOTAL Management Fee</div>
                                                    <div class="col-sm-6 text-right all-fee-text">.....</div>
                                                    <div class="col-sm-12"><br><br></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">TOTAL KESELURUHAN</div>
                                                    <div class="col-sm-6 text-right subtotal-text">.....</div>
                                                    <div class="col-sm-6">PPN 10%</div>
                                                    <div class="col-sm-6 text-right ppn-text">.....</div>
                                                    <div class="col-sm-offset-9 col-sm-3"><hr></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">GRAND TOTAL</div>
                                                    <div class="col-sm-6 text-right total-text">.....</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">Terbilang</div>
                                                    <div class="col-sm-10 text-center text-capitalize terbilang-text">.....</div>
                                                    <div class="col-sm-12"><small>Notes: PPH23 dipotong dari Management Fee</small></div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="invoice">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                            <input type="hidden" id="presensi-input" name="presensi-input" value="">
                                            <input type="hidden" id="lembur-input" name="lembur-input" value="">
                                            <input type="hidden" id="nomor-input" name="nomor-input" value="">
                                            <input type="hidden" id="bayar_driver-input" name="bayar_driver-input" value="">
                                            <input type="hidden" id="fee_driver-input" name="fee_driver-input" value="">
                                            <input type="hidden" id="bayar_helper-input" name="bayar_helper-input" value="">
                                            <input type="hidden" id="fee_helper-input" name="fee_helper-input" value="">
                                            <input type="hidden" id="bayar_korlap-input" name="bayar_korlap-input" value="">
                                            <input type="hidden" id="fee_korlap-input" name="fee_korlap-input" value="">
                                            <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                            <input type="hidden" id="npwp-input" name="npwp-input" value="">
                                            <input type="hidden" id="perihal-input" name="perihal-input" value="">
                                            <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                            <input type="hidden" id="status-input" name="status-input" value="">
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/ig/tabelInv'); ?>">Batal</a>
                                                        <a class="btn btn-success" href="" id="cetak-button" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                                                        <button class="btn btn-info" type="button" id="excel-button">
                                                            <i class="fa fa-file-excel-o"></i>
                                                            Export Detail
                                                        </button>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-money"></i>
                                                            DIBAYAR
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                var kodeParam = '<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>';
                fillInputs(kodeParam);

                $('#excel-button').click(function () {
                    excel("<?php echo site_url('excel/detilInv'); ?>", kodeParam);
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
                $('#simpan-button').click(function () {
                    swal({
                        title: 'Konfirmasi Pembayaran?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Sudah Dibayarkan!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        $('#status-input').val('bayar');
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/ig/tabelInv'); ?>", $('#model-form'));
                    });
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#presensi-input').val(response['data'].presensi);
                        $('#lembur-input').val(response['data'].lembur);
                        $('#nomor-input').val(response['data'].nomor);
                        $('#perihal-input').val(response['data'].perihal);
                        $('#npwp-input').val(response['data'].npwp);
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#bayar_driver-input').val(response['data'].bayar_driver);
                        $('#fee_driver-input').val(response['data'].fee_driver);
                        $('#bayar_helper-input').val(response['data'].bayar_helper);
                        $('#fee_helper-input').val(response['data'].fee_helper);
                        $('#bayar_korlap-input').val(response['data'].bayar_korlap);
                        $('#fee_korlap-input').val(response['data'].fee_korlap);
                        $('#ppn-input').val(response['data'].ppn);

                        if (response['data'].status === 'bayar') {
                            $('#simpan-button').hide();
                        }

                        // view
                        $('#cetak-button').attr('href', '<?php echo site_url('modul/tampil/ig/cetakInv'); ?>/' + param);
                        $('.nomor-text').text('No. ' + response['data'].nomor);
                        $('.periode-text').text(response['data'].periode);
                        $('.rincian-text').text('rincian tagihan bulan ' + response['data'].periode.toLowerCase());
                        $('.npwp-text').text('NPWP. ' + response['data'].npwp);
                        $('.tanggal-text').text(response['data'].tanggal);
                        $('.pt-text').text(response['data'].pt);
                        $('.alamat-text').text(response['data'].alamat);
                        $('.kerjaan-text').text(response['data'].kerjaan);
                        $('.driver-inv-text').text(response['data'].rp_bayar_driver);
                        $('.driver-fee-text').text(response['data'].rp_fee_driver);
                        $('.helper-inv-text').text(response['data'].rp_bayar_helper);
                        $('.helper-fee-text').text(response['data'].rp_fee_helper);
                        $('.korlap-inv-text').text(response['data'].rp_bayar_korlap);
                        $('.korlap-fee-text').text(response['data'].rp_fee_korlap);
                        $('.all-inv-text').text(response['data'].rp_inv_all);
                        $('.all-fee-text').text(response['data'].rp_fee_all);
                        $('.ppn-text').text(response['data'].rp_ppn);
                        $('.subtotal-text').text(response['data'].rp_subtotal);
                        $('.total-text').text(response['data'].rp_total);
                        $('.terbilang-text').text('#' + response['data'].rp_terbilang + ' rupiah#');
                    }
                });
            }

            function excel(url, param) {
                $.blockUI({message: '<h1>Memproses...</h1>'});
                $.ajax({
                    url: url, data: 'param=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $.unblockUI();
                        swal({
                            title: 'Hasil Export Detail Invoice',
                            html: '<p><a href="<?php echo base_url('etc/excel'); ?>/' + response.return.file + '" class="btn btn-success btn-flat btn-block unduhan-link">Unduh File (' + response.return.file + ')</a></p>',
                            type: 'success',
                            showConfirmButton: false
                        });
                    }
                });
            }
        </script>
    </body>
</html>