<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Tabel</li><li>Gaji</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Gaji
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-10"></div>
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="invoice-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Gaji</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Jabatan</th>
                                                    <th>Nominal</th>
                                                    <th>Status</th>
                                                    <th>Tanggal Proses</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="gaji-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Proses Gaji</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal" id="model-form">
                                                <!-- Model Mandatories -->
                                                <input type="hidden" name="model-input" value="gaji">
                                                <input type="hidden" name="action-input" id="action-input" value="">
                                                <input type="hidden" name="key-input" id="key-input" value="">
                                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                                <!-- Model Data -->
                                                <input type="hidden" name="terpakai-input" value="1">
                                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                                <input type="hidden" id="invoice-input" name="invoice-input" value="">
                                                <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                <input type="hidden" id="status-input" name="status-input" value="">
                                                <fieldset>
                                                    <legend>PEGAWAI</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">ID Pegawai</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control" placeholder="ID Pegawai" type="text" id="id-text" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nama</label>
                                                        <div class="col-md-10">
                                                            <input class="form-control" placeholder="Nama Lengkap Pegawai" type="text" id="nama-text" readonly>
                                                        </div>
                                                    </div>
                                                    <legend>PENGHASILAN</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Gaji</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Gaji Pokok (Rp)" type="text" id="gaji-input" name="gaji-input" readonly>
                                                        </div>
                                                        <label class="col-md-2 control-label">Lembur</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Lembur (Rp)" type="text" id="lembur-input" name="lembur-input" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Uang Makan</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Uang Makan (Rp)" type="text" id="makan-input" name="makan-input" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Muat Inap</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Muat Inap (Rp)" type="text" id="inap-input" name="inap-input" readonly>
                                                        </div>
                                                        <label class="col-md-2 control-label">Ritase II</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Ritase II (Rp)" type="text" id="ritase-input" name="ritase-input" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Insentif Kiriman</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Insentif Pengiriman (Rp)" type="text" id="insentif-input" name="insentif-input" readonly>
                                                        </div>
                                                        <label class="col-md-2 control-label">SKR</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="SKR (Rp)" type="text" id="skr-input" name="skr-input" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Klaim</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Klaim (Rp)" type="text" id="klaim-input" name="klaim-input" readonly>
                                                        </div>
                                                    </div>
                                                    <legend>PENGURANGAN</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Pot. Kasbon</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Potongan Cicilan Kasbon (Rp)" type="text" id="pot_kasbon-input" name="pot_kasbon-input" readonly>
                                                        </div>
                                                        <label class="col-md-2 control-label">Pot. Kehadiran</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Potongan Kehadiran (Rp)" type="text" id="pot_hadir-input" name="pot_hadir-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Pot. Lainnya</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Perihal Potongan Lainnya" rows="2" id="pot_lain_info-input" name="pot_lain_info-input"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Potongan Lain-lain (Rp)" type="text" id="pot_lain_isi-input" name="pot_lain_isi-input">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary simpan-button" type="button" data-status="proses">Proses</button>
                                            <button class="btn btn-success simpan-button" type="button" data-status="bayar">Bayar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/gaji/0___0___0___0'); ?>", '#dt_basic', [
                    {"data": "id"},
                    {"data": "biodata"},
                    {"data": "jabatan"},
                    {"data": "nominal"},
                    {"data": "status"},
                    {"data": "tanggal"},
                    {"data": "aksi", 'width': '15%'}
                ]);

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#invoice-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Cut/Off Rekap'});
                    }
                });
                $('#proyek-filter').change(function () {
                    var kode = $(this).val();
                    $('#invoice-filter').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Cut/Off Rekap'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/invoice'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null || $('#invoice-filter').val() !== null) {
                        var tabelnya = $('#dt_basic').DataTable();
                        var kodeInv = $('#invoice-filter').val().split('___');
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/gaji'); ?>/" + $('#proyek-filter').val() + '___' + kodeInv[0] + '___' + kodeInv[1] + '___' + kodeInv[2]).load();
                        tabelnya.columns.adjust().draw();
                    }
                });

                $('.simpan-button').click(function () {
                    $('#status-input').val($(this).data('status'));
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Proses!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        nominalFormats();
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/ig/kelolaGaji'); ?>", $('#model-form'));
                    });
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    fillInputs(idData);
                    $('#id-text').val($(this).data('nik'));
                    $('#nama-text').val($(this).data('nama'));

                    if (idData === '0') {
                        var kodeInv = $('#invoice-filter').val().split('___');
                        $('#proyek-input').val($('#proyek-filter').val());
                        $('#invoice-input').val(kodeInv[2]);
                        $('#biodata-input').val($(this).data('who'));
                        $('#gaji-input').val($(this).data('gaji'));
                        $('#makan-input').val($(this).data('makan'));
                        $('#lembur-input').val($(this).data('lemburan'));
                        $('#inap-input').val($(this).data('inap'));
                        $('#insentif-input').val($(this).data('insentif'));
                        $('#ritase-input').val($(this).data('ritase'));
                        $('#skr-input').val($(this).data('skr'));
                        $('#klaim-input').val($(this).data('klaim'));
                        $('#pot_kasbon-input').val($(this).data('kasbon'));
                    }

                    return false;
                });
                $(table + " .printBtn").on("click", function () {
                    var idData = $(this).attr('href');

                    if (idData === '0') {
                        swal({
                            title: 'Peringatan',
                            html: 'Proses terlebih dahulu!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    } else {
                        window.open('<?php echo site_url('modul/tampil/ig/cetakGaji'); ?>/' + idData, '_blank');
                    }

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=gaji&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#pot_hadir-input').val(response['data'].pot_hadir);
                        $('#pot_lain_info-input').val(response['data'].pot_lain_info);
                        $('#pot_lain_isi-input').val(response['data'].pot_lain_isi);
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);

                        if (response['data'].key > 0) {
                            $('#proyek-input').val(response['data'].proyek);
                            $('#invoice-input').val(response['data'].invoice);
                            $('#biodata-input').val(response['data'].biodata);
                            $('#gaji-input').val(response['data'].gaji);
                            $('#makan-input').val(response['data'].makan);
                            $('#lembur-input').val(response['data'].lembur);
                            $('#inap-input').val(response['data'].inap);
                            $('#insentif-input').val(response['data'].insentif);
                            $('#ritase-input').val(response['data'].ritase);
                            $('#skr-input').val(response['data'].skr);
                            $('#klaim-input').val(response['data'].klaim);
                            $('#pot_kasbon-input').val(response['data'].pot_kasbon);
                        }

                        $('#gaji-modal').modal();
                    }
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }
        </script>
    </body>
</html>