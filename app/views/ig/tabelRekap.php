<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Proses</li><li>Invoice & Penggajian</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Rekap Cut/Off
                            <span>>
                                Absensi Kehadiran & Lembur Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Rekap Cut/Off</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Proyek</th>
                                                    <th>Rekapan</th>
                                                    <th>Jenis</th>
                                                    <th>Tanggal Buat</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <div class="well well-light">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p><b>Absensi: </b> <span id="presensi-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><b>Lembur: </b> <span id="lembur-span">.....</span></p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p style="margin-top: 6px;"><b>Perhitungan: </b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <select style="width:100%" class="select2 form-control" id="itung-opt">
                                            <option value="ph">Penuh/Bulan (26 hari kerja)</option>
                                            <option value="hr">Harian</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select style="width:100%" class="select2 form-control" id="komb-opt">
                                            <option value="yab">Termasuk Komponen B</option>
                                            <option value="nob">Tanpa Komponen B</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 text-right">
                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/ig/tabelRekap'); ?>">Batal</a>
                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                            <i class="fa fa-check"></i>
                                            Proses
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = '0___presensi';
            var kodePresensi = '';
            var kodeLembur = '';

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/rekap'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "proyek"},
                    {"data": "tanggal"},
                    {"data": "jenis"},
                    {"data": "waktu"},
                    {"data": "proses", 'width': '7%'}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        urlParam = $('#proyek-filter').val() + '___presensi___yay';
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/rekap'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                });
                $('#simpan-button').click(function () {
                    if (kodePresensi === '' && kodeLembur === '') {
                        swal({
                            title: 'Perhatian !!!',
                            html: 'Pilih Rekap Cut/Off terlebih dahulu!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    } else {
                        var param1 = (kodePresensi === '') ? 'na' : kodePresensi;
                        var param2 = (kodeLembur === '') ? 'na' : kodeLembur;

                        if (param1 === 'na') {
                            swal({
                                title: 'Perhatian !!!',
                                html: 'Rekap Cut/Off Absensi wajib dipilih!',
                                timer: 3000,
                                type: 'warning',
                                showConfirmButton: false
                            });
                        } else {
                            $(location).attr('href', '<?php echo site_url('modul/tampil/ig/dataIG'); ?>/' + param1 + '/' + param2 + '/' + $('#itung-opt').val() + '/' + $('#komb-opt').val());
                        }
                    }
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    var kode = $(this).attr('href');
                    var jenis = $(this).data('jenis');
                    var info = $(this).data('info');

                    if (jenis === 'presensi') {
                        kodePresensi = kode;
                        $('#presensi-span').html(info);
                    } else if (jenis === 'lembur') {
                        kodeLembur = kode;
                        $('#lembur-span').html(info);
                    }

                    return false;
                });
            }
        </script>
    </body>
</html>