<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .atas-span {
                font-size: 16px;
            }
            hr {
                border-top: 2px solid black;
            }
            small {
                font-size: 70%;
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-block" type="button" id="cetak-button">
                    <i class="fa fa-file-pdf-o"></i>
                    Cetak
                </button>
            </div>
        </div>
        <div id="html-2-pdfwrapper" style="color: black; background: white; padding: 11px 30px;">
            <div class="row">
                <div class="col-sm-7">
                    <h1>
                        <b>INVOICE</b><br>
                        <span class="atas-span periode-text"></span>
                        <div class="pull-right"><span class="atas-span nomor-text"></span></div>
                    </h1>
                    <hr class="clearfix">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <p class="pt-text">.....</p>
                    <p class="alamat-text">.....</p>
                    <p class="npwp-text">.....</p>
                </div>
                <div class="col-sm-3 text-right">
                    <div class="row">
                        <div class="col-sm-3">Tanggal</div>
                        <div class="col-sm-1"> : </div>
                        <div class="col-sm-8 tanggal-text">.....</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Project</div>
                        <div class="col-sm-1"> : </div>
                        <div class="col-sm-8 kerjaan-text">.....</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7"><br></div>
                <div class="col-sm-7 text-capitalize rincian-text"></div>
            </div>
            <div class="row">
                <div class="col-sm-7"><br></div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12">1. Pengemudi</div>
                        <div class="col-sm-6" style="padding-left: 30px;">a. Pengadaan Pengemudi</div>
                        <div class="col-sm-6 text-right driver-inv-text">.....</div>
                        <div class="col-sm-6" style="padding-left: 30px;">b. Management Fee</div>
                        <div class="col-sm-6 text-right driver-fee-text">.....</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">2. Kernet</div>
                        <div class="col-sm-6" style="padding-left: 30px;">a. Pengadaan Kernet</div>
                        <div class="col-sm-6 text-right helper-inv-text">.....</div>
                        <div class="col-sm-6" style="padding-left: 30px;">b. Management Fee</div>
                        <div class="col-sm-6 text-right helper-fee-text">.....</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">3. Korlap</div>
                        <div class="col-sm-6" style="padding-left: 30px;">a. Pengadaan Korlap</div>
                        <div class="col-sm-6 text-right korlap-inv-text">.....</div>
                        <div class="col-sm-6" style="padding-left: 30px;">b. Management Fee</div>
                        <div class="col-sm-6 text-right korlap-fee-text">.....</div>
                        <div class="col-sm-offset-9 col-sm-3"><hr></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">TOTAL Pengadaan</div>
                        <div class="col-sm-6 text-right all-inv-text">.....</div>
                        <div class="col-sm-6">TOTAL Management Fee</div>
                        <div class="col-sm-6 text-right all-fee-text">.....</div>
                        <div class="col-sm-12"><br><br></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">TOTAL KESELURUHAN</div>
                        <div class="col-sm-6 text-right subtotal-text">.....</div>
                        <div class="col-sm-6">PPN 10%</div>
                        <div class="col-sm-6 text-right ppn-text">.....</div>
                        <div class="col-sm-offset-9 col-sm-3"><hr></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">GRAND TOTAL</div>
                        <div class="col-sm-6 text-right total-text">.....</div>
                        <div class="col-sm-12"><br></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Terbilang</div>
                        <div class="col-sm-10 text-center text-capitalize terbilang-text">.....</div>
                        <div class="col-sm-12"><small>Notes: PPH23 dipotong dari Management Fee</small></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12"><br></div>
                <div class="col-sm-7 text-right">
                    <p style="margin-right: 50px;">Hormat Kami,</p>
                    <p><b>PT. GARDA TRIMITRA UTAMA</b></p>
                </div>
            </div>
        </div>

        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script src="<?php echo base_url('res/jsPDF/dist/jspdf.min.js'); ?>"></script>
        <script src="<?php echo base_url('res/html2canvas/html2canvas.min.js'); ?>"></script>
        <script>
            var urlParam = '<?php echo $this->uri->segment(5); ?>';
            var nomorInv = '00000';

            $(document).ready(function () {
                fillInputs();

                $('#cetak-button').click(function () {
                    html2canvas(document.querySelector('#html-2-pdfwrapper')).then(function (canvas) {
                        var imgData = canvas.toDataURL('image/png');
                        var doc = new jsPDF();
                        doc.addImage(imgData, 'PNG', 0, 0);
                        doc.save('invoice#' + nomorInv.toLowerCase() + '.pdf');
                    });
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=' + urlParam,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        nomorInv = response['data'].nomor.replace(/\//g, '-').toLowerCase();
                        $('.nomor-text').text('No. ' + response['data'].nomor);
                        $('.periode-text').text(response['data'].periode);
                        $('.rincian-text').text('rincian tagihan bulan ' + response['data'].periode.toLowerCase());
                        $('.npwp-text').text('NPWP. ' + response['data'].npwp);
                        $('.tanggal-text').text(response['data'].tanggal);
                        $('.pt-text').text(response['data'].pt);
                        $('.alamat-text').text(response['data'].alamat);
                        $('.kerjaan-text').text(response['data'].kerjaan);
                        $('.driver-inv-text').text(response['data'].rp_bayar_driver);
                        $('.driver-fee-text').text(response['data'].rp_fee_driver);
                        $('.helper-inv-text').text(response['data'].rp_bayar_helper);
                        $('.helper-fee-text').text(response['data'].rp_fee_helper);
                        $('.korlap-inv-text').text(response['data'].rp_bayar_korlap);
                        $('.korlap-fee-text').text(response['data'].rp_fee_korlap);
                        $('.all-inv-text').text(response['data'].rp_inv_all);
                        $('.all-fee-text').text(response['data'].rp_fee_all);
                        $('.ppn-text').text(response['data'].rp_ppn);
                        $('.subtotal-text').text(response['data'].rp_subtotal);
                        $('.total-text').text(response['data'].rp_total);
                        $('.terbilang-text').text('#' + response['data'].rp_terbilang + ' rupiah#');
                    }
                });
            }
        </script>
    </body>
</html>
