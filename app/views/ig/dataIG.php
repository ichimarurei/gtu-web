<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$kodePresensi = $this->uri->segment(5);
$kodeLembur = $this->uri->segment(6);
$itungan = $this->uri->segment(7);
$adaKompB = $this->uri->segment(8);
$rPresensi = NULL;
$rLembur = NULL;
$rProyek = NULL;
$lPresensi = array();
$lLembur = array();
$hariKerja = 0;
$driverInv = 0;
$driverFee = 0;
$helperInv = 0;
$helperFee = 0;
$korlapInv = 0;
$korlapFee = 0;
$totalInvoice = 0;
$feeInvoice = 0;
$isUseDay = FALSE;
$isUseB = FALSE;

if ($itungan !== FALSE) {
    $isUseDay = ($itungan === 'hr');
}

if ($adaKompB !== FALSE) {
    $isUseB = ($adaKompB === 'yab');
}

if ($kodePresensi !== FALSE) {
    $rPresensi = $this->model->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $kodePresensi, 'jenis' => 'presensi', 'status' => 'setuju', 'terpakai' => 1)));
}

if ($kodeLembur !== FALSE) {
    $rLembur = $this->model->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $kodeLembur, 'jenis' => 'lembur', 'status' => 'setuju', 'terpakai' => 1)));
}

if ($rPresensi != NULL) {
    $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rPresensi->proyek, 'terpakai' => 1)));
    $dateAwal = $rPresensi->dari;

    while (strtotime($dateAwal) <= strtotime($rPresensi->hingga)) {
        if (date('N', strtotime($dateAwal)) < 7) {
            $hariKerja++;
        }

        $dateAwal = date("Y-m-d", strtotime("+1 day", strtotime($dateAwal)));
    }

    foreach ($this->model->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rPresensi->kode, 'jenis' => 'presensi', 'terpakai' => 1))) as $record) {
        $rData = $this->model->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $record->data)));

        if ($rData != NULL) {
            $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rData->biodata, 'terpakai' => 1)));

            if ($rBiodata != NULL) {
                $waktu = explode(' ', $rData->waktu);
                $lPresensi[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
            }
        }
    }
}

if ($rLembur != NULL) {
    if ($rProyek == NULL) {
        $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rLembur->proyek, 'terpakai' => 1)));
    }

    foreach ($this->model->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rLembur->kode, 'jenis' => 'lembur', 'terpakai' => 1))) as $record) {
        $rData = $this->model->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('kode' => $record->data)));

        if ($rData != NULL) {
            $dAbsen = $this->model->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $rData->presensi)));

            if ($dAbsen != NULL) {
                $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $dAbsen->biodata, 'terpakai' => 1)));

                if ($rBiodata != NULL) {
                    $waktu = explode(' ', $rData->waktu);
                    $lLembur[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
                }
            }
        }
    }
}

if ($hariKerja > 26) {
    $hariKerja = 26;
}

$lInvoice = $this->model->getList(array('table' => 'data_proyek_invoice', 'where' => array('terpakai' => 1, 'YEAR(waktu)' => date('Y'), 'MONTH(waktu)' => date('m'))));
$noInv = count($lInvoice) + 1;
$txtInv = '';

for ($at = 4; $at > strlen($noInv); $at--) {
    $txtInv .= '0';
}

$txtInv = $txtInv . '' . $noInv . '/GTU-INV/' . $this->formatdate->getMonthRoman(date('n')) . '/' . date('y');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Pendataan</li><li>Invoice & Penggajian</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Invoice & Penggajian
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Invoice & Penggajian</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="invoice">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                                    <input type="hidden" id="presensi-input" name="presensi-input" value="">
                                                    <input type="hidden" id="lembur-input" name="lembur-input" value="">
                                                    <input type="hidden" id="nomor-input" name="nomor-input" value="">
                                                    <input type="hidden" id="bayar_driver-input" name="bayar_driver-input" value="">
                                                    <input type="hidden" id="fee_driver-input" name="fee_driver-input" value="">
                                                    <input type="hidden" id="bayar_helper-input" name="bayar_helper-input" value="">
                                                    <input type="hidden" id="fee_helper-input" name="fee_helper-input" value="">
                                                    <input type="hidden" id="bayar_korlap-input" name="bayar_korlap-input" value="">
                                                    <input type="hidden" id="fee_korlap-input" name="fee_korlap-input" value="">
                                                    <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                                    <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                    <input type="hidden" id="status-input" name="status-input" value="">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">NPWP</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="NPWP" type="text" id="npwp-input" name="npwp-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Perihal</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Catatan Terkait Invoice" rows="2" id="perihal-input" name="perihal-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <a class="btn btn-default" href="<?php echo site_url('modul/tampil/ig/tabelRekap'); ?>">Batal</a>
                                                                <button class="btn btn-primary" type="button" id="simpan-button">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-sm-5">
                                                <h1 class="font-400 text-right">INVOICE</h1>
                                                <br>
                                                <div>
                                                    <div>
                                                        <strong>NO :</strong>
                                                        <span class="pull-right"> <?php echo $txtInv; ?> </span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="font-md">
                                                        <strong>TANGGAL :</strong>
                                                        <span class="pull-right"> <i class="fa fa-calendar"></i> <?php echo $this->formatdate->getDate(date('Y-m-d')); ?> </span>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="well well-sm  bg-color-darken txt-color-white no-border">
                                                    <div class="fa-lg">
                                                        Total :
                                                        <span class="pull-right" id="total-span"> Rp. 0 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Jabatan</th>
                                                    <th>SKR</th>
                                                    <th>Ritase 2</th>
                                                    <th>Muat Inap</th>
                                                    <th>Insentif Pengiriman</th>
                                                    <th>Hari Kerja</th>
                                                    <th>Absen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                                                    $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                                                    $isShow = FALSE;
                                                    $sebagai = '';
                                                    $rJabatan = NULL;

                                                    if ($rKontrak != NULL && $rProyek != NULL) {
                                                        // kontrak masih berlaku
                                                        if ($rKontrak->status === 'PKWT') {
                                                            if ($rKontrak->habis >= date('Y-m-d')) {
                                                                $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                                                                $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                                                                $isShow = in_array($sebagai, array('Helper', 'Driver', 'Korlap'));
                                                            }
                                                        }
                                                    }

                                                    if ($isShow) {
                                                        $intAbsen = 0;
                                                        $intBolos = 0;
                                                        $intInap = 0;
                                                        $intSKR = 0;
                                                        $intRit2 = 0;
                                                        $intUpah = 0;
                                                        $gajian = 0;
                                                        $persenFee = 0;
                                                        $pph23 = 0;
                                                        $lemburan = 0;
                                                        $payInap = 0;
                                                        $paySKR = 0;
                                                        $payRit2 = 0;
                                                        $payUpah = 0;
                                                        $bpjs = 0;
                                                        $dibayar = 0;
                                                        $gajian = $rProyek->invoice_umk;

                                                        if ($sebagai === 'Helper') {
                                                            $persenFee = $rProyek->invoice_fee_helper;
                                                        } else if ($sebagai === 'Driver') {
                                                            $persenFee = $rProyek->invoice_fee_driver;
                                                        } else if ($sebagai === 'Korlap') {
                                                            $persenFee = $rProyek->invoice_fee_korlap;
                                                        }

                                                        /* if ($rKontrak != NULL) {
                                                          if ($rKontrak->gaji > 0) {
                                                          $gajian = $rKontrak->gaji;
                                                          }

                                                          if ($rKontrak->fee > 0) {
                                                          $persenFee = $rKontrak->fee;
                                                          }
                                                          } */

                                                        $gajiHarian = round($gajian / 26);

                                                        if (isset($lPresensi[$record->kode])) {
                                                            $intAbsen = count($lPresensi[$record->kode]);

                                                            foreach ($lPresensi[$record->kode] as $kPresensi => $vPresensi) {
                                                                $isLembur = FALSE;
                                                                $rQty = $this->model->getRecord(array('table' => 'data_proyek_qty', 'where' => array('proyek' => $rProyek->kode, 'biodata' => $record->kode, 'DATE(waktu)' => $kPresensi, 'terpakai' => 1)));

                                                                if ($rQty != NULL) {
                                                                    $intInap += $rQty->inap;
                                                                    $intSKR += $rQty->skr;
                                                                    $intRit2 += $rQty->ritase;
                                                                    $intUpah += $rQty->insentif;
                                                                }

                                                                if (isset($lLembur[$record->kode][$kPresensi])) {
                                                                    $isLembur = TRUE;
                                                                }

                                                                if ($isLembur) {
                                                                    if ($sebagai === 'Helper') {
                                                                        $lemburan += $rProyek->lembur_helper;
                                                                    } else if ($sebagai === 'Driver') {
                                                                        $lemburan += $rProyek->lembur_driver;
                                                                    } else if ($sebagai === 'Korlap') {
                                                                        $lemburan += $rProyek->lembur_korlap;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $intBolos = ($hariKerja - $intAbsen);

                                                        if ($isUseDay) { // reset
                                                            if ($intBolos > 1) {
                                                                $gajian = $gajiHarian * $intAbsen;
                                                            } else {
                                                                $gajian = $gajiHarian * $hariKerja;
                                                            }
                                                        }

                                                        if ($rProyek->invoice_ritase > 0) {
                                                            $payRit2 = round(($rProyek->invoice_ritase / 100) * $rProyek->invoice_umk) * $intRit2;
                                                        }

                                                        if ($rProyek->invoice_inap > 0) {
                                                            $payInap = round(($rProyek->invoice_inap / 100) * $rProyek->invoice_umk) * $intInap;
                                                        }

                                                        if ($rProyek->invoice_skr > 0) {
                                                            $paySKR = round(($rProyek->invoice_skr / 100) * $rProyek->invoice_umk) * $intSKR;
                                                        }

                                                        if ($rProyek->invoice_pengiriman > 0) {
                                                            $payUpah = round(($rProyek->invoice_pengiriman / 100) * $rProyek->invoice_umk) * $intUpah;
                                                        }

                                                        if ($rProyek->invoice_bpjs > 0) {
                                                            $bpjs = round(($rProyek->invoice_bpjs / 100) * $rProyek->invoice_umk);
                                                        }

                                                        $bayarTetap = $gajian + $rJabatan->komisi + $lemburan;
                                                        $bayarFleksi = $payRit2 + $payInap + $paySKR + $payUpah;
                                                        $plus2 = ($rProyek->invoice_thr / 12) + ($rProyek->invoice_supervisi / 12) + ($rProyek->invoice_seragam / 12) + $bpjs;

                                                        if ($persenFee > 0) {
                                                            $dibayar = round(($persenFee / 100) * ($bayarTetap));
                                                        }

                                                        if ($rProyek->invoice_pph > 0 && $dibayar > 0) {
                                                            $pph23 = round(($rProyek->invoice_pph / 100) * $dibayar);
                                                        }

                                                        $sub = $bayarTetap + $bayarFleksi + $pph23;

                                                        if ($isUseB) {
                                                            $sub += $plus2;
                                                        }

                                                        $feeInvoice += $dibayar;
                                                        $totalInvoice += $sub;

                                                        if ($sebagai === 'Helper') {
                                                            $helperInv += $sub;
                                                            $helperFee += $dibayar;
                                                        } else if ($sebagai === 'Driver') {
                                                            $driverInv += $sub;
                                                            $driverFee += $dibayar;
                                                        } else if ($sebagai === 'Korlap') {
                                                            $korlapInv += $sub;
                                                            $korlapFee += $dibayar;
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td><?php echo strtoupper($record->id); ?></td>
                                                            <td><?php echo ucwords($record->nama); ?></td>
                                                            <td><?php echo $sebagai; ?></td>
                                                            <td><?php echo $intSKR; ?></td>
                                                            <td><?php echo $intRit2; ?></td>
                                                            <td><?php echo $intInap; ?></td>
                                                            <td><?php echo $intUpah; ?></td>
                                                            <td><?php echo $intAbsen; ?></td>
                                                            <td><?php echo $intBolos; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <?php
                                                $hasilInvoice = $feeInvoice + $totalInvoice;
                                                $ppnInvoice = 0;
                                                ?>
                                                <tr>
                                                    <td colspan="9" style="border: none;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">PENGADAAN PENGEMUDI</td>
                                                    <td colspan="3" class="text-right"><strong><?php echo 'Rp. ' . number_format($driverInv, 0, ',', '.'); ?></strong></td>
                                                    <td colspan="2" class="text-right">MANAGEMENT FEE</td>
                                                    <td colspan="2" class="text-right"><strong><?php echo 'Rp. ' . number_format($driverFee, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">PENGADAAN KERNET</td>
                                                    <td colspan="3" class="text-right"><strong><?php echo 'Rp. ' . number_format($helperInv, 0, ',', '.'); ?></strong></td>
                                                    <td colspan="2" class="text-right">MANAGEMENT FEE</td>
                                                    <td colspan="2" class="text-right"><strong><?php echo 'Rp. ' . number_format($helperFee, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">PENGADAAN PENGAWAS</td>
                                                    <td colspan="3" class="text-right"><strong><?php echo 'Rp. ' . number_format($korlapInv, 0, ',', '.'); ?></strong></td>
                                                    <td colspan="2" class="text-right">MANAGEMENT FEE</td>
                                                    <td colspan="2" class="text-right"><strong><?php echo 'Rp. ' . number_format($korlapFee, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" style="border: none;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">JASA</td>
                                                    <td class="text-right"><strong><?php echo 'Rp. ' . number_format($totalInvoice, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">MANAGEMENT FEE</td>
                                                    <td class="text-right"><strong><?php echo 'Rp. ' . number_format($feeInvoice, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">SUB-TOTAL</td>
                                                    <td class="text-right"><strong><?php echo 'Rp. ' . number_format($hasilInvoice, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">PPN</td>
                                                    <?php
                                                    if ($rProyek->invoice_ppn > 0) {
                                                        $ppnInvoice = round(($rProyek->invoice_ppn / 100) * $hasilInvoice);
                                                    }
                                                    ?>
                                                    <td class="text-right"><strong><?php echo 'Rp. ' . number_format($ppnInvoice, 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">TOTAL</td>
                                                    <td class="text-right" id="total-td"><strong><?php echo 'Rp. ' . number_format(($hasilInvoice + $ppnInvoice), 0, ',', '.'); ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" class="text-center"><strong><?php echo $this->terbilang->format(($hasilInvoice + $ppnInvoice), 'u'); ?> RUPIAH</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                $('#total-span').html($('#total-td').html());
                fillInputs();

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/ig/tabelInv'); ?>", $('#model-form'));
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=0',
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#proyek-input').val('<?php echo $rProyek->kode; ?>');
                        $('#presensi-input').val('<?php echo $kodePresensi; ?>');
                        $('#lembur-input').val('<?php echo (($kodeLembur === 'na') ? '-' : $kodeLembur); ?>');
                        $('#nomor-input').val('<?php echo $txtInv; ?>');
                        $('#bayar_driver-input').val('<?php echo $driverInv; ?>');
                        $('#fee_driver-input').val('<?php echo $driverFee; ?>');
                        $('#bayar_helper-input').val('<?php echo $helperInv; ?>');
                        $('#fee_helper-input').val('<?php echo $helperFee; ?>');
                        $('#bayar_korlap-input').val('<?php echo $korlapInv; ?>');
                        $('#fee_korlap-input').val('<?php echo $korlapFee; ?>');
                        $('#ppn-input').val('<?php echo $ppnInvoice; ?>');
                    }
                });
            }
        </script>
    </body>
</html>