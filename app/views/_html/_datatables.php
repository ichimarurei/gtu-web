<script src="<?php echo base_url('res/SmartAdmin/js/plugin/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/datatables/dataTables.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/datatables/dataTables.tableTools.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/datatables/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/datatable-responsive/datatables.responsive.min.js'); ?>"></script>
<script>
    function initTable(url, tabel, kolom) {
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $(tabel).dataTable({
            "ajax": url,
            "columns": kolom,
            "deferRender": true,
            "ordering": false,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
                'sLengthMenu': 'Menampilkan _MENU_ data', 'sInfo': 'Menampilkan data _START_ - _END_ / ( _TOTAL_ )',
                'sInfoFiltered': '(hasil filter dari _MAX_ data)',
                'sZeroRecords': 'Tidak ada data', 'sInfoEmpty': 'Tidak ada data', 'sEmptyTable': 'Tidak ada data',
                'sLoadingRecords': 'Tunggu beberapa saat...', 'sProcessing': 'Memproses...',
                'oPaginate': {
                    'sFirst': '<i class="fa fa-angle-double-left fa-xs"></i>',
                    'sLast': '<i class="fa fa-angle-double-right fa-xs"></i>',
                    'sNext': '<i class="fa fa-angle-right fa-xs"></i>', 'sPrevious': '<i class="fa fa-angle-left fa-xs"></i>'
                }
            },
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($(tabel), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            },
            "fnDrawCallback": function (oSettings) {
                aksi(tabel);
            }
        });
    }
</script>