<!-- #NAVIGATION -->
<?php
$halamanAktif = '';
$extraParam = '';

if ($this->uri->segment(4) !== FALSE) {
    $halamanAktif = $this->uri->segment(3) . '/' . $this->uri->segment(4);
}

if ($this->uri->segment(5) !== FALSE) {
    $extraParam = $this->uri->segment(5);
}
?>
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <nav>
        <ul>
            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd'))) { ?>
                <li<?php
                echo (in_array($halamanAktif, array(
                    'master/dataTipe', 'master/dataLokasi', 'master/dataKendaraan', 'master/dataJabatan', 'master/dataBank',
                    'master/tabelTipe', 'master/tabelLokasi', 'master/tabelKendaraan', 'master/tabelJabatan', 'master/tabelBank'
                ))) ? ' class="active open"' : '';
                ?>>
                    <a href="#" title="Data Master"><i class="fa fa-lg fa-fw fa-database"></i> <span class="menu-item-parent">Data Master</span></a>
                    <ul>
                        <li<?php
                        echo (in_array($halamanAktif, array('master/dataTipe', 'master/tabelTipe'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Jenis Proyek">Jenis Proyek</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'master/dataTipe') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/dataTipe'); ?>" title="Pendataan Jenis Proyek"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'master/tabelTipe') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/tabelTipe'); ?>" title="Tabel/Daftar Jenis Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('master/dataLokasi', 'master/tabelLokasi'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Lokasi">Lokasi</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'master/dataLokasi') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/dataLokasi'); ?>" title="Pendataan Lokasi"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'master/tabelLokasi') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/tabelLokasi'); ?>" title="Tabel/Daftar Lokasi"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('master/dataKendaraan', 'master/tabelKendaraan'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Kendaraan">Kendaraan</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'master/dataKendaraan') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/dataKendaraan'); ?>" title="Pendataan Kendaraan"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'master/tabelKendaraan') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/tabelKendaraan'); ?>" title="Tabel/Daftar Kendaraan"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('master/dataJabatan', 'master/tabelJabatan'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Jabatan">Jabatan</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'master/dataJabatan') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/dataJabatan'); ?>" title="Pendataan Jabatan"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'master/tabelJabatan') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/tabelJabatan'); ?>" title="Tabel/Daftar Jabatan"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('master/dataBank', 'master/tabelBank'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Bank">Bank</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'master/dataBank') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/dataBank'); ?>" title="Pendataan Bank"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'master/tabelBank') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/master/tabelBank'); ?>" title="Tabel/Daftar Bank"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li<?php
                echo (in_array($halamanAktif, array('proyek/dataProyek', 'proyek/tabelProyek', 'proyek/dataAkun', 'proyek/tabelAkun', 'proyek/tabelGPS', 'proyek/dataGPS'))) ? ' class="active open"' : '';
                ?>>
                    <a href="#" title="Data Proyek"><i class="fa fa-lg fa-fw fa-cab"></i> <span class="menu-item-parent">Proyek</span></a>
                    <ul>
                        <li<?php
                        echo (in_array($halamanAktif, array('proyek/dataProyek', 'proyek/tabelProyek'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Info Proyek">Data Proyek</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'proyek/dataProyek') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/dataProyek'); ?>" title="Pendataan Proyek"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelProyek') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>" title="Tabel/Daftar Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('proyek/dataAkun', 'proyek/tabelAkun'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Akun Proyek">Akun Proyek</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'proyek/dataAkun') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/dataAkun'); ?>" title="Pendataan Akun Proyek"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelAkun') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelAkun'); ?>" title="Tabel/Daftar Akun Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('proyek/dataShipment', 'proyek/tabelShipment'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data Shipment Proyek">Shipment Proyek</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'proyek/dataShipment') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/dataShipment'); ?>" title="Pendataan Shipment Proyek"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelShipment') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelShipment'); ?>" title="Tabel/Daftar Shipment Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php echo ($halamanAktif === 'proyek/tabelGPS') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/proyek/tabelGPS'); ?>" title="Tabel/Daftar Riwayat GPS"><span class="menu-item-parent">Riwayat GPS</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'proyek/dataGPS') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/proyek/dataGPS'); ?>" title="Lihat Live GPS"><span class="menu-item-parent">Live GPS</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <li<?php
            echo (in_array($halamanAktif, array(
                'akun/dataProfil', 'akun/tabelProfil', 'akun/dataAkun', 'proyek/dataPegawai', 'proyek/tabelPegawai',
                'proyek/tabelProfil', 'presensi/dataCuti', 'presensi/tabelCuti', 'presensi/tabelCutiQ', 'akun/tabelQR'
            ))) ? ' class="active open"' : '';
            ?>>
                <a href="#" title="Data HRD"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">HRD</span></a>
                <ul>
                    <li<?php
                    echo (in_array($halamanAktif, array(
                        'akun/dataProfil', 'akun/dataAkun', 'proyek/dataPegawai', 'proyek/tabelPegawai',
                        'akun/tabelProfil', 'proyek/tabelProfil', 'akun/tabelQR', 'akun/laporanKarir'
                    ))) ? ' class="active open"' : '';
                    ?>>
                        <a href="#" title="Data Pegawai">Pegawai</a>
                        <ul>
                            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd', 'shrd'))) { ?>
                                <li<?php
                                echo (in_array($halamanAktif, array(
                                    'akun/dataProfil', 'akun/dataAkun', 'proyek/dataPegawai', 'proyek/tabelPegawai'
                                ))) ? ' class="active"' : '';
                                ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/dataProfil'); ?>" title="Pendataan Pegawai"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'akun/tabelProfil' && $extraParam === 'semua') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>" title="Tabel/Daftar Semua Pegawai"><span class="menu-item-parent">Tabel/Daftar Semua</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'akun/tabelProfil' && $extraParam === 'aktif') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/aktif'); ?>" title="Tabel/Daftar Pegawai Aktif"><span class="menu-item-parent">Tabel/Daftar Aktif</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'akun/tabelProfil' && $extraParam === 'pasif') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/pasif'); ?>" title="Tabel/Daftar Pegawai Tidak Aktif"><span class="menu-item-parent">Tabel/Daftar Tidak Aktif</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'akun/tabelQR') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/tabelQR'); ?>" title="Cetak QR Pegawai"><span class="menu-item-parent">QR Pegawai</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'akun/laporanKarir') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/akun/laporanKarir'); ?>" title="Karir Pegawai"><span class="menu-item-parent">Karir Pegawai</span></a>
                                </li>
                            <?php } else { ?>
                                <li<?php echo ($halamanAktif === 'proyek/tabelProfil' && $extraParam === 'aktif') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelProfil/aktif'); ?>" title="Tabel/Daftar Pegawai Aktif"><span class="menu-item-parent">Tabel/Daftar Aktif</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelProfil' && $extraParam === 'pasif') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelProfil/pasif'); ?>" title="Tabel/Daftar Pegawai Tidak Aktif"><span class="menu-item-parent">Tabel/Daftar Tidak Aktif</span></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li<?php
                    echo (in_array($halamanAktif, array('presensi/dataPresensi', 'presensi/tabelPresensi', 'presensi/tabelPresensiQ'))) ? ' class="active open"' : '';
                    ?>>
                        <a href="#" title="Absensi Kehadiran">Absensi Kehadiran</a>
                        <ul>
                            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd', 'korlap'))) { ?>
                                <li<?php echo ($halamanAktif === 'presensi/dataPresensi') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/presensi/dataPresensi'); ?>" title="Pendataan Absensi Kehadiran"><span class="menu-item-parent">Absensi</span></a>
                                </li>
                            <?php } if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd'))) { ?>
                                <li<?php echo ($halamanAktif === 'presensi/tabelPresensi') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>" title="Tabel/Daftar Absensi Semua Pegawai"><span class="menu-item-parent">Tabel Absensi Pegawai</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'presensi/tabelLembur') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/presensi/tabelLembur'); ?>" title="Tabel/Daftar Lembur Semua Pegawai"><span class="menu-item-parent">Tabel Lembur Pegawai</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'presensi/tabelPulang') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/presensi/tabelPulang'); ?>" title="Tabel/Daftar Absen Pulang Pegawai"><span class="menu-item-parent">Tabel Pulang Pegawai</span></a>
                                </li>
                            <?php } ?>
                            <li<?php echo ($halamanAktif === 'presensi/tabelPresensiQ') ? ' class="active"' : ''; ?>>
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelPresensiQ'); ?>" title="Tabel/Daftar Absensi Saya"><span class="menu-item-parent">Tabel Absensi Saya</span></a>
                            </li>
                            <li<?php echo ($halamanAktif === 'presensi/tabelLemburQ') ? ' class="active"' : ''; ?>>
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelLemburQ'); ?>" title="Tabel/Daftar Lembur Saya"><span class="menu-item-parent">Tabel Lembur Saya</span></a>
                            </li>
                        </ul>
                    </li>
                    <li<?php
                    echo (in_array($halamanAktif, array('presensi/dataCuti', 'presensi/tabelCuti', 'presensi/tabelCutiQ'))) ? ' class="active open"' : '';
                    ?>>
                        <a href="#" title="Ajuan Cuti">Cuti</a>
                        <ul>
                            <li<?php echo ($halamanAktif === 'presensi/dataCuti') ? ' class="active"' : ''; ?>>
                                <a href="<?php echo site_url('modul/tampil/presensi/dataCuti'); ?>" title="Pendataan Ajuan Cuti"><span class="menu-item-parent">Pengajuan</span></a>
                            </li>
                            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd'))) { ?>
                                <li<?php echo ($halamanAktif === 'presensi/tabelCuti') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/presensi/tabelCuti'); ?>" title="Tabel/Daftar Cuti Semua Pegawai"><span class="menu-item-parent">Tabel Cuti Pegawai</span></a>
                                </li>
                            <?php } ?>
                            <li<?php echo ($halamanAktif === 'presensi/tabelCutiQ') ? ' class="active"' : ''; ?>>
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelCutiQ'); ?>" title="Tabel/Daftar Cuti Saya"><span class="menu-item-parent">Tabel Cuti Saya</span></a>
                            </li>
                        </ul>
                    </li>
                    <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd', 'shrd'))) { ?>
                        <li<?php
                        echo (in_array($halamanAktif, array('proyek/dataSP', 'proyek/tabelSP'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data SP">Surat Peringatan</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'proyek/dataSP') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/dataSP'); ?>" title="Pendataan SP"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelSP') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelSP'); ?>" title="Tabel/Daftar SP"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php
                        echo (in_array($halamanAktif, array('proyek/dataSL', 'proyek/tabelSL'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Data SL">Surat Lembur</a>
                            <ul>
                                <li<?php echo ($halamanAktif === 'proyek/dataSL') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/dataSL'); ?>" title="Pendataan SL"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'proyek/tabelSL') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/proyek/tabelSL'); ?>" title="Tabel/Daftar SL"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                            </ul>
                        </li>
                        <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd'))) { ?>
                            <li<?php
                            echo (in_array($halamanAktif, array('presensi/dataPRekapRangkum', 'presensi/tabelPRekap', 'presensi/dataPArsip'))) ? ' class="active open"' : '';
                            ?>>
                                <a href="#" title="Rekap Cut/Off Presensi Pegawai">Rekap Cut/Off Absensi</a>
                                <ul>
                                    <li<?php echo ($halamanAktif === 'presensi/dataPRekapRangkum') ? ' class="active"' : ''; ?>>
                                        <a href="<?php echo site_url('modul/tampil/presensi/dataPRekapRangkum'); ?>" title="Pendataan Rekap Cut/Off Presensi"><span class="menu-item-parent">Pendataan</span></a>
                                    </li>
                                    <li<?php echo (in_array($halamanAktif, array('presensi/tabelPRekap', 'presensi/dataPArsip'))) ? ' class="active"' : ''; ?>>
                                        <a href="<?php echo site_url('modul/tampil/presensi/tabelPRekap'); ?>" title="Tabel/Daftar Rekap Cut/Off Presensi"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li<?php
                            echo (in_array($halamanAktif, array('presensi/dataLRekapRangkum', 'presensi/tabelLRekap', 'presensi/dataLArsip'))) ? ' class="active open"' : '';
                            ?>>
                                <a href="#" title="Rekap Cut/Off Lembur Pegawai">Rekap Cut/Off Lembur</a>
                                <ul>
                                    <li<?php echo ($halamanAktif === 'presensi/dataLRekapRangkum') ? ' class="active"' : ''; ?>>
                                        <a href="<?php echo site_url('modul/tampil/presensi/dataLRekapRangkum'); ?>" title="Pendataan Rekap Cut/Off Lembur"><span class="menu-item-parent">Pendataan</span></a>
                                    </li>
                                    <li<?php echo (in_array($halamanAktif, array('presensi/tabelLRekap', 'presensi/dataLArsip'))) ? ' class="active"' : ''; ?>>
                                        <a href="<?php echo site_url('modul/tampil/presensi/tabelLRekap'); ?>" title="Tabel/Daftar Rekap Cut/Off Lembur"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </li>
            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mhrd', 'mkeu', 'skeu', 'korlap'))) { ?>
                <li<?php
                echo (in_array($halamanAktif, array('klaim/dataKasbon', 'klaim/tabelKasbon', 'klaim/dataKlaim', 'klaim/tabelKlaim'))) ? ' class="active open"' : '';
                ?>>
                    <a href="#" title="Data Klaim & Kasbon"><i class="fa fa-lg fa-fw fa-credit-card-alt"></i> <span class="menu-item-parent">Klaim & Kasbon</span></a>
                    <ul>
                        <li<?php echo ($halamanAktif === 'klaim/dataKasbon') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/klaim/dataKasbon'); ?>" title="Pendataan Kasbon"><span class="menu-item-parent">Ajuan Kasbon</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'klaim/dataKlaim') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/klaim/dataKlaim'); ?>" title="Pendataan Klaim/Reimburse"><span class="menu-item-parent">Ajuan Klaim/Reimburse</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'klaim/tabelKasbon') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/klaim/tabelKasbon'); ?>" title="Tabel/Daftar Kasbon"><span class="menu-item-parent">Tabel Kasbon</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'klaim/tabelKlaim') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/klaim/tabelKlaim'); ?>" title="Tabel/Daftar Klaim/Reimburse"><span class="menu-item-parent">Tabel Klaim/Reimburse</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mkeu', 'skeu'))) { ?>
                <li<?php
                echo (in_array($halamanAktif, array('ig/tabelRekap', 'ig/dataIG', 'ig/tabelInv', 'ig/dataInv', 'ig/kelolaGaji'))) ? ' class="active open"' : '';
                ?>>
                    <a href="#" title="Data Invoice & Penggajian"><i class="fa fa-lg fa-fw fa-calculator"></i> <span class="menu-item-parent">Invoice & Gaji</span></a>
                    <ul>
                        <li<?php echo (in_array($halamanAktif, array('ig/tabelRekap', 'ig/dataIG'))) ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/ig/tabelRekap'); ?>" title="Pendataan Invoice & Penggajian"><span class="menu-item-parent">Proses Invoice & Gaji</span></a>
                        </li>
                        <li<?php echo (in_array($halamanAktif, array('ig/tabelInv', 'ig/dataInv'))) ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/ig/tabelInv'); ?>" title="Tabel/Daftar Invoice"><span class="menu-item-parent">Tabel Invoice</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'ig/kelolaGaji') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/ig/kelolaGaji'); ?>" title="Tabel/Daftar Penggajian"><span class="menu-item-parent">Tabel Penggajian</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mkeu', 'skeu', 'mhrd', 'korlap'))) { ?>
                <li<?php
                echo (in_array($halamanAktif, array(
                    'keu/tabelKacil', 'keu/dataKacil', 'keu/dataSaldo', 'keu/rekapKacil', 'keu/rekapGaji'
                ))) ? ' class="active open"' : '';
                ?>>
                    <a href="#" title="Data Keuangan"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Keuangan</span></a>
                    <ul>
                        <li<?php
                        echo (in_array($halamanAktif, array('keu/tabelKacil', 'keu/dataKacil', 'keu/dataSaldo', 'keu/rekapKacil'))) ? ' class="active open"' : '';
                        ?>>
                            <a href="#" title="Kas Kecil">Kas Kecil</a>
                            <ul>
                                <?php if (in_array($this->session->userdata('_otoritas'), array('admin', 'bos', 'mkeu', 'skeu'))) { ?>
                                    <li<?php echo ($halamanAktif === 'keu/dataSaldo') ? ' class="active"' : ''; ?>>
                                        <a href="<?php echo site_url('modul/tampil/keu/dataSaldo'); ?>" title="Pendataan Pemasukan Kas Kecil"><span class="menu-item-parent">Saldo Kas</span></a>
                                    </li>
                                <?php } ?>
                                <li<?php echo ($halamanAktif === 'keu/dataKacil') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/keu/dataKacil'); ?>" title="Pendataan Pengeluaran Kas Kecil"><span class="menu-item-parent">Pendataan</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'keu/tabelKacil') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/keu/tabelKacil'); ?>" title="Tabel/Daftar Kas Kecil"><span class="menu-item-parent">Tabel/Daftar</span></a>
                                </li>
                                <li<?php echo ($halamanAktif === 'keu/rekapKacil') ? ' class="active"' : ''; ?>>
                                    <a href="<?php echo site_url('modul/tampil/keu/rekapKacil'); ?>" title="Rekap Kas Kecil"><span class="menu-item-parent">Rekap</span></a>
                                </li>
                            </ul>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapGaji') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapGaji'); ?>" title="Rekap Gaji"><span class="menu-item-parent">Rekap Gaji</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapLembur') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapLembur'); ?>" title="Rekap Lembur"><span class="menu-item-parent">Rekap Lembur</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapUpah') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapUpah'); ?>" title="Rekap Insentif Pengiriman"><span class="menu-item-parent">Rekap Insentif Pengiriman</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapInap') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapInap'); ?>" title="Rekap Muat Inap"><span class="menu-item-parent">Rekap Muat Inap</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapRitase') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapRitase'); ?>" title="Rekap Ritase II"><span class="menu-item-parent">Rekap Ritase II</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapSKR') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapSKR'); ?>" title="Rekap SKR"><span class="menu-item-parent">Rekap SKR</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapFee') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapFee'); ?>" title="Rekap Management Fee"><span class="menu-item-parent">Rekap Management Fee</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapPPN') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapPPN'); ?>" title="Rekap PPN"><span class="menu-item-parent">Rekap PPN</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapBPJS') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapBPJS'); ?>" title="Rekap BPJS"><span class="menu-item-parent">Rekap BPJS</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapTHR') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapTHR'); ?>" title="Rekap THR"><span class="menu-item-parent">Rekap THR</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapSupervisi') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapSupervisi'); ?>" title="Rekap Supervisi"><span class="menu-item-parent">Rekap Supervisi</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/rekapSeragam') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/rekapSeragam'); ?>" title="Rekap Seragam"><span class="menu-item-parent">Rekap Seragam</span></a>
                        </li>
                        <li<?php echo ($halamanAktif === 'keu/kasBesar') ? ' class="active"' : ''; ?>>
                            <a href="<?php echo site_url('modul/tampil/keu/kasBesar'); ?>" title="Kas Besar"><span class="menu-item-parent">Kas Besar</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <li>
                <a href="<?php echo site_url('akses/untuk/keluar'); ?>"><i class="fa fa-lg fa-fw fa-sign-out"></i> <span class="menu-item-parent">Keluar</span></a>
            </li>
        </ul>
    </nav>

    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<!-- END NAVIGATION -->