<meta charset="utf-8">
<title>HRIS | PT. GARDA TRIMITRA UTAMA</title>
<meta name="description" content="HRIS | PT. GARDA TRIMITRA UTAMA">
<meta name="author" content="Muhammad Iqbal (市丸 零) <iqbal@indesc.com>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/font-awesome.min.css'); ?>">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/smartadmin-production-plugins.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/smartadmin-production.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/smartadmin-skins.min.css'); ?>">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/SmartAdmin/css/smartadmin-rtl.min.css'); ?>">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="<?php echo base_url('res/SmartAdmin/img/favicon/favicon.png'); ?>" type="image/png">
<link rel="icon" href="<?php echo base_url('res/SmartAdmin/img/favicon/favicon.png'); ?>" type="image/png">

<!-- #GOOGLE FONT -->
<link rel="stylesheet" href="<?php echo base_url('res/SmartAdmin/css/googlefonts.css'); ?>">

<!-- Sweetalert2 -->
<link href="<?php echo base_url('res/sweetalert2.js/sweetalert2.min.css'); ?>" rel="stylesheet">
<!-- File Input -->
<link rel="stylesheet" href="<?php echo base_url('res/bootstrap-fileinput/css/fileinput.min.css'); ?>">
<!-- DatePicker -->
<link rel="stylesheet" href="<?php echo base_url('res/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
<!-- TimePicker -->
<link rel="stylesheet" href="<?php echo base_url('res/bootstrap-timepicker/css/bootstrap-timepicker.min.css'); ?>">
<!-- Lightbox -->
<link href="<?php echo base_url('res/lightbox/ekko-lightbox.css'); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('res/select2/css/select2.min.css'); ?>" rel="stylesheet">
<style>
    .text-tetap {
        resize: none;
    }
    .select2-selection__arrow b::before {
        content: none;
    }
    .select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 32px;
    }
</style>