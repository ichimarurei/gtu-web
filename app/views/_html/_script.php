<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url('res/SmartAdmin/js/plugin/pace/pace.min.js'); ?>"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="<?php echo base_url('res/SmartAdmin/js/libs/jquery-3.2.1.min.js'); ?>"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url('res/SmartAdmin/js/app.config.js'); ?>"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js'); ?>"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url('res/SmartAdmin/js/bootstrap/bootstrap.min.js'); ?>"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url('res/SmartAdmin/js/smartwidgets/jarvis.widget.min.js'); ?>"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js'); ?>"></script>

<!-- SPARKLINES -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/sparkline/jquery.sparkline.min.js'); ?>"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/jquery-validate/jquery.validate.min.js'); ?>"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/masked-input/jquery.maskedinput.min.js'); ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url('res/select2/js/select2.full.min.js'); ?>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js'); ?>"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/msie-fix/jquery.mb.browser.min.js'); ?>"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/fastclick/fastclick.min.js'); ?>"></script>

<!--[if IE 8]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url('res/SmartAdmin/js/app.min.js'); ?>"></script>

<!-- Sweetalert -->
<script src="<?php echo base_url('res/sweetalert2.js/sweetalert2.min.js'); ?>"></script>
<!-- jQuery blockUI -->
<script src="<?php echo base_url('res/sweetalert2.js/jquery.blockUI.js'); ?>"></script>
<!-- File Input -->
<script src="<?php echo base_url('res/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>
<!-- DatePicker -->
<script src="<?php echo base_url('res/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('res/bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min.js'); ?>"></script>
<!-- TimePicker -->
<script src="<?php echo base_url('res/bootstrap-timepicker/js/bootstrap-timepicker.min.js'); ?>"></script>
<!-- Lightbox -->
<script src="<?php echo base_url('res/lightbox/ekko-lightbox.min.js'); ?>"></script>
<!-- Morris Chart Dependencies -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/morris/raphael.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/morris/morris.min.js'); ?>"></script>
<script>
    runAllForms();
    pageSetUp();

    $(document).keypress(function (e) {
        if (e.which === 13) { // enter key press event
            return (e.target.nodeName.toLowerCase() === 'textarea'); // disable enter event except for textarea
        }
    });

    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    function doSave(text, url, form) {
        $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false,
            success: function (response) {
                var title = 'Success';
                var message = '';
                var icon = 'warning';

                if (response['return'].code === 0) {
                    title = 'Eror';
                    icon = 'error';
                    message = 'Error !!!';
                } else if (response['return'].code === 1) {
                    title = 'Berhasil';
                    icon = 'success';
                    message = 'Data Berhasil ' + text;
                } else if (response['return'].code === 2) {
                    title = 'Gagal';
                    icon = 'error';
                    message = 'Data Gagal ' + text;
                } else if (response['return'].code === 3) {
                    title = 'Perhatian !!!';
                    icon = 'warning';
                    message = response['return'].message;
                }

                swal({
                    title: title,
                    html: message,
                    timer: 3000,
                    type: icon,
                    showConfirmButton: false,
                    onClose: function () {
                        $.unblockUI();

                        if (response['return'].code === 1) {
                            $(location).attr('href', url);
                        }
                    }
                });
            }
        });
    }

    function doSaveCallback(form) {
        return $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false
        });
    }

    function formatRp(angka) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        var separator;

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = (split[1] !== undefined) ? rupiah + ',' + split[1] : rupiah;
        return (rupiah ? 'Rp. ' + rupiah : '');
    }

    // duplication image processing
    function setImageAs(img, as, dir) {
        return $.ajax({
            url: "<?php echo site_url('data/salinFoto'); ?>", data: 'img=' + img + '&dir=' + dir + '&as=' + as,
            dataType: 'json', type: 'POST', cache: false
        });
    }

    // retrieve galleries
    function lihatGaleri(dir) {
        return $.ajax({
            url: "<?php echo site_url('data/galeri'); ?>", data: 'dir=' + dir,
            dataType: 'json', type: 'POST', cache: false
        });
    }
</script>