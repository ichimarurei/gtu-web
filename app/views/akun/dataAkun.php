<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Akun</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Akun
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Akun</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="akun">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" name="proyek-input" value="-">
                                            <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ID Pengguna</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="ID Pengguna" type="text" id="id-input" name="id-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">Sandi</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Sandi" type="password" id="pin-input" name="pin-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Otoritas</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="otoritas-input" name="otoritas-input">
                                                            <option value="admin">Admin</option>
                                                            <option value="bos">Owner</option>
                                                            <option value="mhrd">Manajer HRD</option>
                                                            <option value="shrd">Staf HRD</option>
                                                            <option value="mkeu">Manajer Keuangan</option>
                                                            <option value="skeu">Spv Keuangan</option>
                                                            <option value="korlap">Korlap</option>
                                                            <option value="pegawai">Pegawai</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param) {
                if (param === '0') {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>");
                } else {
                    $.ajax({
                        url: "<?php echo site_url('data/detail'); ?>", data: 'param=akun&kode=' + param,
                        dataType: 'json', type: 'POST', cache: false,
                        success: function (response) {
                            $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                            $('#key-input').val(response['data'].key);
                            $('#kode-input').val(response['data'].kode);
                            $('#id-input').val(response['data'].id);
                            $('#pin-input').val(response['data'].pin);
                            $('#biodata-input').val(param);
                            $('#otoritas-input').val(response['data'].otoritas).trigger('change');
                            $('#id-input').focus();

                            if (response['data'].key > 0) {
                                $('#biodata-input').val(response['data'].biodata);
                                $('#id-input').attr('readonly', true);
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>