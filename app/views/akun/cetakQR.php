<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <style>
        #qr-ls-div { background-color: white; }
    </style>
    <body>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-block" type="button" id="cetak-button">
                    <i class="fa fa-file-pdf-o"></i>
                    Cetak
                </button>
            </div>
        </div>
        <div class="row" id="qr-ls-div"></div>

        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <!-- QR-CODE -->
        <script src="<?php echo base_url('res/jquery-qrcode/jquery.qrcode.min.js'); ?>"></script>
        <script src="<?php echo base_url('res/jsPDF/dist/jspdf.min.js'); ?>"></script>
        <script>
            var urlParam = '<?php echo $this->uri->segment(5); ?>';
            var meParam = '<?php echo $this->uri->segment(6); ?>';
            var datanya;

            $(document).ready(function () {
                $('#cetak-button').hide();
                tampilQR();
                $('#cetak-button').click(function () {
                    var doc = new jsPDF();

                    $.each(datanya, function (index, value) {
                        var isShow = (meParam === '');

                        if (!isShow) {
                            isShow = meParam === value.kode;
                        }

                        if (isShow) {
                            doc.addImage($('#qr-ls-div > .qr-pegawai > #qr-div-' + index + ' > img').attr('src'), 'JPEG', 15, 15, 180, 180);
                            doc.addPage();
                        }
                    });

                    doc.save('qr-pegawai.pdf');
                });
            });

            function tampilQR() {
                $.ajax({
                    url: "<?php echo site_url('data/tabel/bio'); ?>/s_1___p_" + urlParam,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var konten = '';

                        $.each(response.data, function (index, value) { // ngumpulin hasil dari server
                            var isShow = (meParam === '');

                            if (!isShow) {
                                isShow = meParam === value.kode;
                            }

                            if (isShow) {
                                konten += '<div class="col-sm-12 text-center qr-pegawai" style="padding: 10px;">';
                                konten += '<div id="qr-div-' + index + '"></div>';
                                konten += '<br><br><br></div>';
                            }
                        });

                        $('#qr-ls-div').html(konten);

                        $.each(response.data, function (index, value) {
                            $('#qr-ls-div > .qr-pegawai > #qr-div-' + index).qrcode({
                                render: 'image',
                                size: 444,
                                text: value.kode
                            });
                        });

                        datanya = response.data;
                        $('#cetak-button').show();
                    }
                });
            }
        </script>
    </body>
</html>
