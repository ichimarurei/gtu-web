<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="bio">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="rekening-input" name="rekening-input" value="">
                                            <input type="hidden" id="telepon-input" name="telepon-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ID Pegawai</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="ID Pegawai" type="text" id="id-input" name="id-input" readonly>
                                                    </div>
                                                    <label class="col-md-2 control-label">KTP</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No KTP" type="text" id="ktp-input" name="ktp-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">NPWP</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No NPWP" type="text" id="npwp-input" name="npwp-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">BPJS</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No BPJS" type="text" id="bpjs-input" name="bpjs-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Nama</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Nama Lengkap Pegawai" type="text" id="nama-input" name="nama-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Kelamin</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="kelamin-input" name="kelamin-input">
                                                            <option value="cowok">Laki-Laki</option>
                                                            <option value="cewek">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-md-2 control-label">Agama</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="agama-input" name="agama-input">
                                                            <option value="Islam">Islam</option>
                                                            <option value="Katolik">Katholik</option>
                                                            <option value="Protestan">Protestan</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Budha">Budha</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tempat Lahir</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tempat Lahir" type="text" id="tempat_lahir-input" name="tempat_lahir-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">Tanggal Lahir</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tanggal Lahir" type="text" id="tanggal_lahir-input" name="tanggal_lahir-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Telepon</label>
                                                    <div class="col-md-5">
                                                        <input class="form-control" placeholder="No Telepon Utama" type="text" id="telepon-utama">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input class="form-control" placeholder="No Telepon Alternatif" type="text" id="telepon-alternatif">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Email</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Email Pegawai" type="email" id="email-input" name="email-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Status Pernikahan</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                            <option value="belum">Belum Menikah</option>
                                                            <option value="nikah">Menikah</option>
                                                            <option value="cerai">Bercerai</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-md-2 control-label">Jumlah Anak</label>
                                                    <div class="col-md-2">
                                                        <input class="form-control" placeholder="Jumlah Anak" type="text" id="anak-input" name="anak-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Rekening</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="bank-rekening"></select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control" placeholder="No Rekening" type="text" id="no-rekening">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pendidikan</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="pendidikan-input" name="pendidikan-input">
                                                            <option>TK</option>
                                                            <option>SD</option>
                                                            <option>SMP</option>
                                                            <option>SMA</option>
                                                            <option>S1</option>
                                                            <option>S2</option>
                                                            <option>S3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Alamat</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control text-tetap" placeholder="Alamat Lengkap Pegawai" rows="4" id="alamat-input" name="alamat-input"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Unggah</label>
                                                    <div class="col-md-10">
                                                        <button class="btn btn-default unggah-button" data-untuk="foto" type="button">Foto</button>
                                                        <button class="btn btn-default unggah-button" data-untuk="ktp" type="button">KTP</button>
                                                        <button class="btn btn-default unggah-button" data-untuk="kk" type="button">Kartu Keluarga</button>
                                                        <button class="btn btn-default unggah-button" data-untuk="sim" type="button">SIM</button>
                                                        <button class="btn btn-default unggah-button" data-untuk="ijazah" type="button">Ijazah</button>
                                                        <button class="btn btn-default unggah-button" data-untuk="npwp" type="button">NPWP</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="row" id="galeri-div">
                                                <hr>
                                                <div class="superbox col-sm-12" id="galeri-akun">
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                        <button class="btn btn-info" type="button" id="lanjut-button">
                                                            <i class="fa fa-chevron-right"></i>
                                                            Simpan &amp; Lanjutkan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="unggah-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Unggah Berkas</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="image/*">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var imgFor = '';
            var pics = ['foto.jpg', 'ktp.jpg', 'kk.jpg', 'sim.jpg', 'ijazah.jpg', 'npwp.jpg'];

            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');
                initUploader();

                $('#bank-rekening').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Bank'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bank'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});

                    if ($('#bank-rekening').val() !== null && $('#no-rekening').val() !== '') {
                        $('#rekening-input').val($('#bank-rekening').val() + '_' + $('#no-rekening').val());
                    }

                    if ($('#telepon-utama').val() !== '') {
                        var telp = '-';

                        if ($('#telepon-alternatif').val() !== '') {
                            telp = $('#telepon-alternatif').val();
                        }

                        $('#telepon-input').val($('#telepon-utama').val() + '_' + telp);
                    }

                    doSave('Disimpan', "<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>", $('#model-form'));
                });
                $('#lanjut-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});

                    if ($('#bank-rekening').val() !== null && $('#no-rekening').val() !== '') {
                        $('#rekening-input').val($('#bank-rekening').val() + '_' + $('#no-rekening').val());
                    }

                    if ($('#telepon-utama').val() !== '') {
                        var telp = '-';

                        if ($('#telepon-alternatif').val() !== '') {
                            telp = $('#telepon-alternatif').val();
                        }

                        $('#telepon-input').val($('#telepon-utama').val() + '_' + telp);
                    }

                    doSave('Disimpan', "<?php echo site_url('modul/tampil/akun/dataAkun'); ?>/" + $('#kode-input').val(), $('#model-form'));
                });
                $('.unggah-button').click(function () {
                    $('#myModalLabel').text('Unggah Berkas ' + $(this).text().toUpperCase());
                    imgFor = $(this).data('untuk');
                    $('#unggah-modal').modal();
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#id-input').val(response['data'].id);
                        $('#ktp-input').val(response['data'].ktp);
                        $('#npwp-input').val(response['data'].npwp);
                        $('#bpjs-input').val(response['data'].bpjs);
                        $('#nama-input').val(response['data'].nama);
                        $('#kelamin-input').val(response['data'].kelamin).trigger('change');
                        $('#tempat_lahir-input').val(response['data'].tempat_lahir);
                        $('#tanggal_lahir-input').val(response['data'].tanggal_lahir);
                        $('#agama-input').val(response['data'].agama).trigger('change');
                        $('#pendidikan-input').val(response['data'].pendidikan).trigger('change');
                        $('#rekening-input').val(response['data'].rekening);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#telepon-input').val(response['data'].telepon);
                        $('#email-input').val(response['data'].email);
                        $('#nikah-input').val(response['data'].nikah).trigger('change');
                        $('#anak-input').val(response['data'].anak);
                        $('#galeri-div').hide();

                        if (response['data'].key > 0) {
                            var rekeningBank = response['data'].rekening.split('_');
                            var noTelp = response['data'].telepon.split('_');
                            var pilihBank = $('#bank-rekening');
                            $('#no-rekening').val(rekeningBank[1]);
                            $('#telepon-utama').val(noTelp[0]);

                            if (noTelp[1] !== '-') {
                                $('#telepon-alternatif').val(noTelp[1]);
                            }

                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=bank&kode=' + rekeningBank[0],
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihBank.append(new Option(data['data'].bank, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihBank.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.when(lihatGaleri('etc/akun/' + response['data'].kode)).done(function (galleries) {
                                var images = '';

                                $.each(galleries.galeri, function (index, value) {
                                    if (pics.includes(value)) {
                                        images += '<div class="superbox-list">';
                                        images += '<a href="<?php echo base_url('etc/akun'); ?>/' + response['data'].kode + '/' + value + '" data-toggle="lightbox" data-gallery="galeri-' + response['data'].kode + '">';
                                        images += '<img src="<?php echo base_url('etc/akun'); ?>/' + response['data'].kode + '/' + value + '" class="superbox-img">';
                                        images += '</a>';
                                        images += '</div>';
                                    }
                                });

                                $('#galeri-akun').html(images);

                                if (images !== '') {
                                    $('#galeri-div').show();
                                }
                            });
                        }
                    }
                });
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 1, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/akun', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '' && imgFor !== '') {
                        setImageAs(filename, imgFor, 'etc/akun/' + $('#kode-input').val());
                    }

                    $('#filedocument').fileinput('clear');
                    $('#unggah-modal').modal('hide');
                });
            }
        </script>
    </body>
</html>