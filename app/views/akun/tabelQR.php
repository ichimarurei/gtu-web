<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Tabel</li><li>QR Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
                <span class="ribbon-button-alignment pull-right">
                    <span id="cetak-span" class="btn btn-ribbon hidden-xs" data-title="Cetak"><a href="" target="_blank" id="cetak-a" style="color: white;"><i class="fa fa-file-pdf-o"></i> Cetak QR Pegawai</a></span>
                </span>
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                QR Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>QR Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row" id="qr-ls-div">
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <!-- QR-CODE -->
        <script src="<?php echo base_url('res/jquery-qrcode/jquery.qrcode.min.js'); ?>"></script>
        <script>
            var urlParam = 'none';

            $(document).ready(function () {
                $('#cetak-span').hide();

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel
                    if ($('#proyek-filter').val() !== null) {
                        urlParam = $('#proyek-filter').val();
                        tampilQR();
                    }
                });

                $(document).on('click', '.klik-me', function () {
                    window.open('<?php echo site_url('modul/tampil/akun/cetakQR'); ?>/' + urlParam + '/' + $(this).data('kode'), '_blank');
                });
            });

            function tampilQR() {
                $.ajax({
                    url: "<?php echo site_url('data/tabel/bio'); ?>/s_1___p_" + urlParam,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var konten = '';

                        $.each(response.data, function (index, value) { // ngumpulin hasil dari server
                            konten += '<div class="col-sm-2 text-center qr-pegawai" style="padding: 10px; cursor: pointer;">';
                            konten += '<div class="klik-me" data-kode="' + value.kode + '" id="qr-div-' + index + '"></div>' + value.nama;
                            konten += '</div>';
                        });

                        $('#cetak-a').attr('href', '<?php echo site_url('modul/tampil/akun/cetakQR'); ?>/' + urlParam);
                        $('#qr-ls-div').html(konten);
                        $('#cetak-span').show();

                        $.each(response.data, function (index, value) {
                            $('#qr-ls-div > .qr-pegawai > #qr-div-' + index).qrcode({
                                render: 'image',
                                size: 100,
                                text: value.kode
                            });
                        });
                    }
                });
            }
        </script>
    </body>
</html>