<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$paramURL = ($this->uri->segment(5) === FALSE) ? '0' : $this->uri->segment(5);
$periodURL = ($this->uri->segment(6) === FALSE) ? 'p1' : $this->uri->segment(6);
$extraURL = ($this->uri->segment(7) === FALSE) ? '0' : $this->uri->segment(7);
$stats = array(
    '01' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '02' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '03' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '04' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '05' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '06' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '07' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '08' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '09' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '10' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '11' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0),
    '12' => array('h' => 0, 'i' => 0, 'a' => 0, 's' => 0)
);
$perPegawai = array();
$thnPegawai = array();
$todayInfo = array();
$months = array(
    'januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06',
    'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12', '0' => ''
);
$listPegawai = $this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc'));

if ($paramURL !== '0') {
    $vals = array('hadir' => 'h', 'izin' => 'i', 'alfa' => 'a', 'sakit' => 's');

    foreach ($this->model->getList(array('table' => 'data_presensi_info', 'where' => array('proyek' => $paramURL))) as $record) {
        $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->biodata, 'proyek' => $paramURL, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

        if ($rKontrak != NULL) {
            // kontrak masih berlaku
            if ($rKontrak->habis >= date('Y-m-d')) {
                $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');

                if (in_array($sebagai, array('Helper', 'Driver', 'Korlap'))) {
                    $bulan = substr($record->waktu, 5, 2);
                    $tanggal = substr($record->waktu, 0, 10);
                    $stats[$bulan][$vals[$record->status]] ++;

                    if ($extraURL !== '0') {
                        $rLembur = $this->model->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('presensi' => $record->kode)));

                        if (!isset($perPegawai[$record->biodata][$bulan][$vals[$record->status]])) {
                            $perPegawai[$record->biodata][$bulan][$vals[$record->status]] = 1;
                        } else {
                            $perPegawai[$record->biodata][$bulan][$vals[$record->status]] ++;
                        }

                        if (!isset($thnPegawai[$record->biodata][$vals[$record->status]])) {
                            $thnPegawai[$record->biodata][$vals[$record->status]] = 1;
                        } else {
                            $thnPegawai[$record->biodata][$vals[$record->status]] ++;
                        }

                        if ($tanggal === date('Y-m-d')) {
                            $jamIn = $tanggal . ' ' . $rKontrak->masuk . ':00';
                            $jamOut = $tanggal . ' ' . $rKontrak->selesai . ':00';
                            $rPulang = $this->model->getRecord(array('table' => 'data_presensi_pulang', 'where' => array('presensi' => $record->kode)));
                            $todayInfo[$record->biodata]['masuk'] = array('status' => $record->status, 'waktu' => $record->waktu, 'harus' => date('Y-m-d H:i:s', strtotime($jamIn)));

                            if ($rPulang != NULL) {
                                $todayInfo[$record->biodata]['pulang'] = array('status' => 'pulang', 'waktu' => $rPulang->waktu, 'harus' => date('Y-m-d H:i:s', strtotime($jamOut)));
                            }
                        }

                        if ($rLembur != NULL) {
                            if (!isset($perPegawai[$record->biodata][$bulan]['l'])) {
                                $perPegawai[$record->biodata][$bulan]['l'] = 1;
                            } else {
                                $perPegawai[$record->biodata][$bulan]['l'] ++;
                            }

                            if ($tanggal === date('Y-m-d')) {
                                $todayInfo[$record->biodata]['lembur'] = TRUE;
                            }
                        }
                    }
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Laporan</li><li>HRD</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-briefcase"></i>
                            Laporan
                            <span>>
                                HRD
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>Kehadiran <b><?php echo date('Y'); ?></b></h2>
                                    <div class="widget-toolbar">
                                        <a href="<?php echo site_url('modul/tampil/akun/laporanKarir/' . $paramURL . '/p1'); ?>" class="btn btn-primary">Januari - Juni</a>
                                        <a href="<?php echo site_url('modul/tampil/akun/laporanKarir/' . $paramURL . '/p2'); ?>" class="btn btn-primary">Juli - Desember</a>
                                    </div>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <?php if ($periodURL === 'p1') { ?>
                                            <div id="presensi-bar-1" class="chart no-padding" style="cursor: pointer;"></div>
                                        <?php } ?>
                                        <?php if ($periodURL === 'p2') { ?>
                                            <div id="presensi-bar-2" class="chart no-padding" style="cursor: pointer;"></div>
                                        <?php } ?>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-1"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-suitcase"></i> </span>
                                    <h2>Detail Bulan <b><?php echo ($extraURL !== '0') ? strtoupper($extraURL) . ' ' . date('Y') : '?'; ?></b></h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>ID Pegawai</th>
                                                        <th>Nama</th>
                                                        <th>Jabatan</th>
                                                        <th>Hadir</th>
                                                        <th>Izin</th>
                                                        <th>Alfa</th>
                                                        <th>Sakit</th>
                                                        <th>Lembur</th>
                                                        <th style="width: 8%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($listPegawai as $record) {
                                                        $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $paramURL, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                                                        $isShow = FALSE;
                                                        $sebagai = '';

                                                        if ($rKontrak != NULL) {
                                                            // kontrak masih berlaku
                                                            if ($rKontrak->habis >= date('Y-m-d')) {
                                                                $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                                                                $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                                                                $isShow = in_array($sebagai, array('Helper', 'Driver', 'Korlap'));
                                                            }
                                                        }

                                                        if ($isShow) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo strtoupper($record->id); ?></td>
                                                                <td><?php echo ucwords($record->nama); ?></td>
                                                                <td><?php echo $sebagai; ?></td>
                                                                <td><?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['h']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['h'] : 0); ?></td>
                                                                <td><?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['i']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['i'] : 0); ?></td>
                                                                <td><?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['a']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['a'] : 0); ?></td>
                                                                <td><?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['s']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['s'] : 0); ?></td>
                                                                <td><?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['l']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['l'] : 0); ?></td>
                                                                <td>
                                                                    <?php if ($extraURL !== '0') { ?>
                                                                        <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#pegawai-modal-<?php echo $record->kode; ?>">Detail</button>
                                                                        <!-- Modal -->
                                                                        <div class="modal fade" id="pegawai-modal-<?php echo $record->kode; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                            <div class="modal-dialog modal-lg">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                        <h4 class="modal-title" id="myModalLabel">Karir Pegawai</h4>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="row">
                                                                                            <div class="col-sm-6">
                                                                                                <h6>BIODATA</h6>
                                                                                                <br>
                                                                                                <div class="row">
                                                                                                    <div class="col-sm-6"><p>ID Pegawai: <b> <?php echo strtoupper($record->id); ?> </b></p></div>
                                                                                                    <div class="col-sm-6"><p>Jabatan: <b> <?php echo $sebagai; ?> </b></p></div>
                                                                                                    <div class="col-sm-12"><p>Nama Pegawai: <b> <?php echo ucwords($record->nama); ?> </b></p></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-6" style="border-left: 1px solid #e5e5e5;">
                                                                                                <?php
                                                                                                $wktMasuk = '-';
                                                                                                $stMasuk = 'alfa';
                                                                                                $tepatMasuk = '';
                                                                                                $wktPulang = '-';
                                                                                                $stPulang = '-';
                                                                                                $strLembur = array(TRUE => 'YA', FALSE => 'TIDAK');
                                                                                                $wrnStatus = array('hadir' => 'success', 'izin' => 'warning', 'sakit' => 'default', 'alfa' => 'danger', 'ok' => 'info', 'pulang cepat' => 'warning', '-' => 'default');
                                                                                                $isLembur = (isset($todayInfo[$record->kode]['lembur']) ? $todayInfo[$record->kode]['lembur'] : FALSE);

                                                                                                if (isset($todayInfo[$record->kode]['masuk'])) {
                                                                                                    $wktMasuk = $this->formatdate->getDateTime($todayInfo[$record->kode]['masuk']['waktu']);
                                                                                                    $stMasuk = $todayInfo[$record->kode]['masuk']['status'];
                                                                                                    $tepatMasuk = (strtotime($todayInfo[$record->kode]['masuk']['waktu']) < strtotime($todayInfo[$record->kode]['masuk']['harus'])) ? '' : 'terlambat';
                                                                                                }

                                                                                                if (isset($todayInfo[$record->kode]['pulang'])) {
                                                                                                    $wktPulang = $this->formatdate->getDateTime($todayInfo[$record->kode]['pulang']['waktu']);
                                                                                                    $stPulang = (strtotime($todayInfo[$record->kode]['pulang']['waktu']) < strtotime($todayInfo[$record->kode]['pulang']['harus'])) ? 'pulang cepat' : 'ok';
                                                                                                }
                                                                                                ?>
                                                                                                <h6>INFO KEHADIRAN HARI INI</h6>
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-hover table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>Info</th>
                                                                                                                <th>Waktu</th>
                                                                                                                <th>Status</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td><code>Kehadiran</code></td>
                                                                                                                <td><span class="label label-<?php echo $wrnStatus[$stMasuk]; ?>"><?php echo $wktMasuk; ?></span></td>
                                                                                                                <td><span class="label label-<?php echo $wrnStatus[$stMasuk]; ?>"><?php echo strtoupper($stMasuk . ' ' . $tepatMasuk); ?></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><code>Pulang</code></td>
                                                                                                                <td><span class="label label-<?php echo $wrnStatus[$stPulang]; ?>"><?php echo $wktPulang; ?></span></td>
                                                                                                                <td><span class="label label-<?php echo $wrnStatus[$stPulang]; ?>"><?php echo strtoupper($stPulang); ?></span></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="2"><code>Lembur</code></td>
                                                                                                                <td><span class="label label-<?php echo ($isLembur) ? 'info' : 'default'; ?>"><?php echo $strLembur[$isLembur]; ?></span></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <hr>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-6">
                                                                                                <h6>KEHADIRAN BULAN <?php echo ($extraURL !== '0') ? strtoupper($extraURL) . ' ' . date('Y') : '?'; ?></h6>
                                                                                                <br>
                                                                                                <div id="donut-graph-<?php echo $record->kode; ?>-1" class="chart no-padding"></div>
                                                                                            </div>
                                                                                            <div class="col-sm-6">
                                                                                                <h6>KESELURUHAN KEHADIRAN</h6>
                                                                                                <br>
                                                                                                <div id="donut-graph-<?php echo $record->kode; ?>-2" class="chart no-padding"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <hr>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-12">
                                                                                                <h6>SURAT PERINGATAN</h6>
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-hover table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>Proyek</th>
                                                                                                                <th>Tanggal</th>
                                                                                                                <th>No. SP</th>
                                                                                                                <th>SP Ke</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                            <?php
                                                                                                            $listSP = $this->model->getList(array('table' => 'data_proyek_spsl', 'where' => array('terpakai' => 1, 'jenis' => 'sp', 'biodata' => $record->kode), 'sort' => 'ke asc'));

                                                                                                            foreach ($listSP as $record) {
                                                                                                                $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek, 'terpakai' => 1)));

                                                                                                                if ($rProyek != NULL) {
                                                                                                                    ?>
                                                                                                                    <tr>
                                                                                                                        <td><?php echo ucwords($rProyek->proyek); ?></td>
                                                                                                                        <td><?php echo $this->formatdate->getDate($record->tanggal); ?></td>
                                                                                                                        <td><?php echo strtoupper($record->nomor); ?></td>
                                                                                                                        <td><?php echo 'SP-' . $record->ke; ?></td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                            }

                                                                                                            if (count($listSP) == 0) {
                                                                                                                ?>
                                                                                                                <tr>
                                                                                                                    <td colspan="4" class="text-center">- TIDAK ADA SP -</td>
                                                                                                                </tr>
                                                                                                            <?php } ?>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                                                    </div>
                                                                                </div><!-- /.modal-content -->
                                                                            </div><!-- /.modal-dialog -->
                                                                        </div><!-- /.modal -->
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var paramURL = '<?php echo $paramURL; ?>';

            $(document).ready(function () {
                var janJun;
                var julDes;

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#filter-button').click(function () {
                    // reload page
                    if ($('#proyek-filter').val() !== null) {
                        $(location).attr('href', "<?php echo site_url('modul/tampil/akun/laporanKarir'); ?>/" + $('#proyek-filter').val());
                    }
                });

                // Use Morris.Bar
                if ($('#presensi-bar-1').length) {
                    janJun = Morris.Bar({
                        element: 'presensi-bar-1',
                        data: [{
                                periode: 'Januari',
                                h: <?php echo $stats['01']['h']; ?>,
                                i: <?php echo $stats['01']['i']; ?>,
                                a: <?php echo $stats['01']['a']; ?>,
                                s: <?php echo $stats['01']['s']; ?>
                            }, {
                                periode: 'Februari',
                                h: <?php echo $stats['02']['h']; ?>,
                                i: <?php echo $stats['02']['i']; ?>,
                                a: <?php echo $stats['02']['a']; ?>,
                                s: <?php echo $stats['02']['s']; ?>
                            }, {
                                periode: 'Maret',
                                h: <?php echo $stats['03']['h']; ?>,
                                i: <?php echo $stats['03']['i']; ?>,
                                a: <?php echo $stats['03']['a']; ?>,
                                s: <?php echo $stats['03']['s']; ?>
                            }, {
                                periode: 'April',
                                h: <?php echo $stats['04']['h']; ?>,
                                i: <?php echo $stats['04']['i']; ?>,
                                a: <?php echo $stats['04']['a']; ?>,
                                s: <?php echo $stats['04']['s']; ?>
                            }, {
                                periode: 'Mei',
                                h: <?php echo $stats['05']['h']; ?>,
                                i: <?php echo $stats['05']['i']; ?>,
                                a: <?php echo $stats['05']['a']; ?>,
                                s: <?php echo $stats['05']['s']; ?>
                            }, {
                                periode: 'Juni',
                                h: <?php echo $stats['06']['h']; ?>,
                                i: <?php echo $stats['06']['i']; ?>,
                                a: <?php echo $stats['06']['a']; ?>,
                                s: <?php echo $stats['06']['s']; ?>
                            }],
                        xkey: 'periode',
                        ykeys: ['h', 'i', 'a', 's'],
                        labels: ['Hadir', 'Izin', 'Alfa', 'Sakit'],
                        barColors: ['#568a89', '#dfb56c', '#a65858', '#a8829f'],
                        resize: true
                    }).on('click', function (i, row) {
                        $(location).attr('href', "<?php echo site_url('modul/tampil/akun/laporanKarir/' . $paramURL . '/' . $periodURL); ?>/" + row.periode);
                    });
                }

                if ($('#presensi-bar-2').length) {
                    julDes = Morris.Bar({
                        element: 'presensi-bar-2',
                        data: [{
                                periode: 'Juli',
                                h: <?php echo $stats['07']['h']; ?>,
                                i: <?php echo $stats['07']['i']; ?>,
                                a: <?php echo $stats['07']['a']; ?>,
                                s: <?php echo $stats['07']['s']; ?>
                            }, {
                                periode: 'Agustus',
                                h: <?php echo $stats['08']['h']; ?>,
                                i: <?php echo $stats['08']['i']; ?>,
                                a: <?php echo $stats['08']['a']; ?>,
                                s: <?php echo $stats['08']['s']; ?>
                            }, {
                                periode: 'September',
                                h: <?php echo $stats['09']['h']; ?>,
                                i: <?php echo $stats['09']['i']; ?>,
                                a: <?php echo $stats['09']['a']; ?>,
                                s: <?php echo $stats['09']['s']; ?>
                            }, {
                                periode: 'Oktober',
                                h: <?php echo $stats['10']['h']; ?>,
                                i: <?php echo $stats['10']['i']; ?>,
                                a: <?php echo $stats['10']['a']; ?>,
                                s: <?php echo $stats['10']['s']; ?>
                            }, {
                                periode: 'November',
                                h: <?php echo $stats['11']['h']; ?>,
                                i: <?php echo $stats['11']['i']; ?>,
                                a: <?php echo $stats['11']['a']; ?>,
                                s: <?php echo $stats['11']['s']; ?>
                            }, {
                                periode: 'Desember',
                                h: <?php echo $stats['12']['h']; ?>,
                                i: <?php echo $stats['12']['i']; ?>,
                                a: <?php echo $stats['12']['a']; ?>,
                                s: <?php echo $stats['12']['s']; ?>
                            }],
                        xkey: 'periode',
                        ykeys: ['h', 'i', 'a', 's'],
                        labels: ['Hadir', 'Izin', 'Alfa', 'Sakit'],
                        barColors: ['#568a89', '#dfb56c', '#a65858', '#a8829f'],
                        resize: true
                    }).on('click', function (i, row) {
                        $(location).attr('href', "<?php echo site_url('modul/tampil/akun/laporanKarir/' . $paramURL . '/' . $periodURL); ?>/" + row.periode);
                    });
                }

                if (paramURL !== '0') {
                    var pilihProyek = $('#proyek-filter');
                    $.ajax({
                        url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + paramURL,
                        dataType: 'json', type: 'POST', cache: false
                    }).then(function (data) {
                        // create the option and append to Select2
                        pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText + ' | ' + data['data'].lokasiText + ' | ' + data['data'].kendaraanText, data['data'].kode, true, true)).trigger('change');
                        // manually trigger the `select2:select` event
                        pilihProyek.trigger({
                            type: 'select2:select',
                            params: {
                                data: data['data']
                            }
                        });
                    });
                }
            });
        </script>
        <?php
        foreach ($listPegawai as $record) {
            $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $paramURL, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
            $isShow = FALSE;
            $sebagai = '';

            if ($rKontrak != NULL) {
                // kontrak masih berlaku
                if ($rKontrak->habis >= date('Y-m-d')) {
                    $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                    $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                    $isShow = in_array($sebagai, array('Helper', 'Driver', 'Korlap'));
                }
            }

            if ($isShow) {
                ?>
                <script>
                    $(document).ready(function () {
                        $('#pegawai-modal-<?php echo $record->kode; ?>').on('shown.bs.modal', function () {
                            $(function () {
                                // donut
                                if ($('#donut-graph-<?php echo $record->kode; ?>-1').length) {
                                    $('#donut-graph-<?php echo $record->kode; ?>-1').empty();
                                    Morris.Donut({
                                        element: 'donut-graph-<?php echo $record->kode; ?>-1',
                                        data: [{
                                                value: <?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['h']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['h'] : 0); ?>,
                                                label: 'Hadir'
                                            }, {
                                                value: <?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['i']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['i'] : 0); ?>,
                                                label: 'Izin'
                                            }, {
                                                value: <?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['a']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['a'] : 0); ?>,
                                                label: 'Alfa'
                                            }, {
                                                value: <?php echo (isset($perPegawai[$record->kode][$months[strtolower($extraURL)]]['s']) ? $perPegawai[$record->kode][$months[strtolower($extraURL)]]['s'] : 0); ?>,
                                                label: 'Sakit'
                                            }],
                                        colors: ['#568a89', '#dfb56c', '#a65858', '#a8829f']
                                    });
                                }

                                if ($('#donut-graph-<?php echo $record->kode; ?>-2').length) {
                                    $('#donut-graph-<?php echo $record->kode; ?>-2').empty();
                                    Morris.Donut({
                                        element: 'donut-graph-<?php echo $record->kode; ?>-2',
                                        data: [{
                                                value: <?php echo (isset($thnPegawai[$record->kode]['h']) ? $thnPegawai[$record->kode]['h'] : 0); ?>,
                                                label: 'Hadir'
                                            }, {
                                                value: <?php echo (isset($thnPegawai[$record->kode]['i']) ? $thnPegawai[$record->kode]['i'] : 0); ?>,
                                                label: 'Izin'
                                            }, {
                                                value: <?php echo (isset($thnPegawai[$record->kode]['a']) ? $thnPegawai[$record->kode]['a'] : 0); ?>,
                                                label: 'Alfa'
                                            }, {
                                                value: <?php echo (isset($thnPegawai[$record->kode]['s']) ? $thnPegawai[$record->kode]['s'] : 0); ?>,
                                                label: 'Sakit'
                                            }],
                                        colors: ['#568a89', '#dfb56c', '#a65858', '#a8829f']
                                    });
                                }
                            });
                        });
                    });
                </script>
                <?php
            }
        }
        ?>
    </body>
</html>