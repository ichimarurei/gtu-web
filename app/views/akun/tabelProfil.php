<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Tabel</li><li>Pegawai <i class="param-span"></i></li>
                </ol>
                <!-- end breadcrumb -->
                <span class="ribbon-button-alignment pull-right">
                    <span id="excel-span" class="btn btn-ribbon hidden-xs" data-title="Export"><i class="fa fa-file-excel-o"></i> Export Data Pegawai</span>
                </span>
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Pegawai <i class="param-span"></i>
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Pegawai <i class="param-span"></i></h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>No Telepon</th>
                                                    <th>Proyek</th>
                                                    <th>Jabatan</th>
                                                    <th>Akun</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="bio">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="id-input" name="id-input" value="">
                                <input type="hidden" id="ktp-input" name="ktp-input" value="">
                                <input type="hidden" id="npwp-input" name="npwp-input" value="">
                                <input type="hidden" id="bpjs-input" name="bpjs-input" value="">
                                <input type="hidden" id="nama-input" name="nama-input" value="">
                                <input type="hidden" id="kelamin-input" name="kelamin-input" value="">
                                <input type="hidden" id="tempat_lahir-input" name="tempat_lahir-input" value="">
                                <input type="hidden" id="tanggal_lahir-input" name="tanggal_lahir-input" value="">
                                <input type="hidden" id="agama-input" name="agama-input" value="">
                                <input type="hidden" id="pendidikan-input" name="pendidikan-input" value="">
                                <input type="hidden" id="rekening-input" name="rekening-input" value="">
                                <input type="hidden" id="alamat-input" name="alamat-input" value="">
                                <input type="hidden" id="telepon-input" name="telepon-input" value="">
                                <input type="hidden" id="email-input" name="email-input" value="">
                                <input type="hidden" id="nikah-input" name="nikah-input" value="">
                                <input type="hidden" id="anak-input" name="anak-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var statParam = '<?php echo ($this->uri->segment(5) === FALSE) ? 'semua' : $this->uri->segment(5); ?>';
            var urlParam = 's_a___p_a';
            var title = '';

            $(document).ready(function () {
                if (statParam === 'aktif') {
                    urlParam = 's_1___p_a';
                    title = '(Aktif)';
                } else if (statParam === 'pasif') {
                    urlParam = 's_0___p_a';
                    title = '(Tidak Aktif)';
                }

                $('.param-span').text(title);
                initTable("<?php echo site_url('data/tabel/bio'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "id"},
                    {"data": "nama"},
                    {"data": "telepon"},
                    {"data": "proyek"},
                    {"data": "jabatan"},
                    {"data": "akun"},
                    {"data": "kontrak"},
                    {"data": "aksi", 'width': '30%'}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        urlParam = urlParam.substring(0, 8) + $('#proyek-filter').val();
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/bio'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                });

                $('#excel-span').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    $.ajax({
                        url: "<?php echo site_url('excel/biodata'); ?>", data: 'param=' + urlParam,
                        dataType: 'json', type: 'POST', cache: false,
                        success: function (response) {
                            $.unblockUI();
                            swal({
                                title: 'Hasil Export Pegawai',
                                html: '<p><a href="<?php echo base_url('etc/excel'); ?>/' + response.return.file + '" class="btn btn-success btn-flat btn-block unduhan-link">Unduh File (' + response.return.file + ')</a></p>',
                                type: 'success',
                                showConfirmButton: false
                            });
                        }
                    });
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/akun/dataProfil'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .userBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/akun/dataAkun'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .contractBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/proyek/tabelPegawai'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData);
                    });

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#id-input').val(response['data'].id);
                        $('#ktp-input').val(response['data'].ktp);
                        $('#npwp-input').val(response['data'].npwp);
                        $('#bpjs-input').val(response['data'].bpjs);
                        $('#nama-input').val(response['data'].nama);
                        $('#kelamin-input').val(response['data'].kelamin);
                        $('#tempat_lahir-input').val(response['data'].tempat_lahir);
                        $('#tanggal_lahir-input').val(response['data'].tanggal_lahir);
                        $('#agama-input').val(response['data'].agama);
                        $('#pendidikan-input').val(response['data'].pendidikan);
                        $('#rekening-input').val(response['data'].rekening);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#telepon-input').val(response['data'].telepon);
                        $('#email-input').val(response['data'].email);
                        $('#nikah-input').val(response['data'].nikah);
                        $('#anak-input').val(response['data'].anak);
                        doSave('Dihapus', "<?php echo site_url('modul/tampil/akun/tabelProfil'); ?>/" + statParam, $('#model-form'));
                    }
                });
            }
        </script>
    </body>
</html>