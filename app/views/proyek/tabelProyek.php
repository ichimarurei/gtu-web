<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Proyek</li><li>Tabel</li><li>Info Proyek</li>
                </ol>
                <!-- end breadcrumb -->
                <span class="ribbon-button-alignment pull-right">
                    <span id="excel-span" class="btn btn-ribbon hidden-xs" data-title="Export"><i class="fa fa-file-excel-o"></i> Export Semua Data</span>
                </span>
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Info Proyek
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Proyek</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Proyek</th>
                                                    <th>Jenis</th>
                                                    <th>Lokasi</th>
                                                    <th>Kendaraan</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="proyek-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Data Proyek</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <legend>Info Proyek</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Proyek</label>
                                                        <div class="col-md-10">
                                                            <p><b id="proyek-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Alamat</label>
                                                        <div class="col-md-10">
                                                            <p><b id="alamat-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jenis</label>
                                                        <div class="col-md-4">
                                                            <p><b id="tipe-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Kendaraan</label>
                                                        <div class="col-md-4">
                                                            <p><b id="kendaraan-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lokasi</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lokasi-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Invoice</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Muat Inap</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_inap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">SKR</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_skr-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">BPJS</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_bpjs-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Ritase 2</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_ritase-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPH23</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_pph-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPN</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_ppn-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Insentif Pengiriman</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_pengiriman-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Supervisi</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_supervisi-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">UMK</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_umk-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Seragam</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_seragam-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">THR</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_thr-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Lembur Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Gaji</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_driver-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_helper-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_korlap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Admin</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_admin-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Admin</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_admin-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Staf</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_staf-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Staf</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_staf-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Lembur Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Muat Inap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_inap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">SKR</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_skr-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Ritase 2</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_ritase-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Insentif Pengiriman</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_insentif-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Management Fee</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Fee Driver</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Fee Helper</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_helper-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Fee Korlap</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="lanjut-button">Ubah</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="proyek">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="alamat-input" name="alamat-input" value="">
                                <input type="hidden" id="tipe-input" name="tipe-input" value="">
                                <input type="hidden" id="lokasi-input" name="lokasi-input" value="">
                                <input type="hidden" id="kendaraan-input" name="kendaraan-input" value="">
                                <input type="hidden" id="invoice_inap-input" name="invoice_inap-input" value="">
                                <input type="hidden" id="invoice_skr-input" name="invoice_skr-input" value="">
                                <input type="hidden" id="invoice_bpjs-input" name="invoice_bpjs-input" value="">
                                <input type="hidden" id="invoice_pengiriman-input" name="invoice_pengiriman-input" value="">
                                <input type="hidden" id="invoice_ritase-input" name="invoice_ritase-input" value="">
                                <input type="hidden" id="invoice_pph-input" name="invoice_pph-input" value="">
                                <input type="hidden" id="invoice_ppn-input" name="invoice_ppn-input" value="">
                                <input type="hidden" id="invoice_umk-input" name="invoice_umk-input" value="" class="nominal-text">
                                <input type="hidden" id="invoice_thr-input" name="invoice_thr-input" value="" class="nominal-text">
                                <input type="hidden" id="invoice_supervisi-input" name="invoice_supervisi-input" value="" class="nominal-text">
                                <input type="hidden" id="invoice_seragam-input" name="invoice_seragam-input" value="" class="nominal-text">
                                <input type="hidden" id="invoice_fee_korlap-input" name="invoice_fee_korlap-input" value="">
                                <input type="hidden" id="invoice_fee_driver-input" name="invoice_fee_driver-input" value="">
                                <input type="hidden" id="invoice_fee_helper-input" name="invoice_fee_helper-input" value="">
                                <input type="hidden" id="gaji_korlap-input" name="gaji_korlap-input" value="" class="nominal-text">
                                <input type="hidden" id="makan_korlap-input" name="makan_korlap-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_driver-input" name="gaji_driver-input" value="" class="nominal-text">
                                <input type="hidden" id="makan_driver-input" name="makan_driver-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_helper-input" name="gaji_helper-input" value="" class="nominal-text">
                                <input type="hidden" id="makan_helper-input" name="makan_helper-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_admin-input" name="gaji_admin-input" value="" class="nominal-text">
                                <input type="hidden" id="makan_admin-input" name="makan_admin-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_staf-input" name="gaji_staf-input" value="" class="nominal-text">
                                <input type="hidden" id="makan_staf-input" name="makan_staf-input" value="" class="nominal-text">
                                <input type="hidden" id="lembur_driver-input" name="lembur_driver-input" value="" class="nominal-text">
                                <input type="hidden" id="lembur_helper-input" name="lembur_helper-input" value="" class="nominal-text">
                                <input type="hidden" id="lembur_korlap-input" name="lembur_korlap-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_lembur_driver-input" name="gaji_lembur_driver-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_lembur_helper-input" name="gaji_lembur_helper-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_lembur_korlap-input" name="gaji_lembur_korlap-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_inap-input" name="gaji_inap-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_skr-input" name="gaji_skr-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_ritase-input" name="gaji_ritase-input" value="" class="nominal-text">
                                <input type="hidden" id="gaji_insentif-input" name="gaji_insentif-input" value="" class="nominal-text">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var nextURL = null;

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/proyek'); ?>", '#dt_basic', [
                    {"data": "proyek"},
                    {"data": "tipe"},
                    {"data": "lokasi"},
                    {"data": "kendaraan"},
                    {"data": "aksi", 'width': '23%'}
                ]);

                $('#excel-span').click(function () {
                    excel("<?php echo site_url('excel/semuaProyek'); ?>", 1);
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
                $('#lanjut-button').click(function () {
                    $(location).attr('href', nextURL);
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    nextURL = "<?php echo site_url('modul/tampil/proyek/dataProyek'); ?>/" + $(this).attr('href');
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    fillInputs($(this).attr('href'), false);

                    return false;
                });
                $(table + " .exportBtn").on("click", function () {
                    excel("<?php echo site_url('excel/proyek'); ?>", $(this).attr('href'));

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData, true);
                    });

                    return false;
                });
            }

            function fillInputs(param, isRemove) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        if (isRemove) {
                            $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                            $('#key-input').val(response['data'].key);
                            $('#kode-input').val(response['data'].kode);
                            $('#proyek-input').val(response['data'].proyek);
                            $('#alamat-input').val(response['data'].alamat);
                            $('#tipe-input').val(response['data'].tipe);
                            $('#lokasi-input').val(response['data'].lokasi);
                            $('#kendaraan-input').val(response['data'].kendaraan);
                            $('#invoice_inap-input').val(response['data'].invoice_inap);
                            $('#invoice_skr-input').val(response['data'].invoice_skr);
                            $('#invoice_bpjs-input').val(response['data'].invoice_bpjs);
                            $('#invoice_pengiriman-input').val(response['data'].invoice_pengiriman);
                            $('#invoice_ritase-input').val(response['data'].invoice_ritase);
                            $('#invoice_pph-input').val(response['data'].invoice_pph);
                            $('#invoice_ppn-input').val(response['data'].invoice_ppn);
                            $('#invoice_umk-input').val(response['data'].invoice_umk);
                            $('#invoice_thr-input').val(response['data'].invoice_thr);
                            $('#invoice_supervisi-input').val(response['data'].invoice_supervisi);
                            $('#invoice_seragam-input').val(response['data'].invoice_seragam);
                            $('#invoice_fee_korlap-input').val(response['data'].invoice_fee_korlap);
                            $('#invoice_fee_driver-input').val(response['data'].invoice_fee_driver);
                            $('#invoice_fee_helper-input').val(response['data'].invoice_fee_helper);
                            $('#gaji_korlap-input').val(response['data'].gaji_korlap);
                            $('#gaji_driver-input').val(response['data'].gaji_driver);
                            $('#gaji_helper-input').val(response['data'].gaji_helper);
                            $('#gaji_admin-input').val(response['data'].gaji_admin);
                            $('#gaji_staf-input').val(response['data'].gaji_staf);
                            $('#makan_korlap-input').val(response['data'].makan_korlap);
                            $('#makan_driver-input').val(response['data'].makan_driver);
                            $('#makan_helper-input').val(response['data'].makan_helper);
                            $('#makan_admin-input').val(response['data'].makan_admin);
                            $('#makan_staf-input').val(response['data'].makan_staf);
                            $('#lembur_driver-input').val(response['data'].lembur_driver);
                            $('#lembur_helper-input').val(response['data'].lembur_helper);
                            $('#lembur_korlap-input').val(response['data'].lembur_korlap);
                            $('#gaji_lembur_driver-input').val(response['data'].gaji_lembur_driver);
                            $('#gaji_lembur_helper-input').val(response['data'].gaji_lembur_helper);
                            $('#gaji_lembur_korlap-input').val(response['data'].gaji_lembur_korlap);
                            $('#gaji_inap-input').val(response['data'].gaji_inap);
                            $('#gaji_skr-input').val(response['data'].gaji_skr);
                            $('#gaji_ritase-input').val(response['data'].gaji_ritase);
                            $('#gaji_insentif-input').val(response['data'].gaji_insentif);
                            nominalFormats();
                            doSave('Dihapus', "<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>", $('#model-form'));
                        } else {
                            $('#tipe-text').text(response['data'].tipeText);
                            $('#lokasi-text').text(response['data'].lokasiText);
                            $('#kendaraan-text').text(response['data'].kendaraanText);
                            $('#proyek-text').text(response['data'].proyek);
                            $('#alamat-text').text(response['data'].alamat);
                            $('#invoice_fee_korlap-text').text(response['data'].invoice_fee_korlap + ' %');
                            $('#invoice_fee_driver-text').text(response['data'].invoice_fee_driver + ' %');
                            $('#invoice_fee_helper-text').text(response['data'].invoice_fee_helper + ' %');
                            $('#invoice_inap-text').text(response['data'].invoice_inap + ' %');
                            $('#invoice_skr-text').text(response['data'].invoice_skr + ' %');
                            $('#invoice_bpjs-text').text(response['data'].invoice_bpjs + ' %');
                            $('#invoice_pengiriman-text').text(response['data'].invoice_pengiriman + ' %');
                            $('#invoice_ritase-text').text(response['data'].invoice_ritase + ' %');
                            $('#invoice_pph-text').text(response['data'].invoice_pph + ' %');
                            $('#invoice_ppn-text').text(response['data'].invoice_ppn + ' %');
                            $('#invoice_umk-text').text(response['data'].invoice_umk);
                            $('#invoice_thr-text').text(response['data'].invoice_thr);
                            $('#invoice_supervisi-text').text(response['data'].invoice_supervisi);
                            $('#invoice_seragam-text').text(response['data'].invoice_seragam);
                            $('#lembur_driver-text').text(response['data'].lembur_driver);
                            $('#lembur_helper-text').text(response['data'].lembur_helper);
                            $('#lembur_korlap-text').text(response['data'].lembur_korlap);
                            $('#gaji_korlap-text').text(response['data'].gaji_korlap);
                            $('#gaji_driver-text').text(response['data'].gaji_driver);
                            $('#gaji_helper-text').text(response['data'].gaji_helper);
                            $('#gaji_admin-text').text(response['data'].gaji_admin);
                            $('#gaji_staf-text').text(response['data'].gaji_staf);
                            $('#makan_korlap-text').text(response['data'].makan_korlap);
                            $('#makan_driver-text').text(response['data'].makan_driver);
                            $('#makan_helper-text').text(response['data'].makan_helper);
                            $('#makan_admin-text').text(response['data'].makan_admin);
                            $('#makan_staf-text').text(response['data'].makan_staf);
                            $('#gaji_lembur_driver-text').text(response['data'].gaji_lembur_driver);
                            $('#gaji_lembur_helper-text').text(response['data'].gaji_lembur_helper);
                            $('#gaji_lembur_korlap-text').text(response['data'].gaji_lembur_korlap);
                            $('#gaji_inap-text').text(response['data'].gaji_inap);
                            $('#gaji_skr-text').text(response['data'].gaji_skr);
                            $('#gaji_ritase-text').text(response['data'].gaji_ritase);
                            $('#gaji_insentif-text').text(response['data'].gaji_insentif);
                            $.unblockUI();
                            $('#proyek-modal').modal();
                        }
                    }
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }

            function excel(url, param) {
                $.blockUI({message: '<h1>Memproses...</h1>'});
                $.ajax({
                    url: url, data: 'param=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $.unblockUI();
                        swal({
                            title: 'Hasil Export Proyek',
                            html: '<p><a href="<?php echo base_url('etc/excel'); ?>/' + response.return.file + '" class="btn btn-success btn-flat btn-block unduhan-link">Unduh File (' + response.return.file + ')</a></p>',
                            type: 'success',
                            showConfirmButton: false
                        });
                    }
                });
            }
        </script>
    </body>
</html>