<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Proyek</li><li>Pendataan</li><li>Info Proyek</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Info Proyek
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Proyek</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="proyek">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <ul class="nav nav-tabs bordered">
                                                <li class="active"><a href="#fieldset-info" data-toggle="tab">Proyek</a></li>
                                                <li><a href="#fieldset-invoice" data-toggle="tab">Invoice</a></li>
                                                <li><a href="#fieldset-gaji" data-toggle="tab">Penggajian</a></li>
                                                <li><a href="#fieldset-fee" data-toggle="tab">Management Fee</a></li>
                                            </ul>
                                            <div class="tab-content padding-10">
                                                <div class="tab-pane fade in active" id="fieldset-info">
                                                    <fieldset>
                                                        <legend>Info Proyek</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="Nama Proyek" type="text" id="proyek-input" name="proyek-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Alamat</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Alamat Proyek" rows="4" id="alamat-input" name="alamat-input"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Jenis</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="tipe-input" name="tipe-input"></select>
                                                            </div>
                                                            <label class="col-md-2 control-label">Kendaraan</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="kendaraan-input" name="kendaraan-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Lokasi</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="lokasi-input" name="lokasi-input"></select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="tab-pane fade" id="fieldset-invoice">
                                                    <fieldset>
                                                        <legend>Pendataan Invoice</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Muat Inap</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Muat Inap (%)" type="text" id="invoice_inap-input" name="invoice_inap-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">SKR</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="SKR (%)" type="text" id="invoice_skr-input" name="invoice_skr-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">BPJS</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="BPJS (%)" type="text" id="invoice_bpjs-input" name="invoice_bpjs-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Ritase 2</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Ritase 2 (%)" type="text" id="invoice_ritase-input" name="invoice_ritase-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">PPH23</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="PPH23 (%)" type="text" id="invoice_pph-input" name="invoice_pph-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">PPN</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="PPN (%)" type="text" id="invoice_ppn-input" name="invoice_ppn-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Insentif Pengiriman</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Insentif (%)" type="text" id="invoice_pengiriman-input" name="invoice_pengiriman-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Supervisi</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Supervisi  (Rp)" type="text" id="invoice_supervisi-input" name="invoice_supervisi-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">UMK</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="UMK  (Rp)" type="text" id="invoice_umk-input" name="invoice_umk-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Seragam</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Seragam  (Rp)" type="text" id="invoice_seragam-input" name="invoice_seragam-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">THR</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="THR  (Rp)" type="text" id="invoice_thr-input" name="invoice_thr-input">
                                                            </div>
                                                        </div>
                                                        <legend>Pendataan Lembur</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uang Lembur Driver</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Driver (Rp)" type="text" id="lembur_driver-input" name="lembur_driver-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Lembur Helper</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Helper (Rp)" type="text" id="lembur_helper-input" name="lembur_helper-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uang Lembur Korlap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Korlap (Rp)" type="text" id="lembur_korlap-input" name="lembur_korlap-input">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="tab-pane fade" id="fieldset-gaji">
                                                    <fieldset>
                                                        <legend>Pendataan Gaji</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Gaji Driver</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Gaji Driver (Rp)" type="text" id="gaji_driver-input" name="gaji_driver-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Makan Driver</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Makan Driver (Rp)" type="text" id="makan_driver-input" name="makan_driver-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Gaji Helper</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Gaji Helper (Rp)" type="text" id="gaji_helper-input" name="gaji_helper-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Makan Helper</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Makan Helper (Rp)" type="text" id="makan_helper-input" name="makan_helper-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Gaji Korlap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Gaji Korlap (Rp)" type="text" id="gaji_korlap-input" name="gaji_korlap-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Makan Korlap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Makan Korlap (Rp)" type="text" id="makan_korlap-input" name="makan_korlap-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Gaji Admin</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Gaji Admin (Rp)" type="text" id="gaji_admin-input" name="gaji_admin-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Makan Admin</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Makan Admin (Rp)" type="text" id="makan_admin-input" name="makan_admin-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Gaji Staf</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Gaji Staf (Rp)" type="text" id="gaji_staf-input" name="gaji_staf-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Makan Staf</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Makan Staf (Rp)" type="text" id="makan_staf-input" name="makan_staf-input">
                                                            </div>
                                                        </div>
                                                        <legend>Pendataan Lembur</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uang Lembur Driver</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Driver (Rp)" type="text" id="gaji_lembur_driver-input" name="gaji_lembur_driver-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Uang Lembur Helper</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Helper (Rp)" type="text" id="gaji_lembur_helper-input" name="gaji_lembur_helper-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uang Lembur Korlap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Uang Lembur Korlap (Rp)" type="text" id="gaji_lembur_korlap-input" name="gaji_lembur_korlap-input">
                                                            </div>
                                                        </div>
                                                        <legend>Pendataan Shipment</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Muat Inap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Muat Inap (Rp)" type="text" id="gaji_inap-input" name="gaji_inap-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">SKR</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="SKR (Rp)" type="text" id="gaji_skr-input" name="gaji_skr-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Ritase 2</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Ritase 2 (Rp)" type="text" id="gaji_ritase-input" name="gaji_ritase-input">
                                                            </div>
                                                            <label class="col-md-2 control-label">Insentif Pengiriman</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control nominal-text" placeholder="Insentif (Rp)" type="text" id="gaji_insentif-input" name="gaji_insentif-input">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="tab-pane fade" id="fieldset-fee">
                                                    <fieldset>
                                                        <legend>Pendataan Management Fee</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Fee Driver</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Fee Driver (%)" type="text" id="invoice_fee_driver-input" name="invoice_fee_driver-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">Fee Helper</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Fee Helper (%)" type="text" id="invoice_fee_helper-input" name="invoice_fee_helper-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">Fee Korlap</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control" placeholder="Fee Korlap (%)" type="text" id="invoice_fee_korlap-input" name="invoice_fee_korlap-input">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="lihat-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="proyek-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Data Proyek</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <legend>Info Proyek</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Proyek</label>
                                                        <div class="col-md-10">
                                                            <p><b id="proyek-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Alamat</label>
                                                        <div class="col-md-10">
                                                            <p><b id="alamat-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jenis</label>
                                                        <div class="col-md-4">
                                                            <p><b id="tipe-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Kendaraan</label>
                                                        <div class="col-md-4">
                                                            <p><b id="kendaraan-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lokasi</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lokasi-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Invoice</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Muat Inap</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_inap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">SKR</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_skr-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">BPJS</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_bpjs-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Ritase 2</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_ritase-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPH23</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_pph-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPN</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_ppn-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Insentif Pengiriman</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_pengiriman-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Supervisi</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_supervisi-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">UMK</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_umk-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Seragam</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_seragam-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">THR</label>
                                                        <div class="col-md-4">
                                                            <p><b id="invoice_thr-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Lembur Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="lembur_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Gaji</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_driver-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_helper-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_korlap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Admin</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_admin-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Admin</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_admin-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Gaji Staf</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_staf-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Uang Makan Staf</label>
                                                        <div class="col-md-4">
                                                            <p><b id="makan_staf-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Driver</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Lembur Helper</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_helper-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Lembur Korlap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_lembur_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Muat Inap</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_inap-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">SKR</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_skr-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Ritase 2</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_ritase-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Insentif Pengiriman</label>
                                                        <div class="col-md-4">
                                                            <p><b id="gaji_insentif-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <legend>Pendataan Management Fee</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Fee Driver</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_driver-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Fee Helper</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_helper-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">Fee Korlap</label>
                                                        <div class="col-md-2">
                                                            <p><b id="invoice_fee_korlap-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="simpan-button">Lanjutkan Proses</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#tipe-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Jenis Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/tipe'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#lokasi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Lokasi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/lokasi'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#kendaraan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Jenis Kendaraan'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/kendaraan'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $(".nominal-text").keyup(function () {
                    if ($(this).attr('id') === 'invoice_umk-input') {
                        var intNominal = $(this).val().replace('Rp. ', '');
                        intNominal = intNominal.replace(/\./g, '');
                        var thr = Math.round(intNominal / 12);
                        $('#invoice_thr-input').val(formatRp(thr.toString()));
                    }

                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#lihat-button').click(function () {
                    var tipeOpt = $('#tipe-input').select2('data');
                    var lokasiOpt = $('#lokasi-input').select2('data');
                    var kendaraanOpt = $('#kendaraan-input').select2('data');

                    if ((typeof tipeOpt[0] !== 'undefined') || typeof lokasiOpt[0] !== 'undefined' || typeof kendaraanOpt[0] !== 'undefined') {
                        $('#tipe-text').text(tipeOpt[0].text);
                        $('#lokasi-text').text(lokasiOpt[0].text);
                        $('#kendaraan-text').text(kendaraanOpt[0].text);
                    }

                    $('#proyek-text').text($('#proyek-input').val());
                    $('#alamat-text').text($('#alamat-input').val());
                    $('#invoice_fee_korlap-text').text($('#invoice_fee_korlap-input').val() + ' %');
                    $('#invoice_fee_driver-text').text($('#invoice_fee_driver-input').val() + ' %');
                    $('#invoice_fee_helper-text').text($('#invoice_fee_helper-input').val() + ' %');
                    $('#invoice_inap-text').text($('#invoice_inap-input').val() + ' %');
                    $('#invoice_skr-text').text($('#invoice_skr-input').val() + ' %');
                    $('#invoice_bpjs-text').text($('#invoice_bpjs-input').val() + ' %');
                    $('#invoice_pengiriman-text').text($('#invoice_pengiriman-input').val() + ' %');
                    $('#invoice_ritase-text').text($('#invoice_ritase-input').val() + ' %');
                    $('#invoice_pph-text').text($('#invoice_pph-input').val() + ' %');
                    $('#invoice_ppn-text').text($('#invoice_ppn-input').val() + ' %');
                    $('#invoice_umk-text').text($('#invoice_umk-input').val());
                    $('#invoice_thr-text').text($('#invoice_thr-input').val());
                    $('#invoice_supervisi-text').text($('#invoice_supervisi-input').val());
                    $('#invoice_seragam-text').text($('#invoice_seragam-input').val());
                    $('#lembur_driver-text').text($('#lembur_driver-input').val());
                    $('#lembur_helper-text').text($('#lembur_helper-input').val());
                    $('#lembur_korlap-text').text($('#lembur_korlap-input').val());
                    $('#gaji_korlap-text').text($('#gaji_korlap-input').val());
                    $('#gaji_driver-text').text($('#gaji_driver-input').val());
                    $('#gaji_helper-text').text($('#gaji_helper-input').val());
                    $('#gaji_admin-text').text($('#gaji_admin-input').val());
                    $('#gaji_staf-text').text($('#gaji_staf-input').val());
                    $('#makan_korlap-text').text($('#makan_korlap-input').val());
                    $('#makan_driver-text').text($('#makan_driver-input').val());
                    $('#makan_helper-text').text($('#makan_helper-input').val());
                    $('#makan_admin-text').text($('#makan_admin-input').val());
                    $('#makan_staf-text').text($('#makan_staf-input').val());
                    $('#gaji_lembur_driver-text').text($('#gaji_lembur_driver-input').val());
                    $('#gaji_lembur_helper-text').text($('#gaji_lembur_helper-input').val());
                    $('#gaji_lembur_korlap-text').text($('#gaji_lembur_korlap-input').val());
                    $('#gaji_inap-text').text($('#gaji_inap-input').val());
                    $('#gaji_skr-text').text($('#gaji_skr-input').val());
                    $('#gaji_ritase-text').text($('#gaji_ritase-input').val());
                    $('#gaji_insentif-text').text($('#gaji_insentif-input').val());
                    $('#proyek-modal').modal();
                });
                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    nominalFormats();
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#invoice_inap-input').val(response['data'].invoice_inap);
                        $('#invoice_skr-input').val(response['data'].invoice_skr);
                        $('#invoice_bpjs-input').val(response['data'].invoice_bpjs);
                        $('#invoice_pengiriman-input').val(response['data'].invoice_pengiriman);
                        $('#invoice_ritase-input').val(response['data'].invoice_ritase);
                        $('#invoice_pph-input').val(response['data'].invoice_pph);
                        $('#invoice_ppn-input').val(response['data'].invoice_ppn);
                        $('#invoice_umk-input').val(response['data'].invoice_umk);
                        $('#invoice_thr-input').val(response['data'].invoice_thr);
                        $('#invoice_supervisi-input').val(response['data'].invoice_supervisi);
                        $('#invoice_seragam-input').val(response['data'].invoice_seragam);
                        $('#invoice_fee_korlap-input').val(response['data'].invoice_fee_korlap);
                        $('#invoice_fee_driver-input').val(response['data'].invoice_fee_driver);
                        $('#invoice_fee_helper-input').val(response['data'].invoice_fee_helper);
                        $('#gaji_korlap-input').val(response['data'].gaji_korlap);
                        $('#gaji_driver-input').val(response['data'].gaji_driver);
                        $('#gaji_helper-input').val(response['data'].gaji_helper);
                        $('#gaji_admin-input').val(response['data'].gaji_admin);
                        $('#gaji_staf-input').val(response['data'].gaji_staf);
                        $('#makan_korlap-input').val(response['data'].makan_korlap);
                        $('#makan_driver-input').val(response['data'].makan_driver);
                        $('#makan_helper-input').val(response['data'].makan_helper);
                        $('#makan_admin-input').val(response['data'].makan_admin);
                        $('#makan_staf-input').val(response['data'].makan_staf);
                        $('#lembur_driver-input').val(response['data'].lembur_driver);
                        $('#lembur_helper-input').val(response['data'].lembur_helper);
                        $('#lembur_korlap-input').val(response['data'].lembur_korlap);
                        $('#gaji_lembur_driver-input').val(response['data'].gaji_lembur_driver);
                        $('#gaji_lembur_helper-input').val(response['data'].gaji_lembur_helper);
                        $('#gaji_lembur_korlap-input').val(response['data'].gaji_lembur_korlap);
                        $('#gaji_inap-input').val(response['data'].gaji_inap);
                        $('#gaji_skr-input').val(response['data'].gaji_skr);
                        $('#gaji_ritase-input').val(response['data'].gaji_ritase);
                        $('#gaji_insentif-input').val(response['data'].gaji_insentif);
                        $('#proyek-input').focus();

                        if (response['data'].key > 0) {
                            var pilihTipe = $('#tipe-input');
                            var pilihLokasi = $('#lokasi-input');
                            var pilihKendaraan = $('#kendaraan-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=tipe&kode=' + response['data'].tipe,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihTipe.append(new Option(data['data'].tipe, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihTipe.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=lokasi&kode=' + response['data'].lokasi,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihLokasi.append(new Option(data['data'].lokasi, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihLokasi.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=kendaraan&kode=' + response['data'].kendaraan,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKendaraan.append(new Option(data['data'].kendaraan, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKendaraan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                        }
                    }
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }
        </script>
    </body>
</html>