<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Karir Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Karir Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Karir Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="kontrak">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                            <input type="hidden" id="terdata-input" name="terdata-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ID Pegawai</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="ID Pegawai" type="text" id="id-text" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Nama Pegawai</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Nama Lengkap Pegawai" type="text" id="nama-text" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-5">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                    <label class="col-md-1 control-label">Jabatan</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="jabatan-input" name="jabatan-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Status</label>
                                                    <div class="col-md-3">
                                                        <select style="width:100%" class="select2 form-control" id="status-input" name="status-input">
                                                            <option value="PKWT">PKWT Ke-</option>
                                                            <option value="Resign">Resign</option>
                                                            <option value="Dikeluarkan">Dikeluarkan</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input class="form-control" placeholder="Ke-" type="text" id="ke-input" name="ke-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Waktu</label>
                                                    <div class="col-md-3">
                                                        <input class="form-control" placeholder="Tanggal Berlaku" type="text" id="berlaku-input" name="berlaku-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input class="form-control" placeholder="Tanggal Berakhir" type="text" id="habis-input" name="habis-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Jam</label>
                                                    <div class="col-md-3">
                                                        <input class="form-control" placeholder="Jam Masuk" type="text" id="masuk-input" name="masuk-input" data-provide="timepicker" data-show-meridian="false">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input class="form-control" placeholder="Jam Pulang" type="text" id="selesai-input" name="selesai-input" data-provide="timepicker" data-show-meridian="false">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Gaji Pegawai</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control nominal-text" placeholder="Gaji Pegawai (Rp)" type="text" id="gaji-input" name="gaji-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">Management Fee</label>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input class="form-control" placeholder="Management Fee (%)" type="text" id="fee-input" name="fee-input">
                                                            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/proyek/tabelPegawai/' . $this->uri->segment(5)); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var urlParam = '<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>';
            var urutKe = 1;
            var tglAkhir = '';
            var jamAwal = '';
            var jamAkhir = '';

            $(document).ready(function () {
                if (urlParam === '0') {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>");
                } else {
                    fillInputs('<?php echo ($this->uri->segment(6) === FALSE) ? 0 : $this->uri->segment(6); ?>');
                }

                $('#berlaku-input').datepicker().on('changeDate', function (e) {
                    $('#habis-input').datepicker('setDate', e.date);
                    $('#habis-input').datepicker('setStartDate', e.date);
                });
                $('#status-input').change(function () {
                    if ($(this).val() === 'PKWT') {
                        $('#ke-input').val(urutKe);
                        $('#selesai-input').val(jamAkhir);
                        $('#ke-input').show();
                        $('#habis-input').show();
                        $('#selesai-input').show();
                        $('#masuk-input').attr('placeholder', 'Jam Masuk');
                    } else {
                        $('#ke-input').val(0);
                        $('#selesai-input').val('0:00');
                        $('#ke-input').hide();
                        $('#habis-input').hide();
                        $('#selesai-input').hide();
                        $('#masuk-input').attr('placeholder', 'Jam Berlaku');
                    }
                });

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#jabatan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Jabatan'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/jabatan'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    nominalFormats();
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/proyek/tabelPegawai/' . $this->uri->segment(5)); ?>", $('#model-form'));
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kontrak&kode=' + urlParam + '___' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#biodata-input').val(urlParam);
                        $('#status-input').val(response['data'].status).trigger('change');
                        $('#ke-input').val(response['data'].ke);
                        $('#berlaku-input').val(response['data'].berlaku);
                        $('#habis-input').val(response['data'].habis);
                        $('#masuk-input').val(response['data'].masuk);
                        $('#selesai-input').val(response['data'].selesai);
                        $('#terdata-input').val(response['data'].terdata);
                        $('#id-text').val(response['data'].id);
                        $('#nama-text').val(response['data'].nama);
                        $('#gaji-input').val(response['data'].gaji);
                        $('#fee-input').val(response['data'].fee);
                        urutKe = response['data'].ke;
                        tglAkhir = response['data'].habis;
                        jamAwal = response['data'].masuk;
                        jamAkhir = response['data'].selesai;

                        if (response['data'].key > 0) {
                            $('#biodata-input').val(response['data'].biodata);
                            var pilihProyek = $('#proyek-input');
                            var pilihJabatan = $('#jabatan-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText + ' | ' + data['data'].lokasiText + ' | ' + data['data'].kendaraanText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=jabatan&kode=' + response['data'].jabatan,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihJabatan.append(new Option(data['data'].jabatan, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihJabatan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                        }
                    }
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }
        </script>
    </body>
</html>