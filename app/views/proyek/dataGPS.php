<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />
        <style>
            #map { width:100%; height:300px; }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Proyek</li><li>Lihat</li><li>Live GPS</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-map-marker"></i>
                            Lihat
                            <span>>
                                Live GPS
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-map-pin"></i> </span>
                                    <h2>Live GPS</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pegawai</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="pegawai-filter"></select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/proyek/dataGPS'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="lihat-button">
                                                            <i class="fa fa-globe"></i>
                                                            Lihat
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <div id='map'></div>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoiaWNoaW1hcnVyZWkiLCJhIjoiY2ozZmxxaTRnMDEzMDJ4bW8yb3MzZDhuayJ9.K1vvEnEMzVsvwjkdEXP7hA';
            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                zoom: 15,
                center: [106.845599, -6.2087634]
            });
            var marker = new mapboxgl.Marker();

            $(document).ready(function () {
                map.addControl(new mapboxgl.GeolocateControl({
                    positionOptions: {
                        enableHighAccuracy: true
                    },
                    trackUserLocation: true
                }));

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(meOn);
                }

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#pegawai-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#proyek-filter').change(function () {
                    var kode = $(this).val();
                    $('#pegawai-filter').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Pegawai'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#lihat-button').click(function () {
                    if ($('#proyek-filter').val() !== null && $('#pegawai-filter').val() !== null) {
                        doView($('#proyek-filter').val(), $('#pegawai-filter').val());
                    } else {
                        swal({
                            title: 'Peringatan',
                            html: 'Proyek dan Pegawai harus dipilih!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
            });

            function doView(proyek, biodata) {
                setInterval(function () {
                    $.ajax({
                        url: "<?php echo site_url('restapi/lokasi'); ?>", data: 'proyek-kode=' + proyek + '&biodata-kode=' + biodata,
                        dataType: 'json', type: 'POST', cache: false,
                        success: function (response) {
                            if (response.data.waktu !== '') {
                                initMap(response.data.long, response.data.lat);
                            }
                        }
                    });
                }, 15000);
            }

            function meOn(gps) {
                var lat = gps.coords.latitude;
                var lon = gps.coords.longitude;
                initMap(lon, lat);
            }

            function initMap(lon, lat) {
                var longlat = new mapboxgl.LngLat(lon, lat);
                marker.setLngLat(longlat).addTo(map);
                map.flyTo({center: [lon, lat], zoom: 15});
            }
        </script>
    </body>
</html>