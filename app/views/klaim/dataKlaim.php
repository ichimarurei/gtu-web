<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Keuangan</li><li>Pendataan</li><li>Klaim/Reimburse</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Klaim/Reimburse
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Klaim/Reimburse</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="klaim">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" name="pemohon-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pegawai</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="pegawai-input" name="pegawai-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tanggal</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tanggal Permohonan" type="text" id="tanggal-input" name="tanggal-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Nominal</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control nominal-text" placeholder="Nominal Permohonan (Rp)" type="text" id="nominal-input" name="nominal-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Perihal</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control text-tetap" placeholder="Perihal Permohonan" rows="4" id="perihal-input" name="perihal-input"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Lampiran</label>
                                                    <div class="col-md-10">
                                                        <button class="btn btn-default" id="unggah-button" type="button">Unggah</button>
                                                        <a href="" class="btn btn-link" id="unduh-link">Unduh</a>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="status-div">
                                                    <label class="col-md-2 control-label">Status</label>
                                                    <div class="col-md-3">
                                                        <select style="width:100%" class="select2 form-control" id="status-input" name="status-input">
                                                            <option value="ajuan">PENGAJUAN</option>
                                                            <option value="terima">SETUJU</option>
                                                            <option value="tolak">TOLAK</option>
                                                            <option value="terbayar">TUTUP/SELESAI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/klaim/tabelKlaim'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="unggah-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Unggah Berkas</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="*">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var akses = '<?php echo $this->session->userdata('_otoritas'); ?>';

            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');
                initUploader();
                $('#status-div').hide();

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#pegawai-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    $('#pegawai-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Pegawai'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    nominalFormats();
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/klaim/tabelKlaim'); ?>", $('#model-form'));
                });
                $('#unggah-button').click(function () {
                    $('#unggah-modal').modal();
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=klaim&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#tanggal-input').val(response['data'].tanggal);
                        $('#perihal-input').val(response['data'].perihal);
                        $('#nominal-input').val(response['data'].nominal);
                        $('#status-input').val(response['data'].status).trigger('change');
                        $('#unduh-link').hide();

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            var pilihBio = $('#pegawai-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText + ' | ' + data['data'].lokasiText + ' | ' + data['data'].kendaraanText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + response['data'].pegawai,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihBio.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihBio.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.when(lihatGaleri('etc/klaim/' + response['data'].kode)).done(function (attachments) {
                                var attachment = '';

                                $.each(attachments.galeri, function (index, value) {
                                    attachment = value;
                                });

                                if (attachment !== '') {
                                    $('#unduh-link').attr('href', '<?php echo base_url('etc/klaim'); ?>/' + response['data'].kode + '/' + attachment);
                                    $('#unduh-link').show();
                                    $('#unggah-button').hide();
                                }
                            });

                            if (akses === 'korlap') {
                                $('#status-div').hide();
                            } else {
                                $('#status-div').show();
                            }
                        }
                    }
                });
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 1, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/klaim', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '') {
                        $('#unduh-link').attr('href', '<?php echo base_url('etc/klaim'); ?>/' + $('#kode-input').val() + '/' + filename);
                        $('#unduh-link').show();
                        $('#unggah-button').hide();
                    }

                    $('#filedocument').fileinput('clear');
                    $('#unggah-modal').modal('hide');
                });
            }

            function nominalFormats() {
                $('.nominal-text').each(function (i, obj) {
                    var nominalText = $(obj).val().replace('Rp. ', '');
                    nominalText = nominalText.replace(/\./g, '');
                    $(obj).val(nominalText);
                });
            }
        </script>
    </body>
</html>