<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class FormatDate {

    private $months = array(
        1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni',
        7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'
    );
    private $days = array(1 => 'Senin', 2 => 'Selasa', 3 => 'Rabu', 4 => 'Kamis', 5 => 'Jumat', 6 => 'Sabtu', 7 => 'Minggu');

    public function __construct() {
        $this->CI = & get_instance(); // duplikasi objek CI
        $this->CI->load->helper('date');
    }

    public function setDate($date = '') { // text to mysql
        if ($date == '') {
            $date = mdate('%d/%m/%Y', time());
        }

        $dateSplit = explode('/', $date);

        return $dateSplit[2] . '-' . $dateSplit[1] . '-' . $dateSplit[0];
    }

    public function getDate($date, $isComponent = false) { // mysql to text/component
        if ($isComponent) { // use for html component
            if ($date != '') {
                return self::_getFDate($date);
            } else {
                return $date;
            }
        } else {
            $date = mdate('%j:%n:%Y', strtotime($date));
            $dateSplit = explode(':', $date);

            return $dateSplit[0] . ' ' . $this->months[$dateSplit[1]] . ' ' . $dateSplit[2];
        }
    }

    public function getDay($at = 0) {
        return $this->days[(($at == 0) ? date('N') : $at)];
    }

    public function getDays() {
        return $this->days;
    }

    public function getMonth($at = 1) {
        return $this->months[$at];
    }

    public function getMonthRoman($number = 1) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';

        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;

                    break;
                }
            }
        }

        return $returnValue;
    }

    public function setDateTime($datetime) { // text to mysql
        $dateTimeSplit = explode(' ', $datetime);
        $date = $dateTimeSplit[0];
        $time = $dateTimeSplit[1];
        $dateSplit = explode('/', $date);
        $time = $time . ':00';

        return $dateSplit[2] . '-' . $dateSplit[1] . '-' . $dateSplit[0] . ' ' . $time;
    }

    public function getDateTime($datetime, $isComponent = false) { // mysql to text/component
        if ($datetime != '') {
            $dateTimeSplit = explode(' ', $datetime);
            $date = $dateTimeSplit[0];
            $time = $dateTimeSplit[1];

            if ($isComponent) { // use for html component
                $dateSplit = explode('-', $date);
                $timeSplit = explode(':', $time);
                $time = $timeSplit[0] . ':' . $timeSplit[1];

                return $dateSplit[2] . '/' . $dateSplit[1] . '/' . $dateSplit[0] . ' ' . $time;
            } else {
                $date = mdate('%j:%n:%Y', strtotime($date));
                $dateSplit = explode(':', $date);

                return $dateSplit[0] . ' ' . $this->months[$dateSplit[1]] . ' ' . $dateSplit[2] . ' ' . $time;
            }
        } else {
            return $datetime;
        }
    }

    private function _getFDate($date) {
        $dateSplit = explode('-', $date);
        $dateConvert = $dateSplit[2] . '/' . $dateSplit[1] . '/' . $dateSplit[0];

        if ($dateConvert == '00/00/0000') {
            $dateConvert = '';
        }

        return $dateConvert;
    }

}
