<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of restapi
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class RestAPI extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo json_encode(array('status' => 1, 'pesan' => 'Akses Tidak Valid!'));
    }

    public function masuk() {
        $request = $this->input->post(NULL, TRUE); // use XSS Filtering
        $data = array();
        $status = 0;

        if ($request !== FALSE) {
            $akun = $this->model->getRecord(array('table' => 'data_akun', 'where' => array('id' => $request['id-input'], 'pin' => $this->cryptorgram->encrypt($request['pin-input']), 'terpakai' => 1)));

            if ($akun != NULL) {
                $status = 1;
                $data = array('_akun' => $akun->kode, '_bio' => $akun->biodata, '_id' => $akun->id, '_otoritas' => $akun->otoritas, '_proyek' => $akun->proyek);
            }
        }

        echo json_encode(array('status' => $status, 'data' => $data));
    }

    // create, update, or delete
    public function simpan() {
        $request = $this->input->post(NULL, TRUE); // use XSS Filtering
        $pesan = 'Akses Tidak Valid!';
        $status = 0;

        if ($request !== FALSE) {
            $model = 'model' . $request['model-input'];
            $this->load->model($model);
            $this->form_validation->set_rules($this->$model->getRules($request['action-input'])); // create, update, delete rules

            if ($this->form_validation->run() == FALSE) {
                $delimiter = '>>>';
                $this->form_validation->set_error_delimiters($delimiter, '|');
                $pesan = validation_errors();
            } else {
                if ($this->$model->doAction($request)) {
                    $status = 1;
                    $pesan = 'Proses Berhasil';
                }
            }
        }

        echo json_encode(array('status' => $status, 'pesan' => $pesan));
    }

    // view record & records
    public function detail() {
        $model = 'model' . $this->input->post('param');
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getData($this->input->post('kode'))));
    }

    public function tabel($name, $query = NULL) {
        $model = 'model' . $name;
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getTabel($query)));
    }

    public function lokasi() {
        $request = $this->input->post(NULL, FALSE); // don't use XSS Filtering
        $data = array('long' => '', 'lat' => '', 'waktu' => '');

        if ($request !== FALSE) {
            $record = $this->model->getRecord(array('table' => 'data_proyek_gps', 'where' => array('terpakai' => 1, 'proyek' => $request['proyek-kode'], 'biodata' => $request['biodata-kode']), 'sort' => 'waktu desc'));

            if ($record != NULL) {
                $data = array('long' => $record->long, 'lat' => $record->lat, 'waktu' => $record->waktu);
            }
        }

        echo json_encode(array('data' => $data));
    }

    public function karir() {
        $request = $this->input->post(NULL, FALSE); // don't use XSS Filtering
        $data = array();

        if ($request !== FALSE) {
            $perPegawai = array();
            $thnPegawai = array();
            $spPegawai = array();
            $todayInfo = array();
            $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('terpakai' => 1, 'kode' => $request['biodata-kode'])));

            if ($rBiodata != NULL) {
                $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $rBiodata->kode, 'proyek' => $request['proyek-kode'], 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                $isShow = FALSE;
                $sebagai = '';

                if ($rKontrak != NULL) {
                    // kontrak masih berlaku
                    if ($rKontrak->habis >= date('Y-m-d')) {
                        $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                        $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                        $isShow = in_array($sebagai, array('Helper', 'Driver', 'Korlap'));
                    }
                }

                if ($isShow) {
                    foreach ($this->model->getList(array('table' => 'data_presensi_info', 'where' => array('biodata' => $rBiodata->kode, 'proyek' => $request['proyek-kode']))) as $rPresensi) {
                        if (in_array($sebagai, array('Helper', 'Driver', 'Korlap'))) {
                            $bulan = substr($rPresensi->waktu, 5, 2);
                            $tanggal = substr($rPresensi->waktu, 0, 10);
                            $rLembur = $this->model->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('presensi' => $rPresensi->kode)));

                            if (!isset($perPegawai[$bulan][$rPresensi->status])) {
                                $perPegawai[$bulan][$rPresensi->status] = 1;
                            } else {
                                $perPegawai[$bulan][$rPresensi->status] ++;
                            }

                            if (!isset($thnPegawai[$rPresensi->status])) {
                                $thnPegawai[$rPresensi->status] = 1;
                            } else {
                                $thnPegawai[$rPresensi->status] ++;
                            }

                            if ($tanggal === date('Y-m-d')) {
                                $jamIn = $tanggal . ' ' . $rKontrak->masuk . ':00';
                                $jamOut = $tanggal . ' ' . $rKontrak->selesai . ':00';
                                $rPulang = $this->model->getRecord(array('table' => 'data_presensi_pulang', 'where' => array('presensi' => $rPresensi->kode)));
                                $todayInfo['masuk'] = array('status' => $rPresensi->status, 'waktu' => $rPresensi->waktu, 'harus' => date('Y-m-d H:i:s', strtotime($jamIn)));

                                if ($rPulang != NULL) {
                                    $todayInfo['pulang'] = array('status' => 'pulang', 'waktu' => $rPulang->waktu, 'harus' => date('Y-m-d H:i:s', strtotime($jamOut)));
                                }
                            }

                            if ($rLembur != NULL) {
                                if (!isset($perPegawai[$bulan]['lembur'])) {
                                    $perPegawai[$bulan]['lembur'] = 1;
                                } else {
                                    $perPegawai[$bulan]['lembur'] ++;
                                }

                                if ($tanggal === date('Y-m-d')) {
                                    $todayInfo['lembur'] = TRUE;
                                }
                            }
                        }
                    }

                    $wktMasuk = '-';
                    $stMasuk = 'alfa';
                    $tepatMasuk = '';
                    $wktPulang = '-';
                    $stPulang = '-';
                    $strLembur = array(TRUE => 'YA', FALSE => 'TIDAK');
                    $wrnStatus = array('hadir' => '#739e73', 'izin' => '#c79121', 'sakit' => '#999', 'alfa' => '#a90329', 'ok' => '#57889c', 'pulang cepat' => '#c79121', '-' => '#999');
                    $isLembur = (isset($todayInfo['lembur']) ? $todayInfo['lembur'] : FALSE);

                    if (isset($todayInfo['masuk'])) {
                        $wktMasuk = $this->formatdate->getDateTime($todayInfo['masuk']['waktu']);
                        $stMasuk = $todayInfo['masuk']['status'];
                        $tepatMasuk = (strtotime($todayInfo['masuk']['waktu']) < strtotime($todayInfo['masuk']['harus'])) ? '' : 'terlambat';
                    }

                    if (isset($todayInfo['pulang'])) {
                        $wktPulang = $this->formatdate->getDateTime($todayInfo['pulang']['waktu']);
                        $stPulang = (strtotime($todayInfo['pulang']['waktu']) < strtotime($todayInfo['pulang']['harus'])) ? 'pulang cepat' : 'ok';
                    }

                    foreach ($this->model->getList(array('table' => 'data_proyek_spsl', 'where' => array('terpakai' => 1, 'jenis' => 'sp', 'biodata' => $rBiodata->kode, 'proyek' => $request['proyek-kode']), 'sort' => 'ke asc')) as $rSP) {
                        array_push($spPegawai, array('tanggal' => $this->formatdate->getDate($rSP->tanggal), 'nomor' => strtoupper($rSP->nomor), 'ke' => 'SP-' . $rSP->ke));
                    }

                    $data = array(
                        'nik' => strtoupper($rBiodata->id), 'nama' => ucwords($rBiodata->nama), 'jabatan' => $sebagai,
                        'hadir' => (isset($perPegawai[$request['bulan-digit']]['hadir']) ? $perPegawai[$request['bulan-digit']]['hadir'] : 0),
                        'izin' => (isset($perPegawai[$request['bulan-digit']]['izin']) ? $perPegawai[$request['bulan-digit']]['izin'] : 0),
                        'alfa' => (isset($perPegawai[$request['bulan-digit']]['alfa']) ? $perPegawai[$request['bulan-digit']]['alfa'] : 0),
                        'sakit' => (isset($perPegawai[$request['bulan-digit']]['sakit']) ? $perPegawai[$request['bulan-digit']]['sakit'] : 0),
                        'lembur' => (isset($perPegawai[$request['bulan-digit']]['lembur']) ? $perPegawai[$request['bulan-digit']]['lembur'] : 0),
                        'hadir_sekarang' => array('warna' => $wrnStatus[$stMasuk], 'jam' => $wktMasuk, 'info' => strtoupper($stMasuk . ' ' . $tepatMasuk)),
                        'pulang_sekarang' => array('warna' => $wrnStatus[$stPulang], 'jam' => $wktPulang, 'info' => strtoupper($stPulang)),
                        'lembur_sekarang' => array('warna' => ($isLembur) ? 'info' : 'default', 'info' => $strLembur[$isLembur]),
                        'statistik' => array(
                            'hadir' => (isset($thnPegawai['hadir']) ? $thnPegawai['hadir'] : 0),
                            'izin' => (isset($thnPegawai['izin']) ? $thnPegawai['izin'] : 0),
                            'alfa' => (isset($thnPegawai['alfa']) ? $thnPegawai['alfa'] : 0),
                            'sakit' => (isset($thnPegawai['sakit']) ? $thnPegawai['sakit'] : 0)
                        ), 'sp' => $spPegawai
                    );
                }
            }
        }

        echo json_encode(array('data' => $data));
    }

    // save images
    public function gambar() {
        $request = $this->input->post(NULL, FALSE); // don't use XSS Filtering
        $pesan = 'Akses Tidak Valid!';
        $status = 0;

        if ($request !== FALSE) {
            $path = 'etc/' . $request['dir-text'] . '/' . $request['kode-text'];

            if (self::_dirInit($path)) {
                $parts = explode(';base64,', $request['gambar-text']);
                $aux = explode('image/', $parts[0]);
                $type = $aux[1];
                $base64 = base64_decode($parts[1]);
                $file = $path . '/' . $request['kode-text'] . '' . random_string('alnum', 3) . '.' . $type;
                $simpan = file_put_contents($file, $base64);

                if ($simpan !== FALSE) {
                    $status = 1;
                    $pesan = 'Proses Berhasil';
                }
            }
        }

        echo json_encode(array('status' => $status, 'pesan' => $pesan));
    }

    private function _dirInit($path) {
        $continue = FALSE;

        if (!file_exists($path)) {
            if (!is_dir($path)) {
                $continue = (!file_exists($path)) ? mkdir($path, 0777, TRUE) : TRUE;
            } else {
                $continue = TRUE;
            }
        } else {
            $continue = TRUE;
        }

        return $continue;
    }

}
