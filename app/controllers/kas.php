<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of kas
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Kas extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        redirect(site_url());
    }

    public function gaji($query, $jenis = 'gatot') {
        $data = array();

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);

            if ($this->sesiAkun !== FALSE) {
                $blnInt = intval($queries[0]);
                $blnStr = $blnInt;

                if ($blnInt < 10) {
                    $blnStr = '0' . $blnInt;
                }

                foreach ($this->model->getList(array('table' => 'data_proyek_info', 'where' => array('terpakai' => 1), 'sort' => 'proyek asc')) as $rProyek) {
                    $tagih = 0;
                    $bayar = 0;
                    $margin = 0;
                    $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                    $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                    $rKendaraan = $this->model->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));

                    if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                        foreach ($this->model->getList(array('table' => 'data_presensi_rekap', 'where' => array('jenis' => 'presensi', 'status' => 'setuju', 'MONTH(dari)' => $blnStr, 'YEAR(dari)' => $queries[1], 'proyek' => $rProyek->kode, 'terpakai' => 1))) as $rRekap) {
                            $rInv = $this->model->getRecord(array('table' => 'data_proyek_invoice', 'where' => array('proyek' => $rProyek->kode, 'presensi' => $rRekap->kode, 'terpakai' => 1)));

                            if ($rInv != NULL) {
                                foreach ($this->model->getList(array('table' => 'data_proyek_gaji', 'where' => array('proyek' => $rProyek->kode, 'invoice' => $rInv->kode, 'terpakai' => 1), 'sort' => 'waktu asc')) as $rGaji) {
                                    $total = 0;

                                    if ($jenis === 'gatot') { // gatot = gaji total ^_^
                                        $total = ($rGaji->gaji + $rGaji->makan + $rGaji->lembur + $rGaji->inap + $rGaji->ritase + $rGaji->skr + $rGaji->insentif + $rGaji->klaim) - ($rGaji->pot_hadir + $rGaji->pot_kasbon + $rGaji->pot_lain_isi);
                                    } else if ($jenis === 'cumalembur') {
                                        $total = $rGaji->lembur;
                                    } else if ($jenis === 'cumaupah') {
                                        $total = $rGaji->insentif;
                                    } else if ($jenis === 'cumainap') {
                                        $total = $rGaji->inap;
                                    } else if ($jenis === 'cumaritase') {
                                        $total = $rGaji->ritase;
                                    } else if ($jenis === 'cumaskr') {
                                        $total = $rGaji->skr;
                                    }

                                    if ($rGaji->status === 'proses') {
                                        $tagih += $total;
                                    } else if ($rGaji->status === 'bayar') {
                                        $bayar += $total;
                                        $tagih += $total;
                                    }
                                }
                            }
                        }

                        $margin = $tagih - $bayar;
                        $data[] = array(
                            'kode' => '0',
                            'proyek' => ucwords($rProyek->proyek) . ' | ' . ucwords($rTipe->tipe) . ' | ' . ucwords($rLokasi->lokasi) . ' | ' . ucwords($rKendaraan->kendaraan),
                            'tagih' => self::_toRp($tagih), 'bayar' => self::_toRp($bayar), 'margin' => self::_toRp($margin),
                            'tagihInt' => $tagih, 'bayarInt' => $bayar,
                            'aksi' => ''
                        );
                    }
                }
            }
        }

        echo json_encode(array('data' => $data)); // DATATABLES
    }

    public function fee($query, $jenis = 'feeaja') {
        $data = array();

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);

            if ($this->sesiAkun !== FALSE) {
                $blnInt = intval($queries[0]);
                $blnStr = $blnInt;

                if ($blnInt < 10) {
                    $blnStr = '0' . $blnInt;
                }

                foreach ($this->model->getList(array('table' => 'data_proyek_info', 'where' => array('terpakai' => 1), 'sort' => 'proyek asc')) as $rProyek) {
                    $tagih = 0;
                    $bayar = 0;
                    $margin = 0;
                    $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                    $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                    $rKendaraan = $this->model->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));

                    if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                        foreach ($this->model->getList(array('table' => 'data_proyek_invoice', 'where' => array('terpakai' => 1, 'MONTH(waktu)' => $blnStr, 'YEAR(waktu)' => $queries[1], 'proyek' => $rProyek->kode), 'sort' => 'waktu asc')) as $rInv) {
                            if ($jenis === 'feeaja') {
                                $tagih += ($rInv->fee_driver + $rInv->fee_helper + $rInv->fee_korlap);
                            } else if ($jenis === 'ppnaja') {
                                $tagih += $rInv->ppn;
                            }
                        }

                        $linkBtn = '';

                        if ($jenis === 'ppnaja') {
                            $rPPN = $this->model->getRecord(array('table' => 'data_keu_ppn', 'where' => array('proyek' => $rProyek->kode, 'bulan' => $blnStr, 'tahun' => $queries[1], 'terpakai' => 1)));
                            $linkBtn = '<a href="' . $rProyek->kode . '___' . $blnStr . '___' . $queries[1] . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';

                            if ($rPPN != NULL) {
                                $bayar = $rPPN->ppn;
                                $linkBtn .= ' <a href="' . $rProyek->kode . '___' . $blnStr . '___' . $queries[1] . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                            }
                        }

                        $margin = $tagih - $bayar;

                        if ($margin < 0) {
                            $margin = $margin * -1;
                        }

                        $data[] = array(
                            'kode' => '0',
                            'proyek' => ucwords($rProyek->proyek) . ' | ' . ucwords($rTipe->tipe) . ' | ' . ucwords($rLokasi->lokasi) . ' | ' . ucwords($rKendaraan->kendaraan),
                            'tagih' => self::_toRp($tagih), 'bayar' => self::_toRp($bayar), 'margin' => self::_toRp($margin),
                            'tagihInt' => $tagih, 'bayarInt' => $bayar,
                            'aksi' => $linkBtn
                        );
                    }
                }
            }
        }

        echo json_encode(array('data' => $data)); // DATATABLES
    }

    public function lain($query, $jenis = 'bpjs') {
        $data = array();

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);

            if ($this->sesiAkun !== FALSE) {
                $blnInt = intval($queries[0]);
                $blnStr = $blnInt;

                if ($blnInt < 10) {
                    $blnStr = '0' . $blnInt;
                }

                foreach ($this->model->getList(array('table' => 'data_proyek_info', 'where' => array('terpakai' => 1), 'sort' => 'proyek asc')) as $rProyek) {
                    $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                    $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                    $rKendaraan = $this->model->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));
                    $tagih = 0;
                    $bayar = 0;
                    $margin = 0;

                    if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                        foreach ($this->model->getList(array('table' => 'data_proyek_invoice', 'where' => array('terpakai' => 1, 'MONTH(waktu)' => $blnStr, 'YEAR(waktu)' => $queries[1], 'proyek' => $rProyek->kode), 'sort' => 'waktu asc')) as $rInv) {
                            if ($this->model->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $rInv->presensi, 'jenis' => 'presensi', 'status' => 'setuju', 'terpakai' => 1))) != NULL) {
                                foreach ($this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                                    $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                                    $isShow = FALSE;
                                    $sebagai = '';
                                    $rJabatan = NULL;

                                    if ($rKontrak != NULL) {
                                        // kontrak masih berlaku
                                        if ($rKontrak->habis >= date('Y-m-d')) {
                                            $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                                            $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                                            $isShow = TRUE;
                                        }
                                    }

                                    if ($isShow) {
                                        if ($jenis === 'bpjs') {
                                            if ($rProyek->invoice_bpjs > 0) {
                                                $tagih += round(($rProyek->invoice_bpjs / 100) * $rProyek->invoice_umk);
                                            }
                                        } else if ($jenis === 'thr') {
                                            $tagih += $rProyek->invoice_thr;
                                        } else if ($jenis === 'supervisi') {
                                            $tagih += $rProyek->invoice_supervisi;
                                        } else if ($jenis === 'seragam') {
                                            $tagih += $rProyek->invoice_seragam;
                                        }
                                    }
                                }
                            }
                        }

                        $linkBtn = '<a href="' . $rProyek->kode . '___' . $blnStr . '___' . $queries[1] . '" data-tagih="' . self::_toRp($tagih) . '" class="actionBtn btn btn-primary btn-flat">Ubah</a>';
                        $rKeu = $this->model->getRecord(array('table' => 'data_keu_lain', 'where' => array('proyek' => $rProyek->kode, 'bulan' => $blnStr, 'tahun' => $queries[1], 'terpakai' => 1, 'jenis' => $jenis)));

                        if ($rKeu != NULL) {
                            $bayar = $rKeu->bayar;
                            $tagih = $rKeu->tagih;
                            $linkBtn .= ' <a href="' . $rProyek->kode . '___' . $blnStr . '___' . $queries[1] . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                        }

                        $margin = $tagih - $bayar;

                        if ($margin < 0) {
                            $margin = $margin * -1;
                        }

                        $data[] = array(
                            'kode' => '0',
                            'proyek' => ucwords($rProyek->proyek) . ' | ' . ucwords($rTipe->tipe) . ' | ' . ucwords($rLokasi->lokasi) . ' | ' . ucwords($rKendaraan->kendaraan),
                            'tagih' => self::_toRp($tagih), 'bayar' => self::_toRp($bayar), 'margin' => self::_toRp($margin),
                            'tagihInt' => $tagih, 'bayarInt' => $bayar,
                            'aksi' => $linkBtn
                        );
                    }
                }
            }
        }

        echo json_encode(array('data' => $data)); // DATATABLES
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

}
