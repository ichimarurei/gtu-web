<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modul
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Modul extends CI_Controller {

    private $sesiAkun; // store session
    private $defaultPage = 'masuk';

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        self::tampil();
    }

    public function tampil($folder = 'akses', $halaman = null) {
        if ($halaman == NULL) {
            $halaman = $this->defaultPage;
        }

        if ($this->sesiAkun !== FALSE) {
            if ($halaman === $this->defaultPage) {
                $halaman = 'beranda';
            }

            $this->load->view($folder . '/' . $halaman);
        } else {
            $this->load->view('akses/' . $this->defaultPage);
        }
    }

}
