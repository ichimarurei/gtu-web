<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of excel
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Excel extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
        $this->load->library('excelutil');
    }

    public function index() {
        redirect(site_url());
    }

    public function proyek() {
        if ($this->sesiAkun !== FALSE) {
            $request = $this->input->post(NULL, TRUE); // use XSS Filtering
            $this->objPHPExcel = new PHPExcel();
            $this->objPHPExcel->getActiveSheet();
            $objWorkSheet = $this->objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Data Proyek');
            $objWorkSheet->setCellValue('A1', 'NAMA PROYEK')->mergeCells('A1:B1');
            $objWorkSheet->setCellValue('A2', 'JENIS PROYEK')->mergeCells('A2:B2');
            $objWorkSheet->setCellValue('A3', 'LOKASI')->mergeCells('A3:B3');
            $objWorkSheet->setCellValue('A4', 'KENDARAAN')->mergeCells('A4:B4');
            $objWorkSheet->setCellValue('A6', 'INVOICE')->mergeCells('A6:L6');
            $objWorkSheet->setCellValue('A7', 'MUAT INAP');
            $objWorkSheet->setCellValue('B7', 'SKR');
            $objWorkSheet->setCellValue('C7', 'BPJS');
            $objWorkSheet->setCellValue('D7', 'INSENTIF PENGIRIMAN');
            $objWorkSheet->setCellValue('E7', 'RITASE 2');
            $objWorkSheet->setCellValue('F7', 'PPH23');
            $objWorkSheet->setCellValue('G7', 'PPN');
            $objWorkSheet->setCellValue('H7', 'UMK');
            $objWorkSheet->setCellValue('I7', 'THR');
            $objWorkSheet->setCellValue('J7', 'SUPERVISI');
            $objWorkSheet->setCellValue('K7', 'SERAGAM');
            $objWorkSheet->setCellValue('L7', 'MANAGEMENT FEE');
            $objWorkSheet->setCellValue('A10', 'GAJI & UANG MAKAN')->mergeCells('A10:J10');
            $objWorkSheet->setCellValue('A11', 'GAJI KORLAP');
            $objWorkSheet->setCellValue('B11', 'UANG MAKAN KORLAP');
            $objWorkSheet->setCellValue('C11', 'GAJI DRIVER');
            $objWorkSheet->setCellValue('D11', 'UANG MAKAN DRIVER');
            $objWorkSheet->setCellValue('E11', 'GAJI HELPER');
            $objWorkSheet->setCellValue('F11', 'UANG MAKAN HELPER');
            $objWorkSheet->setCellValue('G11', 'GAJI ADMIN');
            $objWorkSheet->setCellValue('H11', 'UANG MAKAN ADMIN');
            $objWorkSheet->setCellValue('I11', 'GAJI STAF');
            $objWorkSheet->setCellValue('J11', 'UANG MAKAN STAF');
            $objWorkSheet->setCellValue('A14', 'UANG LEMBUR DRIVER')->mergeCells('A14:C14');
            $objWorkSheet->setCellValue('A15', 'UANG LEMBUR HELPER')->mergeCells('A15:C15');

            if ($request !== FALSE) {
                $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $request['param'])));

                if ($rProyek != NULL) {
                    $mngFee = $rProyek->invoice_fee_korlap + $rProyek->invoice_fee_driver + $rProyek->invoice_fee_helper;
                    $objWorkSheet->setCellValue('C1', ucwords($rProyek->proyek));
                    $objWorkSheet->setCellValue('A8', self::_fixDec($rProyek->invoice_inap));
                    $objWorkSheet->setCellValue('B8', self::_fixDec($rProyek->invoice_skr));
                    $objWorkSheet->setCellValue('C8', self::_fixDec($rProyek->invoice_bpjs));
                    $objWorkSheet->setCellValue('D8', self::_fixDec($rProyek->invoice_pengiriman));
                    $objWorkSheet->setCellValue('E8', self::_fixDec($rProyek->invoice_ritase));
                    $objWorkSheet->setCellValue('F8', self::_fixDec($rProyek->invoice_pph));
                    $objWorkSheet->setCellValue('G8', self::_fixDec($rProyek->invoice_ppn));
                    $objWorkSheet->setCellValue('H8', self::_toRp($rProyek->invoice_umk));
                    $objWorkSheet->setCellValue('I8', self::_toRp($rProyek->invoice_thr));
                    $objWorkSheet->setCellValue('J8', self::_toRp($rProyek->invoice_supervisi));
                    $objWorkSheet->setCellValue('K8', self::_toRp($rProyek->invoice_seragam));
                    $objWorkSheet->setCellValue('L8', self::_toRp($mngFee));
                    $objWorkSheet->setCellValue('A12', self::_toRp($rProyek->gaji_korlap));
                    $objWorkSheet->setCellValue('B12', self::_toRp($rProyek->makan_korlap));
                    $objWorkSheet->setCellValue('C12', self::_toRp($rProyek->gaji_driver));
                    $objWorkSheet->setCellValue('D12', self::_toRp($rProyek->makan_driver));
                    $objWorkSheet->setCellValue('E12', self::_toRp($rProyek->gaji_helper));
                    $objWorkSheet->setCellValue('F12', self::_toRp($rProyek->makan_helper));
                    $objWorkSheet->setCellValue('G12', self::_toRp($rProyek->gaji_admin));
                    $objWorkSheet->setCellValue('H12', self::_toRp($rProyek->makan_admin));
                    $objWorkSheet->setCellValue('I12', self::_toRp($rProyek->gaji_staf));
                    $objWorkSheet->setCellValue('J12', self::_toRp($rProyek->makan_staf));
                    $objWorkSheet->setCellValue('D14', self::_toRp($rProyek->lembur_driver));
                    $objWorkSheet->setCellValue('D15', self::_toRp($rProyek->lembur_helper));
                    $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                    $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                    $rKendaraan = $this->model->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));

                    if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                        $objWorkSheet->setCellValue('C2', ucwords($rTipe->tipe));
                        $objWorkSheet->setCellValue('C3', ucwords($rLokasi->lokasi));
                        $objWorkSheet->setCellValue('C4', ucwords($rKendaraan->kendaraan));
                    }
                }
            }

            self::_done('Data_Proyek_' . $this->session->userdata('_id') . '.xls');
        } else {
            self::index();
        }
    }

    public function semuaProyek() {
        if ($this->sesiAkun !== FALSE) {
            $this->objPHPExcel = new PHPExcel();
            $this->objPHPExcel->getActiveSheet();
            $objWorkSheet = $this->objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Data Semua Proyek');
            $objWorkSheet->setCellValue('A1', 'NO')->mergeCells('A1:A2');
            $objWorkSheet->setCellValue('B1', 'NAMA PROYEK')->mergeCells('B1:B2');
            $objWorkSheet->setCellValue('C1', 'JENIS PROYEK')->mergeCells('C1:C2');
            $objWorkSheet->setCellValue('D1', 'LOKASI')->mergeCells('D1:D2');
            $objWorkSheet->setCellValue('E1', 'KENDARAAN')->mergeCells('E1:E2');
            $objWorkSheet->setCellValue('F1', 'INVOICE')->mergeCells('F1:Q1');
            $objWorkSheet->setCellValue('F2', 'MUAT INAP');
            $objWorkSheet->setCellValue('G2', 'SKR');
            $objWorkSheet->setCellValue('H2', 'BPJS');
            $objWorkSheet->setCellValue('I2', 'INSENTIF PENGIRIMAN');
            $objWorkSheet->setCellValue('J2', 'RITASE 2');
            $objWorkSheet->setCellValue('K2', 'PPH23');
            $objWorkSheet->setCellValue('L2', 'PPN');
            $objWorkSheet->setCellValue('M2', 'UMK');
            $objWorkSheet->setCellValue('N2', 'THR');
            $objWorkSheet->setCellValue('O2', 'SUPERVISI');
            $objWorkSheet->setCellValue('P2', 'SERAGAM');
            $objWorkSheet->setCellValue('Q2', 'MANAGEMENT FEE');
            $objWorkSheet->setCellValue('R1', 'GAJI & UANG MAKAN')->mergeCells('R1:AA1');
            $objWorkSheet->setCellValue('R2', 'GAJI KORLAP');
            $objWorkSheet->setCellValue('S2', 'UANG MAKAN KORLAP');
            $objWorkSheet->setCellValue('T2', 'GAJI DRIVER');
            $objWorkSheet->setCellValue('U2', 'UANG MAKAN DRIVER');
            $objWorkSheet->setCellValue('V2', 'GAJI HELPER');
            $objWorkSheet->setCellValue('W2', 'UANG MAKAN HELPER');
            $objWorkSheet->setCellValue('X2', 'GAJI ADMIN');
            $objWorkSheet->setCellValue('Y2', 'UANG MAKAN ADMIN');
            $objWorkSheet->setCellValue('Z2', 'GAJI STAF');
            $objWorkSheet->setCellValue('AA2', 'UANG MAKAN STAF');
            $objWorkSheet->setCellValue('AB1', 'UANG LEMBUR DRIVER')->mergeCells('AB1:AB2');
            $objWorkSheet->setCellValue('AC1', 'UANG LEMBUR HELPER')->mergeCells('AC1:AC2');
            $ke = 3;
            $no = 1;

            foreach ($this->model->getList(array('table' => 'data_proyek_info', 'where' => array('terpakai' => 1))) as $rProyek) {
                $mngFee = $rProyek->invoice_fee_korlap + $rProyek->invoice_fee_driver + $rProyek->invoice_fee_helper;
                $objWorkSheet->setCellValue('A' . $ke, $no);
                $objWorkSheet->setCellValue('B' . $ke, ucwords($rProyek->proyek));
                $objWorkSheet->setCellValue('F' . $ke, self::_fixDec($rProyek->invoice_inap));
                $objWorkSheet->setCellValue('G' . $ke, self::_fixDec($rProyek->invoice_skr));
                $objWorkSheet->setCellValue('H' . $ke, self::_fixDec($rProyek->invoice_bpjs));
                $objWorkSheet->setCellValue('I' . $ke, self::_fixDec($rProyek->invoice_pengiriman));
                $objWorkSheet->setCellValue('J' . $ke, self::_fixDec($rProyek->invoice_ritase));
                $objWorkSheet->setCellValue('K' . $ke, self::_fixDec($rProyek->invoice_pph));
                $objWorkSheet->setCellValue('L' . $ke, self::_fixDec($rProyek->invoice_ppn));
                $objWorkSheet->setCellValue('M' . $ke, self::_toRp($rProyek->invoice_umk));
                $objWorkSheet->setCellValue('N' . $ke, self::_toRp($rProyek->invoice_thr));
                $objWorkSheet->setCellValue('O' . $ke, self::_toRp($rProyek->invoice_supervisi));
                $objWorkSheet->setCellValue('P' . $ke, self::_toRp($rProyek->invoice_seragam));
                $objWorkSheet->setCellValue('Q' . $ke, self::_toRp($mngFee));
                $objWorkSheet->setCellValue('R' . $ke, self::_toRp($rProyek->gaji_korlap));
                $objWorkSheet->setCellValue('S' . $ke, self::_toRp($rProyek->makan_korlap));
                $objWorkSheet->setCellValue('T' . $ke, self::_toRp($rProyek->gaji_driver));
                $objWorkSheet->setCellValue('U' . $ke, self::_toRp($rProyek->makan_driver));
                $objWorkSheet->setCellValue('V' . $ke, self::_toRp($rProyek->gaji_helper));
                $objWorkSheet->setCellValue('W' . $ke, self::_toRp($rProyek->makan_helper));
                $objWorkSheet->setCellValue('X' . $ke, self::_toRp($rProyek->gaji_admin));
                $objWorkSheet->setCellValue('Y' . $ke, self::_toRp($rProyek->makan_admin));
                $objWorkSheet->setCellValue('Z' . $ke, self::_toRp($rProyek->gaji_staf));
                $objWorkSheet->setCellValue('AA' . $ke, self::_toRp($rProyek->makan_staf));
                $objWorkSheet->setCellValue('AB' . $ke, self::_toRp($rProyek->lembur_driver));
                $objWorkSheet->setCellValue('AC' . $ke, self::_toRp($rProyek->lembur_helper));
                $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                $rKendaraan = $this->model->getRecord(array('table' => 'data_proyek_kendaraan', 'where' => array('kode' => $rProyek->kendaraan)));

                if ($rLokasi != NULL && $rTipe != NULL && $rKendaraan != NULL) {
                    $objWorkSheet->setCellValue('C' . $ke, ucwords($rTipe->tipe));
                    $objWorkSheet->setCellValue('D' . $ke, ucwords($rLokasi->lokasi));
                    $objWorkSheet->setCellValue('E' . $ke, ucwords($rKendaraan->kendaraan));
                }

                $no++;
                $ke++;
            }

            self::_done('Data_Proyek_' . $this->session->userdata('_id') . '.xls');
        } else {
            self::index();
        }
    }

    public function detilInv() {
        if ($this->sesiAkun !== FALSE) {
            $request = $this->input->post(NULL, TRUE); // use XSS Filtering
            $this->objPHPExcel = new PHPExcel();
            $this->objPHPExcel->getActiveSheet();
            $objWorkSheet = $this->objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Rinci Invoice');
            $objWorkSheet->getDefaultStyle()->applyFromArray(array(
                'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ));

            if ($request !== FALSE) {
                $rInvoice = $this->model->getRecord(array('table' => 'data_proyek_invoice', 'where' => array('kode' => $request['param'])));

                if ($rInvoice != NULL) {
                    $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rInvoice->proyek)));
                    $rRekap = $this->model->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $rInvoice->presensi)));

                    if ($rProyek != NULL && $rRekap != NULL) {
                        $rLokasi = $this->model->getRecord(array('table' => 'data_proyek_lokasi', 'where' => array('kode' => $rProyek->lokasi)));
                        $rTipe = $this->model->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $rProyek->tipe)));
                        $drivers = array();
                        $helpers = array();
                        $lPresensi = array();
                        $objDari = new DateTime($rRekap->dari);
                        $objHingga = new DateTime($rRekap->hingga);
                        $diff = $objHingga->diff($objDari);
                        $hariKerja = (($diff != NULL) ? (intval($diff->d) + 1) : 0);

                        if ($hariKerja > 26) {
                            $hariKerja = 26;
                        }

                        foreach ($this->model->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rRekap->kode, 'jenis' => 'presensi', 'terpakai' => 1))) as $record) {
                            $rData = $this->model->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $record->data)));

                            if ($rData != NULL) {
                                $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rData->biodata, 'terpakai' => 1)));

                                if ($rBiodata != NULL) {
                                    $waktu = explode(' ', $rData->waktu);
                                    $lPresensi[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
                                }
                            }
                        }

                        foreach ($this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                            $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

                            if ($rKontrak != NULL) {
                                // kontrak masih berlaku
                                if ($rKontrak->habis >= date('Y-m-d')) {
                                    $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                                    $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                                    $gajian = 0;
                                    $persenFee = 0;
                                    $intAbsen = 0;
                                    $dibayar = 0;
                                    $pph23 = 0;

                                    if ($sebagai === 'Driver') {
                                        $gajian = $rProyek->gaji_driver;
                                        $persenFee = $rProyek->invoice_fee_driver;
                                    } else if ($sebagai === 'Helper') {
                                        $gajian = $rProyek->gaji_helper;
                                        $persenFee = $rProyek->invoice_fee_helper;
                                    }

                                    if ($rKontrak->gaji > 0) {
                                        $gajian = $rKontrak->gaji;
                                    }

                                    if ($rKontrak->fee > 0) {
                                        $persenFee = $rKontrak->fee;
                                    }

                                    $gajiHarian = round($gajian / 26);
                                    $gajinya = 0;

                                    if (isset($lPresensi[$record->kode])) {
                                        foreach ($lPresensi[$record->kode] as $kPresensi => $vPresensi) {
                                            if (in_array($vPresensi[0]->status, array('hadir', 'sakit'))) {
                                                $gajinya += $gajiHarian;
                                            } else {
                                                $intAbsen++;
                                            }
                                        }
                                    }

                                    $bayarTetap = $gajinya + (($rJabatan != NULL) ? $rJabatan->komisi : 0);

                                    if ($persenFee > 0) {
                                        $dibayar = round(($persenFee / 100) * $bayarTetap);
                                    }

                                    if ($rProyek->invoice_pph > 0 && $dibayar > 0) {
                                        $pph23 = round(($rProyek->invoice_pph / 100) * $dibayar);
                                    }

                                    $results = array(
                                        'nama' => ucwords($record->nama),
                                        'gaji' => $gajinya, 'pph' => $pph23, 'fee' => $dibayar,
                                        'hari' => ($hariKerja - $intAbsen), 'absen' => $intAbsen
                                    );

                                    if ($sebagai === 'Driver') {
                                        array_push($drivers, $results);
                                    } else if ($sebagai === 'Helper') {
                                        array_push($helpers, $results);
                                    }
                                }
                            }
                        }

                        if ($rLokasi != NULL && $rTipe != NULL) {
                            $jasaSupir = 0;
                            $jasaKernet = 0;
                            $feeSupir = 0;
                            $feeKernet = 0;
                            // header file
                            $periode = $this->formatdate->getDate($rRekap->dari) . ' - ' . $this->formatdate->getDate($rRekap->hingga);
                            $objWorkSheet->setCellValue('A1', 'Rincian Tagihan Supir & Kernet ' . strtoupper($rTipe->tipe) . ' Periode ' . $periode)->mergeCells('A1:M1');
                            $objWorkSheet->setCellValue('A2', strtoupper($rProyek->proyek))->mergeCells('A2:H2');
                            $objWorkSheet->setCellValue('A3', 'Lokasi: ' . strtoupper($rLokasi->lokasi))->mergeCells('A3:H3');
                            // header part "Supir"
                            $objWorkSheet->setCellValue('A5', 'Supir')->mergeCells('A5:B9');
                            $objWorkSheet->getStyle('A5:B9')->applyFromArray(array(
                                'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ));
                            $objWorkSheet->setCellValue('C5', 'Komponen Upah A')->mergeCells('C5:D5');
                            $objWorkSheet->setCellValue('E5', 'Harga/bln')->mergeCells('E5:F5');
                            $objWorkSheet->setCellValue('G5', 'Harga/hr')->mergeCells('G5:H5');
                            $objWorkSheet->setCellValue('I6', 'Komponen Upah B')->mergeCells('I6:J6');
                            $objWorkSheet->setCellValue('K5', 'Harga/bln')->mergeCells('K5:L5');
                            $objWorkSheet->setCellValue('M5', 'Harga/hr')->mergeCells('M5:N5');
                            $objWorkSheet->setCellValue('A11', 'NO');
                            $objWorkSheet->setCellValue('B11', 'NAMA')->mergeCells('B11:D11');
                            $objWorkSheet->setCellValue('E11', 'UPAH POKOK')->mergeCells('E11:F11');
                            $objWorkSheet->setCellValue('G11', 'SKILL')->mergeCells('G11:H11');
                            $objWorkSheet->setCellValue('I11', 'PPh')->mergeCells('I11:J11');
                            $objWorkSheet->setCellValue('K11', 'KOMPONEN B')->mergeCells('K11:L11');
                            $objWorkSheet->setCellValue('M11', 'MANAGEMENT FEE')->mergeCells('M11:N11');
                            $objWorkSheet->setCellValue('O11', 'HARI KERJA');
                            $objWorkSheet->setCellValue('P11', 'ABSEN');
                            $objWorkSheet->setCellValue('Q11', 'TOTAL')->mergeCells('Q11:R11');
                            // content part "Supir"
                            $bonusSupir = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('jabatan' => 'Driver')));
                            $bonusan = ($bonusSupir != NULL) ? $bonusSupir->komisi : 0;
                            $objWorkSheet->setCellValue('C6', 'Upah Pokok')->mergeCells('C6:D6');
                            $objWorkSheet->setCellValue('C7', 'SKILL')->mergeCells('C7:D7');
                            $objWorkSheet->setCellValue('E6', self::_toRp($rProyek->gaji_driver))->mergeCells('E6:F6');
                            $objWorkSheet->setCellValue('G6', self::_toRp(round($rProyek->gaji_driver / 26)))->mergeCells('G6:H6');
                            $objWorkSheet->setCellValue('E7', (($bonusan > 0) ? self::_toRp($bonusan) : '-'))->mergeCells('E7:F7');
                            $objWorkSheet->setCellValue('I7', 'Management Fee')->mergeCells('I7:J7');
                            $objWorkSheet->setCellValue('I8', 'PPh')->mergeCells('I8:J8');
                            $bpjs = 0;
                            $mngFee = 0;
                            $pph23 = 0;
                            $bayarTetap = $rProyek->gaji_driver + $bonusan + $rProyek->lembur_driver;

                            if ($rProyek->invoice_bpjs > 0) {
                                $bpjs = round(($rProyek->invoice_bpjs / 100) * $rProyek->invoice_umk);
                            }

                            if ($rProyek->invoice_fee_driver > 0) {
                                $mngFee = round(($rProyek->invoice_fee_driver / 100) * $bayarTetap);
                            }

                            if ($rProyek->invoice_pph > 0 && $mngFee > 0) {
                                $pph23 = round(($rProyek->invoice_pph / 100) * $mngFee);
                            }

                            $kompB = $rProyek->invoice_supervisi + $rProyek->invoice_seragam + $bpjs;
                            $objWorkSheet->setCellValue('K6', self::_toRp($kompB))->mergeCells('K6:L6');
                            $objWorkSheet->setCellValue('K7', self::_toRp($mngFee))->mergeCells('K7:L7');
                            $objWorkSheet->setCellValue('M7', self::_toRp(round($mngFee / 26)))->mergeCells('M7:N7');
                            $objWorkSheet->setCellValue('K8', self::_toRp($pph23))->mergeCells('K8:L8');
                            $no = 1;
                            $totalFee = 0;
                            $totalSemua = 0;
                            $barisan = 0;

                            foreach ($drivers as $supir) {
                                $baris = 11 + $no;
                                $diTotal = $supir['gaji'] + $bonusan + $supir['pph'] + $kompB + $supir['fee'];
                                $objWorkSheet->setCellValue('A' . $baris, $no);
                                $objWorkSheet->setCellValue('B' . $baris, $supir['nama'])->mergeCells('B' . $baris . ':D' . $baris);
                                $objWorkSheet->setCellValue('E' . $baris, self::_toRp($supir['gaji']))->mergeCells('E' . $baris . ':F' . $baris);
                                $objWorkSheet->setCellValue('G' . $baris, (($bonusan > 0) ? self::_toRp($bonusan) : '-'))->mergeCells('G' . $baris . ':H' . $baris);
                                $objWorkSheet->setCellValue('I' . $baris, self::_toRp($supir['pph']))->mergeCells('I' . $baris . ':J' . $baris);
                                $objWorkSheet->setCellValue('K' . $baris, self::_toRp($kompB))->mergeCells('K' . $baris . ':L' . $baris);
                                $objWorkSheet->setCellValue('M' . $baris, self::_toRp($supir['fee']))->mergeCells('M' . $baris . ':N' . $baris);
                                $objWorkSheet->setCellValue('O' . $baris, $supir['hari']);
                                $objWorkSheet->setCellValue('P' . $baris, $supir['absen']);
                                $objWorkSheet->setCellValue('Q' . $baris, self::_toRp($diTotal))->mergeCells('Q' . $baris . ':R' . $baris);
                                $totalFee += $supir['fee'];
                                $totalSemua += $diTotal;
                                $no++;
                                $barisan = $baris;
                            }

                            $barisan++;
                            $objWorkSheet->setCellValue('M' . $barisan, self::_toRp($totalFee))->mergeCells('M' . $barisan . ':N' . $barisan);
                            $objWorkSheet->setCellValue('Q' . $barisan, self::_toRp($totalSemua))->mergeCells('Q' . $barisan . ':R' . $barisan);
                            $jasaSupir = $totalSemua;
                            $feeSupir = $totalFee;
                            $barisKernet = $barisan + 2;
                            // header part "Kernet"
                            $objWorkSheet->setCellValue('A' . $barisKernet, 'Kernet')->mergeCells('A' . $barisKernet . ':B' . ($barisKernet + 4));
                            $objWorkSheet->getStyle('A' . $barisKernet . ':B' . ($barisKernet + 4))->applyFromArray(array(
                                'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ));
                            $objWorkSheet->setCellValue('C' . $barisKernet, 'Komponen Upah A')->mergeCells('C' . $barisKernet . ':D' . $barisKernet);
                            $objWorkSheet->setCellValue('E' . $barisKernet, 'Harga/bln')->mergeCells('E' . $barisKernet . ':F' . $barisKernet);
                            $objWorkSheet->setCellValue('G' . $barisKernet, 'Harga/hr')->mergeCells('G' . $barisKernet . ':H' . $barisKernet);
                            $objWorkSheet->setCellValue('I' . ($barisKernet + 1), 'Komponen Upah B')->mergeCells('I' . ($barisKernet + 1) . ':J' . ($barisKernet + 1));
                            $objWorkSheet->setCellValue('K' . $barisKernet, 'Harga/bln')->mergeCells('K' . $barisKernet . ':L' . $barisKernet);
                            $objWorkSheet->setCellValue('M' . $barisKernet, 'Harga/hr')->mergeCells('M' . $barisKernet . ':N' . $barisKernet);
                            $objWorkSheet->setCellValue('A' . ($barisKernet + 6), 'NO');
                            $objWorkSheet->setCellValue('B' . ($barisKernet + 6), 'NAMA')->mergeCells('B' . ($barisKernet + 6) . ':D' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('E' . ($barisKernet + 6), 'UPAH POKOK')->mergeCells('E' . ($barisKernet + 6) . ':F' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('G' . ($barisKernet + 6), 'SKILL')->mergeCells('G' . ($barisKernet + 6) . ':H' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('I' . ($barisKernet + 6), 'PPh')->mergeCells('I' . ($barisKernet + 6) . ':J' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('K' . ($barisKernet + 6), 'KOMPONEN B')->mergeCells('K' . ($barisKernet + 6) . ':L' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('M' . ($barisKernet + 6), 'MANAGEMENT FEE')->mergeCells('M' . ($barisKernet + 6) . ':N' . ($barisKernet + 6));
                            $objWorkSheet->setCellValue('O' . ($barisKernet + 6), 'HARI KERJA');
                            $objWorkSheet->setCellValue('P' . ($barisKernet + 6), 'ABSEN');
                            $objWorkSheet->setCellValue('Q' . ($barisKernet + 6), 'TOTAL')->mergeCells('Q' . ($barisKernet + 6) . ':R' . ($barisKernet + 6));
                            // content part "Kernet"
                            $bonusKernet = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('jabatan' => 'Helper')));
                            $bonusan = ($bonusKernet != NULL) ? $bonusKernet->komisi : 0;
                            $objWorkSheet->setCellValue('C' . ($barisKernet + 1), 'Upah Pokok')->mergeCells('C' . ($barisKernet + 1) . ':D' . ($barisKernet + 1));
                            $objWorkSheet->setCellValue('C' . ($barisKernet + 2), 'SKILL')->mergeCells('C' . ($barisKernet + 2) . ':D' . ($barisKernet + 2));
                            $objWorkSheet->setCellValue('E' . ($barisKernet + 1), self::_toRp($rProyek->gaji_helper))->mergeCells('E' . ($barisKernet + 1) . ':F' . ($barisKernet + 1));
                            $objWorkSheet->setCellValue('G' . ($barisKernet + 1), self::_toRp(round($rProyek->gaji_helper / 26)))->mergeCells('G' . ($barisKernet + 1) . ':H' . ($barisKernet + 1));
                            $objWorkSheet->setCellValue('E' . ($barisKernet + 2), (($bonusan > 0) ? self::_toRp($bonusan) : '-'))->mergeCells('E' . ($barisKernet + 2) . ':F' . ($barisKernet + 2));
                            $objWorkSheet->setCellValue('I' . ($barisKernet + 2), 'Management Fee')->mergeCells('I' . ($barisKernet + 2) . ':J' . ($barisKernet + 2));
                            $objWorkSheet->setCellValue('I' . ($barisKernet + 3), 'PPh')->mergeCells('I' . ($barisKernet + 3) . ':J' . ($barisKernet + 3));
                            $mngFee = 0;
                            $pph23 = 0;
                            $bayarTetap = $rProyek->gaji_helper + $bonusan + $rProyek->lembur_helper;

                            if ($rProyek->invoice_fee_helper > 0) {
                                $mngFee = round(($rProyek->invoice_fee_helper / 100) * $bayarTetap);
                            }

                            if ($rProyek->invoice_pph > 0 && $mngFee > 0) {
                                $pph23 = round(($rProyek->invoice_pph / 100) * $mngFee);
                            }

                            $objWorkSheet->setCellValue('K' . ($barisKernet + 1), self::_toRp($kompB))->mergeCells('K' . ($barisKernet + 1) . ':L' . ($barisKernet + 1));
                            $objWorkSheet->setCellValue('K' . ($barisKernet + 2), self::_toRp($mngFee))->mergeCells('K' . ($barisKernet + 2) . ':L' . ($barisKernet + 2));
                            $objWorkSheet->setCellValue('M' . ($barisKernet + 2), self::_toRp(round($mngFee / 26)))->mergeCells('M' . ($barisKernet + 2) . ':N' . ($barisKernet + 2));
                            $objWorkSheet->setCellValue('K' . ($barisKernet + 3), self::_toRp($pph23))->mergeCells('K' . ($barisKernet + 3) . ':L' . ($barisKernet + 3));
                            $no = 1;
                            $totalFee = 0;
                            $totalSemua = 0;
                            $barisan = 0;
                            $lanjut = $barisKernet + 6;

                            foreach ($helpers as $kernet) {
                                $baris = $lanjut + $no;
                                $diTotal = $kernet['gaji'] + $bonusan + $kernet['pph'] + $kompB + $kernet['fee'];
                                $objWorkSheet->setCellValue('A' . $baris, $no);
                                $objWorkSheet->setCellValue('B' . $baris, $kernet['nama'])->mergeCells('B' . $baris . ':D' . $baris);
                                $objWorkSheet->setCellValue('E' . $baris, self::_toRp($kernet['gaji']))->mergeCells('E' . $baris . ':F' . $baris);
                                $objWorkSheet->setCellValue('G' . $baris, (($bonusan > 0) ? self::_toRp($bonusan) : '-'))->mergeCells('G' . $baris . ':H' . $baris);
                                $objWorkSheet->setCellValue('I' . $baris, self::_toRp($kernet['pph']))->mergeCells('I' . $baris . ':J' . $baris);
                                $objWorkSheet->setCellValue('K' . $baris, self::_toRp($kompB))->mergeCells('K' . $baris . ':L' . $baris);
                                $objWorkSheet->setCellValue('M' . $baris, self::_toRp($kernet['fee']))->mergeCells('M' . $baris . ':N' . $baris);
                                $objWorkSheet->setCellValue('O' . $baris, $kernet['hari']);
                                $objWorkSheet->setCellValue('P' . $baris, $kernet['absen']);
                                $objWorkSheet->setCellValue('Q' . $baris, self::_toRp($diTotal))->mergeCells('Q' . $baris . ':R' . $baris);
                                $totalFee += $kernet['fee'];
                                $totalSemua += $diTotal;
                                $no++;
                                $barisan = $baris;
                            }

                            $barisan++;
                            $objWorkSheet->setCellValue('M' . $barisan, self::_toRp($totalFee))->mergeCells('M' . $barisan . ':N' . $barisan);
                            $objWorkSheet->setCellValue('Q' . $barisan, self::_toRp($totalSemua))->mergeCells('Q' . $barisan . ':R' . $barisan);
                            $jasaKernet = $totalSemua;
                            $feeKernet = $totalFee;
                            $totalItung = $jasaKernet + $jasaSupir + $feeKernet + $feeSupir;
                            $ppnItung = 0;

                            if ($rProyek->invoice_ppn > 0) {
                                $ppnItung = round(($rProyek->invoice_ppn / 100) * $totalItung);
                            }

                            $barisTutup = $barisan + 2;
                            $objWorkSheet->setCellValue('B' . $barisTutup, 'Jasa Supir')->mergeCells('B' . $barisTutup . ':C' . $barisTutup);
                            $objWorkSheet->setCellValue('D' . $barisTutup, self::_toRp($jasaSupir))->mergeCells('D' . $barisTutup . ':E' . $barisTutup);
                            $objWorkSheet->setCellValue('M' . $barisTutup, 'PT. GARDA TRIMITRA UTAMA')->mergeCells('M' . $barisTutup . ':O' . $barisTutup);
                            $barisTutup++;
                            $objWorkSheet->setCellValue('B' . $barisTutup, 'Management Fee')->mergeCells('B' . $barisTutup . ':C' . $barisTutup);
                            $objWorkSheet->setCellValue('D' . $barisTutup, self::_toRp($feeSupir))->mergeCells('D' . $barisTutup . ':E' . $barisTutup);
                            $objWorkSheet->setCellValue('G' . $barisTutup, 'Jasa')->mergeCells('G' . $barisTutup . ':H' . $barisTutup);
                            $objWorkSheet->setCellValue('I' . $barisTutup, self::_toRp($jasaSupir + $jasaKernet))->mergeCells('I' . $barisTutup . ':J' . $barisTutup);
                            $barisTutup++;
                            $objWorkSheet->setCellValue('F' . $barisTutup, 'Total');
                            $objWorkSheet->setCellValue('G' . $barisTutup, 'Management')->mergeCells('G' . $barisTutup . ':H' . $barisTutup);
                            $objWorkSheet->setCellValue('I' . $barisTutup, self::_toRp($feeSupir + $feeKernet))->mergeCells('I' . $barisTutup . ':J' . $barisTutup);
                            $barisTutup++;
                            $objWorkSheet->setCellValue('B' . $barisTutup, 'Jasa Kernet')->mergeCells('B' . $barisTutup . ':C' . $barisTutup);
                            $objWorkSheet->setCellValue('D' . $barisTutup, self::_toRp($jasaKernet))->mergeCells('D' . $barisTutup . ':E' . $barisTutup);
                            $objWorkSheet->setCellValue('I' . $barisTutup, self::_toRp($totalItung))->mergeCells('I' . $barisTutup . ':J' . $barisTutup);
                            $barisTutup++;
                            $objWorkSheet->setCellValue('B' . $barisTutup, 'Management Fee')->mergeCells('B' . $barisTutup . ':C' . $barisTutup);
                            $objWorkSheet->setCellValue('D' . $barisTutup, self::_toRp($feeKernet))->mergeCells('D' . $barisTutup . ':E' . $barisTutup);
                            $objWorkSheet->setCellValue('I' . $barisTutup, self::_toRp($ppnItung))->mergeCells('I' . $barisTutup . ':J' . $barisTutup);
                            $barisTutup++;
                            $objWorkSheet->setCellValue('I' . $barisTutup, self::_toRp($ppnItung + $totalItung))->mergeCells('I' . $barisTutup . ':J' . $barisTutup);
                            $objWorkSheet->setCellValue('M' . $barisTutup, '( SIMON LIAN )')->mergeCells('M' . $barisTutup . ':O' . $barisTutup);
                            $barisTutup += 2;
                            $objWorkSheet->setCellValue('D' . $barisTutup, 'NB: KOMPONEN B EXCLUDE THR')->mergeCells('D' . $barisTutup . ':M' . $barisTutup);
                        }
                    }
                }
            }

            self::_done('Rinci_Invoice_' . $request['param'] . '.xls');
        } else {
            self::index();
        }
    }

    public function biodata() {
        if ($this->sesiAkun !== FALSE) {
            $this->objPHPExcel = new PHPExcel();
            $this->objPHPExcel->getActiveSheet();
            $objWorkSheet = $this->objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Data Pegawai');
            $objWorkSheet->setCellValue('A1', 'ID Pegawai');
            $objWorkSheet->setCellValue('B1', 'Nama Lengkap');
            $objWorkSheet->setCellValue('C1', 'No KTP');
            $objWorkSheet->setCellValue('D1', 'No NPWP');
            $objWorkSheet->setCellValue('E1', 'No BPJS');
            $objWorkSheet->setCellValue('F1', 'Tempat Lahir');
            $objWorkSheet->setCellValue('G1', 'Tanggal Lahir');
            $objWorkSheet->setCellValue('H1', 'Alamat Lengkap');
            $objWorkSheet->setCellValue('I1', 'No Telepon Utama');
            $objWorkSheet->setCellValue('J1', 'No Telepon Alternatif');
            $objWorkSheet->setCellValue('K1', 'Email');
            $objWorkSheet->setCellValue('L1', 'Rekening');
            $objWorkSheet->setCellValue('M1', 'Pendidikan');
            $objWorkSheet->setCellValue('N1', 'Jenis Kelamin');
            $objWorkSheet->setCellValue('O1', 'Status Pernikahan');
            $objWorkSheet->setCellValue('P1', 'Jumlah Anak');
            $objWorkSheet->setCellValue('Q1', 'Agama');
            $objWorkSheet->setCellValue('R1', 'Proyek');
            $objWorkSheet->setCellValue('S1', 'Jabatan');
            $objWorkSheet->setCellValue('T1', 'Status');
            $request = $this->input->post(NULL, TRUE); // use XSS Filtering

            if ($request !== FALSE) {
                // view all
                $status = 'a';
                $proyek = 'a';
                // proses
                $queries = explode('___', $request['param']);
                $baris = 2;
                $kelamin = array('cowok' => 'Laki-Laki', 'cewek' => 'Perempuan');
                $nikah = array('belum' => 'Belum Menikah', 'nikah' => 'Menikah', 'cerai' => 'Bercerai');

                if (strpos($queries[0], 's') !== FALSE) {
                    $stat = explode('_', $queries[0]);
                    $status = $stat[1];
                }

                if (strpos($queries[1], 'p') !== FALSE) {
                    $stat = explode('_', $queries[1]);
                    $proyek = $stat[1];
                }

                foreach ($this->model->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                    $rProyek = NULL;
                    $rJabatan = NULL;
                    $isShow = TRUE;
                    $kontrak = '-';
                    $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

                    if ($rKontrak != NULL) {
                        $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $rKontrak->proyek, 'terpakai' => 1)));
                        $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));

                        if ($rKontrak->status === 'PKWT') {
                            $kontrak = $rKontrak->status . '-' . $rKontrak->ke;
                        } else {
                            $kontrak = strtoupper($rKontrak->status);
                        }

                        if (($rKontrak->status === 'PKWT') && ($rKontrak->habis < date('Y-m-d'))) {
                            $kontrak = ($rKontrak->status === 'PKWT') ? 'HABIS MASA KONTRAK' : strtoupper($rKontrak->status);
                        }
                    }

                    if ($status === '1') {
                        $isShow = FALSE;

                        if ($rKontrak != NULL) {
                            if ($rKontrak->habis >= date('Y-m-d')) { // kontrak masih berlaku
                                if ($rKontrak->status === 'PKWT') {
                                    $isShow = TRUE;
                                    $kontrak = $rKontrak->status . '-' . $rKontrak->ke;
                                }
                            }
                        }
                    }

                    if ($status === '0') { // kontrak habis
                        $isShow = FALSE;

                        if ($rKontrak != NULL) {
                            $isShow = (($rKontrak->status !== 'PKWT') || ($rKontrak->habis < date('Y-m-d')));
                            $kontrak = ($rKontrak->status === 'PKWT') ? 'HABIS MASA KONTRAK' : strtoupper($rKontrak->status);
                        }
                    }

                    if ($proyek !== 'a') {
                        if ($rProyek != NULL && $isShow) {
                            $isShow = ($rProyek->kode === $proyek);
                        } else {
                            $isShow = FALSE;
                        }
                    }

                    if ($isShow) {
                        $noTelp = explode('_', $record->telepon);
                        $rekBank = explode('_', $record->rekening);
                        $rBank = $this->model->getRecord(array('table' => 'data_bank', 'where' => array('kode' => $rekBank[0], 'terpakai' => 1)));
                        $objWorkSheet->setCellValue('A' . $baris, strtoupper($record->id));
                        $objWorkSheet->setCellValue('B' . $baris, ucwords($record->nama));
                        $objWorkSheet->setCellValue('C' . $baris, strtoupper($record->ktp));
                        $objWorkSheet->setCellValue('D' . $baris, strtoupper($record->npwp));
                        $objWorkSheet->setCellValue('E' . $baris, strtoupper($record->bpjs));
                        $objWorkSheet->setCellValue('F' . $baris, ucwords($record->tempat_lahir));
                        $objWorkSheet->setCellValue('G' . $baris, $this->formatdate->getDate($record->tanggal_lahir));
                        $objWorkSheet->setCellValue('H' . $baris, $record->alamat);
                        $objWorkSheet->setCellValue('I' . $baris, $noTelp[0]);
                        $objWorkSheet->setCellValue('J' . $baris, $noTelp[1]);
                        $objWorkSheet->setCellValue('K' . $baris, $record->email);
                        $objWorkSheet->setCellValue('L' . $baris, (($rBank == NULL) ? '' : strtoupper($rBank->bank) . ' ' . $rekBank[1]));
                        $objWorkSheet->setCellValue('M' . $baris, $record->pendidikan);
                        $objWorkSheet->setCellValue('N' . $baris, $kelamin[$record->kelamin]);
                        $objWorkSheet->setCellValue('O' . $baris, $nikah[$record->nikah]);
                        $objWorkSheet->setCellValue('P' . $baris, $record->anak);
                        $objWorkSheet->setCellValue('Q' . $baris, $record->agama);
                        $objWorkSheet->setCellValue('R' . $baris, (($rProyek != NULL) ? ucwords($rProyek->proyek) : '-'));
                        $objWorkSheet->setCellValue('S' . $baris, (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-'));
                        $objWorkSheet->setCellValue('T' . $baris, $kontrak);
                        $baris++;
                    }
                }
            }

            self::_done('Data_Pegawai_' . $this->session->userdata('_id') . '.xls');
        } else {
            self::index();
        }
    }

    public function presensi() {
        if ($this->sesiAkun !== FALSE) {
            $this->objPHPExcel = new PHPExcel();
            $this->objPHPExcel->getActiveSheet();
            $objWorkSheet = $this->objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Data Presensi');
            $objWorkSheet->setCellValue('A1', 'No')->mergeCells('A1:A2');
            $objWorkSheet->setCellValue('B1', 'Nama')->mergeCells('B1:B2');
            $objWorkSheet->setCellValue('C1', 'Jabatan')->mergeCells('C1:C2');
            $objWorkSheet->getStyle('A1:A2')->applyFromArray(array(
                'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ));
            $objWorkSheet->getStyle('B1:B2')->applyFromArray(array(
                'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ));
            $objWorkSheet->getStyle('C1:C2')->applyFromArray(array(
                'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ));
            $request = $this->input->post(NULL, TRUE); // use XSS Filtering

            if ($request !== FALSE) {
                $proyek = $request['proyek'];
                $periode = $request['periode'];
                $biodata = $request['biodata'];
                $bulan = 0;
                $tahun = 0;
                $where = array('terpakai' => 1, 'YEAR(waktu)' => date('Y'), 'MONTH(waktu)' => date('m'));
                $warnaStatus = array('hadir' => '07B52A', 'izin' => 'FFFFFF', 'alfa' => 'FFFFFF', 'sakit' => 'FAFF00', 'lembur' => 'F40212');
                $presensi = array();
                $pegawai = array();
                $no = 1;
                $baris = 3;
                $colHari = 0;
                $isiHari = array();
                $isiBulan = array();
                $kolom = 'D';
                $col = 'E';
                $periodeText = '';
                $isAllow = TRUE;

                if ($proyek !== 'all') {
                    $where['proyek'] = $proyek;
                }

                if ($biodata !== 'all') {
                    $where['biodata'] = $biodata;
                }

                if ($periode === 'x') { // periode berdasarkan bulan dan tahun
                    $bulan = intval($request['bulan']);
                    $tahun = intval($request['tahun']);
                    $colHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                    $where['YEAR(waktu)'] = $tahun;
                    $where['MONTH(waktu)'] = $request['bulan'];
                    $periodeText = 'Periode ' . $this->formatdate->getMonth($bulan) . ' ' . $tahun;

                    for ($ke = 1; $ke <= $colHari; $ke++) {
                        $keText = '0' . $ke;

                        if ($ke >= 10) {
                            $keText = $ke;
                        }

                        $isiHari[$ke] = $keText;
                        $isiBulan[$ke] = $request['bulan'];
                    }
                } else { // pakai periode, bulan dan tahun = x
                    unset($where['YEAR(waktu)']);
                    unset($where['MONTH(waktu)']);
                    $periodenya = explode('___', $periode);
                    $where['DATE(waktu) >='] = $periodenya[0];
                    $where['DATE(waktu) <='] = $periodenya[1];
                    $tahun = intval(substr($periodenya[0], 0, 4));
                    $rangeBulan = array(substr($periodenya[0], 5, 2), substr($periodenya[1], 5, 2));
                    $rangeTgl = array(substr($periodenya[0], 8, 2), substr($periodenya[1], 8, 2));
                    $dateA = new DateTime($periodenya[0]);
                    $dateB = new DateTime($periodenya[1]);
                    $diff = $dateB->diff($dateA);
                    $isAllow = (intval($diff->y) === 0);
                    $periodeText = 'Periode ' . $this->formatdate->getDate($periodenya[0]) . ' - ' . $this->formatdate->getDate($periodenya[1]);

                    if ($periodenya[0] === $periodenya[1]) {
                        $periodeText = 'Periode ' . $this->formatdate->getDate($periodenya[0]);
                    }

                    if ($isAllow) {
                        $jmlBulan = intval($diff->m);
                        $isAllow = ($jmlBulan <= 1);

                        if ($isAllow) {
                            for ($bln = 0; $bln <= $jmlBulan; $bln++) {
                                $tglDari = intval($rangeTgl[$bln]);
                                $tglHingga = cal_days_in_month(CAL_GREGORIAN, $rangeBulan[$bln], $tahun);

                                if ($bln > 0) {
                                    $tglDari = 1;
                                    $tglHingga = intval($rangeTgl[$bln]);
                                }

                                for ($ke = $tglDari; $ke <= $tglHingga; $ke++) {
                                    $keText = '0' . $ke;
                                    $colHari++;

                                    if ($ke >= 10) {
                                        $keText = $ke;
                                    }

                                    $isiBulan[$colHari] = $rangeBulan[$bln];
                                    $isiHari[$colHari] = $keText;
                                }
                            }
                        }
                    }
                }

                if ($isAllow) {
                    foreach ($this->model->getList(array('table' => 'data_presensi_info', 'where' => $where, 'sort' => 'waktu asc')) as $record) {
                        $rBiodata = $this->model->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

                        if ($rBiodata != NULL) {
                            $waktu = explode(' ', $record->waktu);
                            $rKontrak = $this->model->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $rBiodata->kode, 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                            $rJabatan = NULL;

                            if ($rKontrak != NULL) {
                                $rJabatan = $this->model->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                            }

                            if (!isset($presensi[$record->biodata . '=' . $waktu[0]])) {
                                $presensi[$record->biodata . '=' . $waktu[0]] = array($record->status, $record->kode);
                            }

                            if (!isset($pegawai[$rBiodata->kode])) {
                                $pegawai[$rBiodata->kode] = array(ucwords($rBiodata->nama), (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-'));
                            }
                        }
                    }

                    foreach ($pegawai as $key => $value) {
                        $objWorkSheet->setCellValue('A' . $baris, $no);
                        $objWorkSheet->setCellValue('B' . $baris, $value[0]);
                        $objWorkSheet->setCellValue('C' . $baris, $value[1]);
                        $objWorkSheet->getStyle('A' . $baris)->applyFromArray(array(
                            'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ));
                        $totalHadir = 0;
                        $totalLembur = 0;

                        for ($ke = 1; $ke <= $colHari; $ke++) {
                            $status = $key . '=' . $tahun . '-' . $isiBulan[$ke] . '-' . $isiHari[$ke];

                            if (isset($presensi[$status])) {
                                $colorAt = $presensi[$status][0];

                                if ($presensi[$status][0] === 'hadir') {
                                    if ($this->model->getRecord(array('table' => 'data_presensi_lembur', 'where' => array('presensi' => $presensi[$status][1], 'terpakai' => 1))) == NULL) {
                                        $totalHadir++;
                                    } else {
                                        $totalLembur++;
                                        $colorAt = 'lembur';
                                    }
                                }

                                $objWorkSheet->setCellValue($kolom . '' . $baris, 'P');
                                $objWorkSheet->getStyle($kolom . '' . $baris)->applyFromArray(array(
                                    'font' => array('name' => 'Wingdings 2'),
                                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => $warnaStatus[$colorAt]),
                                    )
                                ));
                            }

                            $kolom++;
                            $lanjut = $kolom;
                        }

                        $objWorkSheet->setCellValue($lanjut . '' . $baris, $totalHadir);
                        $objWorkSheet->setCellValue(( ++$lanjut) . '' . $baris, $totalLembur);
                        $baris++;
                        $no++;
                        $kolom = 'D'; // reset kolom
                    }

                    for ($ke = 1; $ke <= $colHari; $ke++) {
                        $objWorkSheet->setCellValue($kolom . '2', $isiHari[$ke]);
                        $objWorkSheet->getStyle($kolom . '2')->applyFromArray(array(
                            'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ));
                        $col = $kolom;
                        $kolom++;
                    }

                    $objWorkSheet->setCellValue('D1', $periodeText)->mergeCells('D1:' . $col . '1');
                    $objWorkSheet->getStyle('D1:' . $col . '1')->applyFromArray(array(
                        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ));
                    $objWorkSheet->setCellValue($kolom . '1', 'Total Aktif Kerja')->mergeCells($kolom . '1:' . $kolom . '2');
                    $objWorkSheet->getStyle($kolom . '1:' . $kolom . '2')->applyFromArray(array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, 'wrap' => TRUE
                        )
                    ));
                    $kolom++;
                    $objWorkSheet->setCellValue($kolom . '1', 'Total Hari Lembur')->mergeCells($kolom . '1:' . $kolom . '2');
                    $objWorkSheet->getStyle($kolom . '1:' . $kolom . '2')->applyFromArray(array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, 'wrap' => TRUE
                        )
                    ));
                }
            }

            self::_done('Data_Presensi_' . $this->session->userdata('_id') . '.xls');
        } else {
            self::index();
        }
    }

    private function _fixDec($value) {
        return str_replace('.00', '', $value) . ' %';
    }

    private function _toRp($value) {
        return 'Rp. ' . number_format($value, 0, ',', '.');
    }

    private function _done($filename) { // saving file
        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
        $objWriter->save('./etc/excel/' . $filename); // writing it to server's HD
        echo json_encode(array('return' => array('file' => $filename)));
    }

}
