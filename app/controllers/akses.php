<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of akses
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Akses extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        redirect(site_url());
    }

    // login or logout
    public function untuk($action) {
        if ($action == 'masuk') { // for login
            echo self::_masuk();
        } else if ($action == 'keluar') { // for logout
            self::_keluar();
        }
    }

    private function _masuk() {
        /*
         * code info:
         * 	- 0 = akses tidak sah (user undefined)
         * 	- 1 = user granted
         * 	- 2 = input invalid
         */
        $code = 2;
        $message = 'Akses Ditolak';

        if ($this->sesiAkun === FALSE) {
            $this->load->model('otentikasi');
            $this->form_validation->set_rules($this->otentikasi->getRules());

            if ($this->form_validation->run() === FALSE) {
                $delimiter = '<i class="fa fa-exclamation"></i> ';
                $this->form_validation->set_error_delimiters($delimiter, '<br>');
                $message = validation_errors();
            } else {
                $code = $this->otentikasi->isValid($this->input->post(NULL, TRUE)); // use XSS Filtering
            }
        }

        if ($code === 1) {
            $this->session->set_userdata($this->otentikasi->getUserdata());
            $code = 1;
            $message = 'Akses Diterima, Selamat Datang';
        }

        return json_encode(array('data' => array(
                'code' => $code, 'message' => $message
        )));
    }

    private function _keluar() {
        if ($this->sesiAkun !== FALSE) {
            $this->session->sess_destroy();
        }

        redirect(site_url());
    }

}
